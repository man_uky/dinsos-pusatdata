<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\ClientsExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Auth;
use App\ClientIntervensi;

class ReportController extends Controller
{
	public function index(Request $request)
	{
        if($request->month)$data['month'] = $request->month;
        else $data['month'] = date('n');
        if($request->year)$data['year'] = $request->year;
        else $data['year']  = date('Y');
        if($request->date_start)$data['date_start'] = $request->date_start;
        else $data['date_start'] = 1;
        if($request->date_end)$data['date_end'] = $request->date_end;
        else $data['date_end'] = JUMLAH_HARI[$data['month'] -1];
        $data['jumlah_hari'] = JUMLAH_HARI[$data['month'] -1];
		return view('admin.report.index',$data);
	}
    public function get_qr_url($request){
        $now = new \DateTime('now');
        $bulan = $now->format('m');
        $tahun = $now->format('Y');
        $tanggal = $now->format('d');
        $url_qr= url('/').'/validasi_cetak?user_id='.Auth::user()->id.'&uri='.$request->path().'&waktu='.$tanggal.'_'.$bulan.'_'.$tahun;
        return $url_qr;
    }
    public function download_client()
    {
    	return Excel::download(new ClientsExport, 'data.xlsx');
    }
    public function download_kwitansi(Request $request)
    {
        $url_qr = $this->get_qr_url($request);
        $bulan = $request->bulan;
        $tahun = $request->tahun;
    	$user = Auth::user();
        $kelurahan = explode(")",explode("(",$user->nama)[1])[0];
        $client_intervensi = ClientIntervensi::where('penyedia_id',$user->penyedia_id)
        ->whereMonth('tanggal_mulai','<=',$bulan)
        ->whereYear('tanggal_mulai','<=',$tahun)
        ->where(function($query) use ($bulan,$tahun){
            $query->whereMonth('tanggal_berhenti','>=',$bulan)
            ->whereYear('tanggal_berhenti','>=',$tahun)
            ->orWhereNull('tanggal_berhenti');
        })->get();
        if($bulan==2 && $tahun%400==0 || $tahun%400!=0 && $tahun%100!=0 && $tahun%4==0)$jumlah_hari=29;
        else $jumlah_hari = JUMLAH_HARI[$bulan-1];
        $jumlah_pmks = $client_intervensi->count();
        $total_harga_pertama = (int)HARGA_MAKANAN * (int)$jumlah_pmks * 10;
        $total_harga_kedua = (int)HARGA_MAKANAN * (int)$jumlah_pmks * ((int)$jumlah_hari-10);
        $ppn_pertama = $total_harga_pertama * 10/100;
		$harga_makanan_bersih_pertama = $total_harga_pertama - $ppn_pertama;
		$ppn_kedua = $total_harga_kedua * 10/100;
		$harga_makanan_bersih_kedua = $total_harga_kedua - $ppn_kedua;
        PDF::SetMargins(5, 10, 5);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', 12);

        $html = '
        <table width="100%" >
        	<tr>
        		<td rowspan="19" bgcolor="#bdbac1" width="10%;" style="border-right-width:1;border-left-width:1;border-bottom-width:1;border-top-width:1;"></td>
        		<td colspan="12" align="center" width="90%;" style="border-right-width:1;border-top-width:1;"><h2>KUITANSI</h2></td>
        	</tr>
        	<tr>
        		<td style="border-bottom-width:1;" colspan="2" width="20%">NO</td>
        		<td style="border-right-width:1;" colspan="10" width="70%" ></td>
        	</tr>
        	<tr>
        		<td colspan="2" width="20%" >Telah Terima Dari</td>
        		<td width="2%">:</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Pejabat Pembuat Komitmen Dinas Sosial Kota Surabaya</td>
        	</tr>
        	<tr>
        		<td colspan="2" width="20%" >Uang Sejumlah</td>
        		<td width="2%">:</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%"><h4>'.ucwords(terbilang((int)$harga_makanan_bersih_pertama)).' Rupiah</h4></td>
        	</tr>
        	<tr>
        		<td colspan="2" width="20%" >Untuk Pembayaran</td>
        		<td width="2%">:</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Belanja Makanan dan Minuman Untuk Masyarakat dalam rangka pemenuhan</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="12">kebutuhan dasar Permakanan bagi Lanjut Usia di Kelurahan '.$kelurahan.' untuk bulan '.ucwords(strtolower(BULAN[$bulan-1])).' '.$tahun.'</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="12">Kode Kegiatan 1.1.1.06.01.0003</td>
        	</tr>
        	<tr>
        		<td style="border-bottom-width:1;" colspan="2" width="20%" >Rincian Transfer</td>
        		<td style="border-bottom-width:1;" width="2%">:</td>
        		<td style="border-bottom-width:1;" width="5%">Rp.</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="8" width="63%">'.number_format( $total_harga_pertama, 0 , '' , ',' ).'</td>
        	</tr>
        	<tr>
        		<td style="border-bottom-width:1;" colspan="2" width="20%" >PPN / SPD 10%</td>
        		<td style="border-bottom-width:1;" width="2%">:</td>
        		<td style="border-bottom-width:1;" width="5%">Rp.</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="8" width="63%">'.number_format( $ppn_pertama, 0 , '' , ',' ).'</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1;" colspan="12"></td>
        	</tr>
        	<tr>
        		<td colspan="8"></td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="4">Surabaya,</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1;" colspan="12"></td>
        	</tr>
        	<tr>

        		<td colspan="8"></td>
        		<td style="border-right-width:1;" colspan="4"  align="center">Ketua Karang Werdha</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1;" colspan="12"></td>
        	</tr>
        	<tr>

                <td colspan="4">
                <img src="'.url_qrcode("300x300",$url_qr).'" alt="My QR code" height="70px" width="70px">
                </td>
        		<td style="border-right-width:1;" colspan="8"></td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1;" colspan="12"></td>
        	</tr>
        	<tr>
        		<td style="border-top-width:1; border-bottom-width:1;" bgcolor="#e6e6e6" ><h3>Rp.</h3></td>
        		<td style="border-top-width:1; border-bottom-width:1;" colspan="4" bgcolor="#e6e6e6" ><h3>'.number_format( $harga_makanan_bersih_pertama, 0 , '' , ',' ).'</h3></td>
        		<td style="border-right-width:1;" colspan="7">
                </td>
        	</tr>
        	<tr>
                <td colspan="8"></td>
        		<td style="border-right-width:1;" colspan="4" align="center"><b><u>'.$user->nama_ketua.'</u></b></td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="12"></td>
        	</tr>
        </table>
        <br>
        <br>
        <br>
        ';

        $html_kedua = '
        <table width="100%" >
        	<tr>
        		<td rowspan="19" bgcolor="#bdbac1" width="10%;" style="border-right-width:1;border-left-width:1;border-bottom-width:1;border-top-width:1;"></td>
        		<td colspan="12" align="center" width="90%;" style="border-right-width:1;border-top-width:1;"><h2>KUITANSI</h2></td>
        	</tr>
        	<tr>
        		<td style="border-bottom-width:1;" colspan="2" width="20%">NO</td>
        		<td style="border-right-width:1;" colspan="10" width="70%" ></td>
        	</tr>
        	<tr>
        		<td colspan="2" width="20%" >Telah Terima Dari</td>
        		<td width="2%">:</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Pejabat Pembuat Komitmen Dinas Sosial Kota Surabaya</td>
        	</tr>
        	<tr>
        		<td colspan="2" width="20%" >Uang Sejumlah</td>
        		<td width="2%">:</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%"><h4>'.ucwords(terbilang((int)$harga_makanan_bersih_kedua)).' Rupiah</h4></td>
        	</tr>
        	<tr>
        		<td colspan="2" width="20%" >Untuk Pembayaran</td>
        		<td width="2%">:</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Belanja Makanan dan Minuman Untuk Masyarakat dalam rangka pemenuhan</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="12">kebutuhan dasar Permakanan bagi Lanjut Usia di Kelurahan'.$kelurahan.' untuk bulan '.ucwords(strtolower(BULAN[$bulan-1])).' '.$tahun.'</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="12">Kode Kegiatan 1.1.1.06.01.0003</td>
        	</tr>
        	<tr>
        		<td style="border-bottom-width:1;" colspan="2" width="20%" >Rincian Transfer</td>
        		<td style="border-bottom-width:1;" width="2%">:</td>
        		<td style="border-bottom-width:1;" width="5%">Rp.</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="8" width="63%">'.number_format( $total_harga_kedua, 0 , '' , ',' ).'</td>
        	</tr>
        	<tr>
        		<td style="border-bottom-width:1;" colspan="2" width="20%" >PPN / SPD 10%</td>
        		<td style="border-bottom-width:1;" width="2%">:</td>
        		<td style="border-bottom-width:1;" width="5%">Rp.</td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="8" width="63%">'.number_format( $ppn_kedua, 0 , '' , ',' ).'</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1;" colspan="12"></td>
        	</tr>
        	<tr>
        		<td colspan="8"></td>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="4">Surabaya,</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1;" colspan="12"></td>
        	</tr>
        	<tr>

        		<td colspan="8"></td>
        		<td style="border-right-width:1;" colspan="4"  align="center">Ketua Karang Werdha</td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1;" colspan="12"></td>
        	</tr>
        	<tr>
                <td colspan="4">
                <img src="'.url_qrcode("300x300",$url_qr).'" alt="My QR code" height="70px" width="70px">
                </td>
        		<td style="border-right-width:1;" colspan="8"></td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1;" colspan="12"></td>
        	</tr>
        	<tr>
        		<td style="border-top-width:1; border-bottom-width:1;" bgcolor="#e6e6e6" ><h3>Rp.</h3></td>
        		<td style="border-top-width:1; border-bottom-width:1;" colspan="4" bgcolor="#e6e6e6" ><h3>'.number_format( $harga_makanan_bersih_kedua, 0 , '' , ',' ).'</h3></td>
        		<td style="border-right-width:1;" colspan="7"></td>
        	</tr>
        	<tr>
        		<td colspan="8"></td>
        		<td style="border-right-width:1;" colspan="4" align="center"><b><u>'.$user->nama_ketua.'</u></b></td>
        	</tr>
        	<tr>
        		<td style="border-right-width:1; border-bottom-width:1;" colspan="12"></td>
        	</tr>
        </table>
        <br>
        <br>
        <br>
        ';
	    PDF::SetTitle('KUITANSI');
	    PDF::AddPage('P', 'A4');
	    PDF::writeHTML($html.$html_kedua, true, false, true, false, '');
	    PDF::Output('kuitansi.pdf', 'I');
    }
    public function download_pencairan(Request $request)
    {
        $url_qr = $this->get_qr_url($request);
    	$bulan = $request->bulan;
        $tahun = $request->tahun;
        $user = Auth::user();
        $nama_karwed = explode(" (",$user->nama)[0];
        $kelurahan = explode(")",explode("(",$user->nama)[1])[0];
        $client_intervensi = ClientIntervensi::where('penyedia_id',$user->penyedia_id)
        ->whereMonth('tanggal_mulai','<=',$bulan)
        ->whereYear('tanggal_mulai','<=',$tahun)
        ->where(function($query) use ($bulan,$tahun){
            $query->whereMonth('tanggal_berhenti','>=',$bulan)
            ->whereYear('tanggal_berhenti','>=',$tahun)
            ->orWhereNull('tanggal_berhenti');
        })->get();
        if($bulan==2 && $tahun%400==0 || $tahun%400!=0 && $tahun%100!=0 && $tahun%4==0)$jumlah_hari=29;
        else $jumlah_hari = JUMLAH_HARI[$bulan-1];
        $jumlah_pmks = $client_intervensi->count();
        $total_harga = (int)HARGA_MAKANAN * (int)$jumlah_pmks * ((int)$jumlah_hari);
		$ppn = $total_harga * 10/100;
		$harga_makanan_bersih = $total_harga - $ppn;

    	PDF::SetMargins(15, 15, 15);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', 12);

        $html = '
        	<table width="100%">
    		    <tr>
				    <td align="center" width="20%"><img SRC="/images/karwed.png" height="60" width="60"></td>
				    <td align="center" width="60%">KARANG WERDA<br>
				    KELURAHAN '.$kelurahan.'<br>
			        </td>
    		    </tr>
        	</table>
		    <hr color="black">
		    </br>
		    <br>
		    <br>

	        <table>
	        <tr>
	        	<td width="300"></td>
	        	<td>Surabaya,</td>
	        </tr>
	        <tr>
	        	<td width="60">Nomor</td>
	    	  	<td width="240">: '."".'</td>
	    	  	<td>Kepada Yth</td>
	        </tr>
	        <tr>
	        	<td width="60">Lampiran</td>
	    	  	<td width="240">: '."1(satu)".'</td>
	    	  	<td>Pejabat Pembuat Komitmen</td>
	        </tr>
	        <tr>
	        	<td width="60">Perihal</td>
	    	  	<td width="240">: '."Permohonan Pencairan".'</td>
	    	  	<td>Kegiatan Pemberian Kebutuhan Dasar</td>
	        </tr>
	        <tr>
	        	<td width="60"></td>
	    	  	<td width="240">  '."Dana Permakanan".'</td>
	    	  	<td>Permakanan Bagi Lanjut Usia</td>
	        </tr>
	        </table>
	        <br>
	        <br>
            <br>
	        <table>
	        <tr>
	        	<td style="text-align:justify;" width="500"><p style="text-indent: 10em; line-height:2;">Berkenaan dengan penyelenggaraan kegiatan pemenuhan kebutuhan dasar permakanan bagi lanjut usia miskin dan lanjut usia terlantar di Kelurahan '.$kelurahan.', bersama ini mengajukan permohonan pencairan dana permakanan untuk periode Bulan '.ucwords(strtolower(BULAN[$bulan-1])).'  '.$tahun.' (tanggal 1 s.d '.$jumlah_hari.' bulan  '.ucwords(strtolower(BULAN[$bulan-1])).'  '.$tahun.') sebesar Rp. '.number_format( $harga_makanan_bersih, 0 , '' , ',' ).',- ('.ucwords(terbilang($harga_makanan_bersih)).' Rupiah). </p></td>
	        </tr>
	        <tr>
	        	<td width="500"></td>
	        </tr>
	        <tr>
	        	<td width="500"><p>Dengan rincian sebagai berikut : </p></td>
	        </tr>

	        </table>
	        <br>
	        <br>
             <br>
            <table width="100%">
                <tr>
                    <th style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height:2;"><b>Jumlah Penerima Manfaat</b></th>
                    <th style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height:2;"><b>Jumlah Hari</b></th>
                    <th style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height:2;"><b>Harga Permakanan</b></th>
                    <th style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Jumlah</b></th>
                </tr>
                <tr>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>'.$jumlah_pmks.' Orang</b></td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>'.$jumlah_hari.' Hari</b></td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Rp. 11.000,-</b></td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Rp. '.number_format( $harga_makanan_bersih, 0 , '' , ',' ).',-</b></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;">PPN / SPD 10%</td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Rp. '.number_format( $ppn, 0 , '' , ',' ).',-</b></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Total</b></td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Rp. '.number_format( $total_harga, 0 , '' , ',' ).',-</b></td>
                </tr>
            </table>
            <br><br><br>
            <table>
                <tr>
                <td width="500"><p>Demikian atas bantuan dan kerjasamanya disampaikan terima kasih.</p></td>
                </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                    <tr>
                      <td >Mengetahui</td>
                      <td >Ketua Karang Werdha <b>"'.$nama_karwed.'"</b></td>
                    </tr>
                    <tr>
                      <td><b>Lurah '.$kelurahan.'</b></td>
                      <td><b>Kelurahan '.$kelurahan.'</b></td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    
                    <tr>
                       <td colspan="2">
                       <img src="'.url_qrcode("300x300",$url_qr).'" alt="My QR code" height="70px" width="70px">
                       </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><b><u>'.$user->nama_ketua.'</u></b></td>
                    </tr>
            </table>
            <br><br>
            <table>
                <tr>
                <td width="500"><p><u>Tembusan:</u></p></td>
                </tr>
                <tr>
                <td width="500"><p>Bpk. Kepala Dinas Sosial Kota Surabaya</p></td>
                </tr>
            </table>
        ';
	    PDF::SetTitle('Permohonan Pencairan');
	    PDF::AddPage('P', 'A4');
	    PDF::writeHTML($html, true, false, true, false, '');
	    PDF::Output('Permohonan_Pencairan.pdf', 'I');
    }

    public function download_penerima(Request $request)
    {
        $url_qr = $this->get_qr_url($request);
    	$bulan = $request->bulan;
        $tahun = $request->tahun;
        $user = Auth::user();
        $nama_karwed = explode(" (",$user->nama)[0];
        $kelurahan = explode(")",explode("(",$user->nama)[1])[0];
        $client_intervensi = ClientIntervensi::where('penyedia_id',$user->penyedia_id)
        ->whereMonth('tanggal_mulai','<=',$bulan)
        ->whereYear('tanggal_mulai','<=',$tahun)
        ->where(function($query) use ($bulan,$tahun){
            $query->whereMonth('tanggal_berhenti','>=',$bulan)
            ->whereYear('tanggal_berhenti','>=',$tahun)
            ->orWhereNull('tanggal_berhenti');
        })->get();
        $tr = "";
        $tr_lanjut = "";
        $aneh=false;
        if($client_intervensi->count()>50 && $client_intervensi->count()<150)
		{
			$aneh=true;
			foreach ($client_intervensi as $key => $d) {
		        if($key<50)
		        {
			        $no=$key+1;
			        $tr.=
			        '<tr>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$no.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->nik.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->nipm.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->nama.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.date_view($d->tanggal_lahir).'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->alamat.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->no_rt.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->no_rw.'</td>
					 </tr>';
		        }
		        else {
		        	$no=$key+1;
			        $tr_lanjut.=
			        '<tr>
					 	<td style="border: 1px solid black;border-collapse: collapse;width: 25px;">'.$no.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:15%;">'.$d->nik.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:7%;">'.$d->nipm.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:20%;">'.$d->nama.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.date_view($d->tanggal_lahir).'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:30%;">'.$d->alamat.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:5%;">'.$d->no_rt.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:5%;">'.$d->no_rw.'</td>
					 </tr>';
		        }

			}
		}
		else if($client_intervensi->count()%10>10)
		{
			$aneh=true;
			foreach ($client_intervensi as $key => $d) {
		        if($client_intervensi->count()-$key>5)
		        {
			        $no=$key+1;
			        $tr.=
			        '<tr>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$no.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->nik.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->nipm.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->nama.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.date_view($d->tanggal_lahir).'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->alamat.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->no_rt.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->no_rw.'</td>
					 </tr>';
		        }
		        else {
		        	$no=$key+1;
			        $tr_lanjut.=
			        '<tr>
					 	<td style="border: 1px solid black;border-collapse: collapse;width: 25px;">'.$no.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:15%;">'.$d->nik.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:7%;">'.$d->nipm.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:20%;">'.$d->nama.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;">'.date_view($d->tanggal_lahir).'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:30%;">'.$d->alamat.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:5%;">'.$d->no_rt.'</td>
					 	<td style="border: 1px solid black;border-collapse: collapse;width:5%;">'.$d->no_rw.'</td>
					 </tr>';
		        }

			}
		}
		else
		{
			foreach ($client_intervensi as $key => $d) {
		        $no=$key+1;
		        $tr.=
		        '<tr>
				 	<td style="border: 1px solid black;border-collapse: collapse;">'.$no.'</td>
				 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->client->nik.'</td>
				 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->nipm.'</td>
				 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->client->nama.'</td>
				 	<td style="border: 1px solid black;border-collapse: collapse;">'.date_view($d->client->tanggal_lahir).'</td>
				 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->client->alamat_kk.'</td>
				 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->client->rt.'</td>
				 	<td style="border: 1px solid black;border-collapse: collapse;">'.$d->client->rw.'</td>
				 </tr>';

			}
		}
    	PDF::SetMargins(10, 10, 10);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', 12);

        $html = '
            <table width="100%">
                <tr>
                    <td></td>
                    <td>Lampiran Surat</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Nomor :</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tanggal :</td>
                </tr>
            </table>
            <br><br><br>
            <table width="100%" style="text-align:center; font-weight: bold;">
                <tr>
                    <td>DAFTAR NAMA PENERIMA MANFAAT KEGIATAN PEMENUHAN KEBUTUHAN DASAR</td>
                </tr>
                <tr>
                    <td>PERMAKANAN BAGI LANJUT USIA MISKIN DAN TERLANTAR</td>
                </tr>
                <tr>
                    <td>DI KELURAHAN '.strtoupper($kelurahan).' KECAMATAN JAMBANGAN </td>
                </tr>
                <tr>
                    <td>BULAN '.BULAN[$bulan-1].' TAHUN '.$tahun.'</td>
                </tr>
            </table>
            <br><br>
            <table style="border: 1px solid black;border-collapse: collapse;text-align:center;" class="font-kecil">
                <tr>
                    <th style="border: 1px solid black;border-collapse: collapse;width: 25px;">No</th>
                    <th style="border: 1px solid black;border-collapse: collapse;width:15%;">NIK</th>
                    <th style="border: 1px solid black;border-collapse: collapse;width:7%;">NIPM</th>
                    <th style="border: 1px solid black;border-collapse: collapse;width:20%;">NAMA LENGKAP</th>
                    <th style="border: 1px solid black;border-collapse: collapse;">TANGGAL LAHIR</th>
                    <th style="border: 1px solid black;border-collapse: collapse;width:30%;">ALAMAT</th>
                    <th style="border: 1px solid black;border-collapse: collapse;width:5%;">RT</th>
                    <th style="border: 1px solid black;border-collapse: collapse;width:5%;">RW</th>
                </tr>
             '.$tr.'
            </table>
            <br><br>';
        $ttd= '
        <table style="text-align:center">
            <tr>
                <td></td>
                <td >Surabaya, ................................................</td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td >Mengetahui</td>
                <td >Ketua Karang Werdha "'.$nama_karwed.'"</td>
            </tr>
            <tr>
                <td>Lurah '.$kelurahan.'</td>
                <td>Kelurahan '.$kelurahan.'</td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2">
                <img src="'.url_qrcode("300x300",$url_qr).'" alt="My QR code" height="70px" width="70px">
                </td>
            </tr>
            <tr>
                <td></td>
                <td><b><u>'.$user->nama_ketua.'</u></b></td>
            </tr>
        </table>';
        if(!$aneh)
		{
			$html.='<br><br>'.$ttd;
		}
	    PDF::SetTitle('Daftar Penerima');
	    PDF::AddPage('P', 'A4');
        PDF::writeHTML(cetak_css().$html, true, false, true, false, '');
        PDF::lastPage();
		if($aneh)
		{
			$tr_lanjut='<table style="border: 1px solid black;border-collapse: collapse;text-align:center;" class="font-kecil">'.$tr_lanjut.'</table><br><br>'.$ttd;
			PDF::AddPage('P','FOLIO');
			PDF::writeHTML(cetak_css().$tr_lanjut, true, false, true, false, '');
		}
		// ---------------------------------------------------------
		PDF::lastPage();
	    PDF::Output('Daftar_Penerima.pdf', 'I');
    }

    public function download_mutlak(Request $request)
    {
        $url_qr = $this->get_qr_url($request);
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        $user = Auth::user();
        $nama_karwed = explode(" (",$user->nama)[0];
        $kelurahan = explode(")",explode("(",$user->nama)[1])[0];
        $client_intervensi = ClientIntervensi::where('penyedia_id',$user->penyedia_id)
        ->whereMonth('tanggal_mulai','<=',$bulan)
        ->whereYear('tanggal_mulai','<=',$tahun)
        ->where(function($query) use ($bulan,$tahun){
            $query->whereMonth('tanggal_berhenti','>=',$bulan)
            ->whereYear('tanggal_berhenti','>=',$tahun)
            ->orWhereNull('tanggal_berhenti');
        })->get();

        PDF::SetMargins(15, 15, 15);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', 12);

        $html = '
            <table width="100%">
                <tr>
                    <td align="center" width="10%"><img SRC="/images/karwed.png" height="60" width="60"></td>
                    <td align="center" width="90%">KARANG WERDA '.$nama_karwed.'<br>
                    KELURAHAN '.$kelurahan.' KECAMATAN «Kecamatan_Besar»<br>
                    KOTA SURABAYA<br>
                    '.$user->alamat.'
                    </td>
                </tr>
            </table>
            <hr color="black">
            </br>
            <br>
            <table width="100%" style="text-align:center;">
                <tr>
                    <td><b><u>SURAT PERNYATAAN TANGGUNG JAWAB BELANJA MUTLAK</u></b></td>
                </tr>
                <tr>
                    <td>Nomor : ......................................................................</td>
                </tr>
            </table>
            <br><br><br>
            <span>Yang bertanda tangan dibawah ini :</span><br>
            <table width="100%" >
            <tr>
                <td width="10%">Nama</td>
                <td width="2%">:</td>
                <td width="88%"><b>'.$user->nama_ketua.'</b></td>
            </tr>
            <tr>
                <td width="10%">Jabatan</td>
                <td width="2%">:</td>
                <td width="88%">Ketua Karang Werda <b>"'.$nama_karwed.'" </b></td>
            </tr>
            <tr>
                <td width="10%"></td>
                <td width="2%"></td>
                <td width="88%">Kelurahan <b> '.$kelurahan.' </b> Kecamatan <b>Tandes</b></td>
            </tr>
            </table>
            <br><br><br>
            <span>Menyatakan dengan sesungguhnya bahwa  :</span>
            <ol>
            <li style="text-align:justify; line-height:1.5;">Bersedia untuk bertanggung jawab mutlak penyelenggaraan kegiatan pemenuhan kebutuhan dasar Permakanan bagi Lanjut Usia Miskin dan Lanjut Usia Terlantar pada <b> bulan '.ucwords(strtolower(BULAN[$bulan-1])).' </b> '.$tahun.' di wilayah <b> Kelurahan '.$kelurahan.' </b> sesuai dengan <b> Surat Perjanjian Kerjasama  / Kontrak antara Dinas Sosial Kota Surabaya (Pejabat Pembuat Komitmen) </b> dengan Ketua Karang Werdha <b> "'.$nama_karwed.'" </b> Kelurahan <b> '.$kelurahan.' </b> Kecamatan <b> Tandes </b> Kota Surabaya. <b> Nomor SPK : 460/0021.125/436.7.7/2019 Tanggal 2 '.ucwords(strtolower(BULAN[$bulan-1])).' '.$tahun.' </b></li>
            <li style="text-align:justify; line-height:1.5;">Apabila dikemudian hari terdapat kelebihan anggaran atas kegiatan penyelenggaraan program tersebut di atas, kami bersedia untuk menanggung secara pribadi dan menyetorkan ke Kas Daerah melalui Dinas Sosial.</li>
            </ol>
            <table width="100%" >
            <tr>
            <td style="text-align:justify; width="85%""><p style="text-indent: 10em; ">Demikian surat pernyataan ini dibuat dengan sebenar-benarnya dan tanpa ada unsur paksaan dari pihak manapun</p></td>
            </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                <tr>
                    <td></td>
                    <td >Surabaya, ................................................</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td ></td>
                    <td >Ketua Karang Werdha <b>"'.$nama_karwed.'" </b></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Kelurahan <b>'.$kelurahan.'</b></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2">
                       <img src="'.url_qrcode("300x300",$url_qr).'" alt="My QR code" height="70px" width="70px">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><b><u>'.$user->nama_ketua.'</u></b></td>
                </tr>
            </table>

        ';
        PDF::SetTitle('Multlak');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('Multlak.pdf', 'I');
    }
    public function download_kesanggupan(Request $request)
    {
        $url_qr = $this->get_qr_url($request);
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        $user = Auth::user();
        $nama_karwed = explode(" (",$user->nama)[0];
        $kelurahan = explode(")",explode("(",$user->nama)[1])[0];
        $client_intervensi = ClientIntervensi::where('penyedia_id',$user->penyedia_id)
        ->whereMonth('tanggal_mulai','<=',$bulan)
        ->whereYear('tanggal_mulai','<=',$tahun)
        ->where(function($query) use ($bulan,$tahun){
            $query->whereMonth('tanggal_berhenti','>=',$bulan)
            ->whereYear('tanggal_berhenti','>=',$tahun)
            ->orWhereNull('tanggal_berhenti');
        })->get();

        PDF::SetMargins(15, 15, 15);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', '12');

        $html = '
            <table width="100%">
                <tr>
                    <td align="center" width="10%"><img SRC="/images/karwed.png" height="60" width="60"></td>
                    <td align="center" width="90%">KARANG WERDA '.$nama_karwed.'<br>
                    KELURAHAN '.$kelurahan.' KECAMATAN «Kecamatan_Besar»<br>
                    KOTA SURABAYA<br>
                    '.$user->alamat.'
                    </td>
                </tr>
            </table>
            <hr color="black">
            </br>
            <br>
            <table width="100%" style="text-align:center;">
                <tr>
                    <td><b>SURAT PERNYATAAN KESANGGUPAN MENYELESAIKAN PEKERJAAN <br>
                    PENYELENGGARAAN KEGIATAN KEBUTUHAN DASAR PERMAKANAN BAGI<br>
                    LANJUT USIA MISKIN DAN LANJUT USIA TERLANTAR<br>
                    DI KELURAHAN SIMOMULYO<br>
                    NOMOR : ......................................................................
                    </b></td>
                </tr>
            </table>
            <br><br><br>
            <span>Pada hari ini <b>Jum’at </b>tanggal 1 bulan <b>'.ucwords(strtolower(BULAN[$bulan-1])).' </b> tahun '.$tahun.', Yang bertanda tangan dibawah ini:</span><br><br>
            <table width="100%" >
            <tr>
                <td width="10%">Nama</td>
                <td width="2%">:</td>
                <td width="88%"><b>'.$user->nama_ketua.'</b></td>
            </tr>
            <tr>
                <td width="10%">Jabatan</td>
                <td width="2%">:</td>
                <td width="88%">Ketua Karang Werdha <b>"'.$nama_karwed.'" </b>, dalam hal ini bertindak dan atas </td>
            </tr>
            <tr>
                <td width="10%"></td>
                <td width="2%"></td>
                <td width="88%">nama Karang Werdha di Kelurahan <b>'.$kelurahan.'</b> Kecamatan <b>Sukomanunggal</b></td>
            </tr>
            </table>
            <br><br>
            <span>Dengan ini menyatakan bahwa:</span>
            <ol>
            <li style="text-align:justify; line-height:1.5;">Sanggup melaksanakan pekerjaan <b> penyelenggaraan kegiatan pemenuhan kebutuhan dasar permakanan bagi lanjut usia miskin dan lanjut usia terlantar </b> di wilayah Kelurahan <b>Simomulyo</b> terhitung mulai tanggal 1 sampai tanggal <b>28</b> bulan <b>Februari</b> tahun 2019</li>
            <li style="text-align:justify; line-height:1.5;">Nama lansia yang menerima permakanan sesuai dengan nama lansia yang tercantum <b> dalam permohonan pencairan dana dan SPJ (Surat Pertanggung Jawaban)</b></li>
            <li style="text-align:justify; line-height:1.5;">Harga permakanan adalah paling tinggi sebesar Rp 11.000,- per orang dengan <b>dipungut PPh Pasal 23</b></li>
            <li style="text-align:justify; line-height:1.5;"><b>Pemberian permakanan diberikan dalam bentuk makanan dan  sesuai dengan standar yang ditetapkan</b></li>
            <li style="text-align:justify; line-height:1.5;">Pemberian permakanan dilakukan setiap hari tanpa pernah diliburkan.</li>
            <li style="text-align:justify; line-height:1.5;">Membuat Berita Acara perubahan/pergantian lansia miskin dan terlantar apabila terjadi perubahan atau pergantian penerima manfaat dalam bulan berkenaan.</li>
            </ol>
            <table width="100%" >
            <tr>
            <td style="text-align:justify; width="85%""><p style="text-indent: 10em; ">Demikian surat pernyataan ini dibuat dengan sebenar-benarnya dan penuh tanggung jawab.</p></td>
            </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                    <tr>
                      <td></td>
                      <td >Surabaya, ................................................</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td ></td>
                      <td >Ketua Karang Werdha <b>"'.$nama_karwed.'" </b></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Kelurahan <b>'.$kelurahan.'</b><br>MATERAI <br> 6000</td>
                    </tr>
                    <tr>
                       <td colspan="2">
                       <img src="'.url_qrcode("300x300",$url_qr).'" alt="My QR code" height="70px" width="70px">
                       </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><b><u>'.$user->nama_ketua.'</u></b></td>
                    </tr>
            </table>
        ';
        PDF::SetTitle('Kesanggupan');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('Kesanggupan.pdf', 'I');
    }

    public function download_rekapitulasi(Request $request)
    {
        $url_qr = $this->get_qr_url($request);
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        $user = Auth::user();
        $nama_karwed = explode(" (",$user->nama)[0];
        $kelurahan = explode(")",explode("(",$user->nama)[1])[0];
        $client_intervensi = ClientIntervensi::where('penyedia_id',$user->penyedia_id)
        ->whereMonth('tanggal_mulai','<=',$bulan)
        ->whereYear('tanggal_mulai','<=',$tahun)
        ->where(function($query) use ($bulan,$tahun){
            $query->whereMonth('tanggal_berhenti','>=',$bulan)
            ->whereYear('tanggal_berhenti','>=',$tahun)
            ->orWhereNull('tanggal_berhenti');
        })->get();
        if($bulan==2 && $tahun%400==0 || $tahun%400!=0 && $tahun%100!=0 && $tahun%4==0)$jumlah_hari=29;
        else $jumlah_hari = JUMLAH_HARI[$bulan-1];
        $jumlah_pmks = $client_intervensi->count();
        $total_harga = (int)HARGA_MAKANAN * (int)$jumlah_pmks;
		$ppn = $total_harga * 10/100;
		$harga_makanan_bersih = $total_harga - $ppn;

        PDF::SetMargins(10, 10, 10);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', '12');
        PDF::SetTitle('Rekapitulasi');
        $tr="";
		$tr_lanjut="";
        $aneh=false;
        $html='<h5 style="text-align:center;"><b>REKAPITULASI JUMLAH PERMAKANAN<br>KARANG WERDHA "'.$nama_karwed.'"<br>KELURAHAN '.$kelurahan.' KECAMATAN ASEMROWO<br>UNTUK BULAN : '.BULAN[$bulan-1].' '.$tahun.'</b></h5>
        <hr color="black">
        </br><br>
        ';
        for($i=1;$i<=$jumlah_hari;$i++)
		{
            $tr .='<tr>
                <td style="border: 1px solid black;border-collapse: collapse;">'.$i.'</td>
                <td style="border: 1px solid black;border-collapse: collapse;">'.$i.' '.ucwords(strtolower(BULAN[$bulan-1])).' '.$tahun.'</td>
                <td style="border: 1px solid black;border-collapse: collapse;">'.$jumlah_pmks.' Orang</td>
                <td style="border: 1px solid black;border-collapse: collapse;">Rp. '.number_format( HARGA_MAKANAN, 0 , '' , '.' ).',-</td>
                <td style="border: 1px solid black;border-collapse: collapse;">Rp. '.number_format( $harga_makanan_bersih, 0 , '' , '.' ).',-</td>
                <td style="border: 1px solid black;border-collapse: collapse;">'."<ol><li>Nasi</li><li>Telur Bumbu Bali</li><li>Tahu Bumbu Bali</li></ol>".'
                </td>
                <td style="border: 1px solid black;border-collapse: collapse;"></td>
            </tr>';
            if($i%10==1 && $i!=1 || $i==$jumlah_hari )
			{
                if($i!=$jumlah_hari)
                {
                    $tr.=
                    '<tr>
                        <td style="border: 1px solid black;border-collapse: collapse;text-align:center;" class="font-kecil" colspan="4">JUMLAH DIPINDAHKAN</td>
                        <td style="border: 1px solid black;border-collapse: collapse;">Rp. '.number_format( $harga_makanan_bersih*$i, 0 , '' , '.' ).',-</td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid black;border-collapse: collapse;text-align:center;" class="font-kecil" colspan="4"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid black;border-collapse: collapse;text-align:center;" class="font-kecil" colspan="4">JUMLAH PINDAHAN</td>
                        <td style="border: 1px solid black;border-collapse: collapse;">Rp. '.number_format( $harga_makanan_bersih*$i, 0 , '' , '.' ).',-</td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                    </tr>';
                }
                else
                {
                    $tr.=
                    '<tr>
                        <td style="border: 1px solid black;border-collapse: collapse;text-align:center;" class="font-kecil" colspan="4">JUMLAH</td>
                        <td style="border: 1px solid black;border-collapse: collapse;">Rp. '.number_format( $harga_makanan_bersih*$i, 0 , '' , '.' ).',-</td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                        <td style="border: 1px solid black;border-collapse: collapse;"></td>
                    </tr>';
                }

                 if($i==11)
                 {
                     $html.= '
                     <table style="border: 1px solid black;border-collapse: collapse;text-align:left;">
                     <tr>
                         <th style="border: 1px solid black;border-collapse: collapse;width: 25px">No</th>
                         <th style="border: 1px solid black;border-collapse: collapse;width: 90px">Tanggal</th>
                         <th style="border: 1px solid black;border-collapse: collapse;">JUMLAH PENERIMA</th>
                         <th style="border: 1px solid black;border-collapse: collapse;">HARGA PERMAKANAN</th>
                         <th style="border: 1px solid black;border-collapse: collapse;">JUMLAH + PPN 10%</th>
                         <th style="border: 1px solid black;border-collapse: collapse;width: 125px">JENIS MENU</th>
                         <th style="border: 1px solid black;border-collapse: collapse;width: 0px">KETERANGAN</th>
                         </tr>
                         '.$tr.'
                     </table>';
                 }
                 else
                 {
                     $html.='
                     <table style="border: 1px solid black;border-collapse: collapse;text-align:left;">
                     <tr>
                         <th style="border: 1px solid black;border-collapse: collapse;width: 25px">No</th>
                         <th style="border: 1px solid black;border-collapse: collapse;width: 90px">Tanggal</th>
                         <th style="border: 1px solid black;border-collapse: collapse;">JUMLAH PENERIMA</th>
                         <th style="border: 1px solid black;border-collapse: collapse;">HARGA PERMAKANAN</th>
                         <th style="border: 1px solid black;border-collapse: collapse;">JUMLAH + PPN 10%</th>
                         <th style="border: 1px solid black;border-collapse: collapse;width: 125px">JENIS MENU</th>
                         <th style="border: 1px solid black;border-collapse: collapse;width: 0px">KETERANGAN</th>
                         </tr>

                         '.$tr.'
                     </table>';
                 }
                 if($i<$jumlah_hari)
                 {
                    //  echo htmlentities($html);
                    //  die();
                     PDF::AddPage('L', 'A4');
                     PDF::writeHTML(cetak_css().$html, true, false, true, false, '');
                     PDF::lastPage();
                     $tr = '';
                     $html='';
                 }
                 else
                 {
                     $ttd='<br><br>
                     <table width="100%">
                     <tr>
                         <td colspan="2" width="60%"></td>
                         <td width="40%" style="text-align:center;">Surabaya, ..... Januari 2019</td>
                     </tr>
                     <tr>
                         <td colspan="2" width="60%">Panitia/Pejabat Penerima Hasil Pekerjaan</td>
                         <td width="40%" style="text-align:center;">Ketua Karang Werdha "'.$nama_karwed.'"</td>
                     </tr>
                     <tr>
                         <td colspan="2" width="60%">1. M. RIFAI</td>
                         <td width="40%" style="text-align:center;">Kelurahan '.$kelurahan.'</td>
                     </tr>
                     <tr>
                         <td  width="25%">Pengatur</td>
                         <td width="35%"> .................... </td>
                         <td width="40%"></td>
                     </tr>
                     <tr>
                         <td colspan="2" width="60%"> NIP. 19670424 200901 1 001</td>
                         <td width="40%"></td>

                     </tr>
                     <tr>
                         <td colspan="2" width="60%">2. RUDIYANTO, S.H, S.K.H, MM</td>
                         <td width="40%"></td>

                     </tr>
                     <tr>
                         <td width="25%">Penata / III c</td>
                         <td width="35%"> .................... </td>
                         <td width="40%"></td>
                     </tr>
                     <tr>
                         <td colspan="2" width="60%">NIP. 197106031994021001</td>
                         <td width="40%"></td>

                     </tr>
                     <tr>
                         <td colspan="2" width="60%">3. JHONNY MANULLANG, A.Md</td>
                         <td width="40%" style="text-align:center;">'.$user->nama_ketua.'</td>
                     </tr>
                     <tr>
                         <td width="25%">Penata Muda / III a </td>
                         <td width="35%"> ....................</td>
                         <td width="40%"></td>
                     </tr>
                     <tr>
                         <td colspan="2" width="50%">NIP. 197201092009011002</td>
                         <td>
                         <img src="'.url_qrcode("300x300",$url_qr).'" alt="My QR code" height="70px" width="70px">
                        </td>
                         
                     </tr>
                     </table>';
                     if(!$aneh)
                     {
                         $html.='<br><br>'.$ttd;
                     }
                     PDF::AddPage('L', 'A4');

                     PDF::writeHTML(cetak_css().$html, true, false, true, false, '');
                     PDF::lastPage();
                     if($aneh)
                     {
                         $tr_lanjut='<table style="border: 1px solid black;border-collapse: collapse;text-align:center;" class="font-kecil">'.$tr_lanjut.'</table><br><br>'.$ttd;
                         PDF::AddPage('L', 'A4');
                         PDF::writeHTML(cetak_css().$tr_lanjut, true, false, true, false, '');
                     }
                     // ---------------------------------------------------------
                     PDF::lastPage();

                 }
            }

        }

        $html = '
            <h5 style="text-align:center;"><b>REKAPITULASI JUMLAH PERMAKANAN<br>KARANG WERDHA "'.$nama_karwed.'"<br>KELURAHAN '.$kelurahan.' KECAMATAN ASEMROWO<br>UNTUK BULAN : '.BULAN[$bulan-1].' '.$tahun.'</b></h5>
            <hr color="black">
            </br><br>
            <table width="100%" border="1" style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; ">
            <tr>
                <th>NO</th>
                <th>TANGGAL</th>
                <th>JUMLAH PENERIMA</th>
                <th>HARGA PERMAKANAN</th>
                <th>JUMLAH + PPN 10%</th>
                <th>JENIS MENU</th>
                <th>KETERANGAN</th>
            </tr>
            <tr>
                <td >1</td>
                <td>1 Januari 2019</td>
                <td>76 Orang</td>
                <td>Rp. 11.000,00</td>
                <td>Rp. 919.600,00</td>
                <td><ol><li>Nasi</li><li>Telur Bumbu Bali</li><li>Tahu Bumbu Bali</li></ol></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">JUMLAH</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </table>
            <br><br>
            <table width="100%">
            <tr>
                <td colspan="2" width="60%"></td>
                <td width="40%" style="text-align:center;">Surabaya, ..... Januari 2019</td>
            </tr>
            <tr>
                <td colspan="2" width="60%">Panitia/Pejabat Penerima Hasil Pekerjaan</td>
                <td width="40%" style="text-align:center;">Ketua Karang Werdha "'.$nama_karwed.'"</td>
            </tr>
            <tr>
                <td colspan="2" width="60%">1. M. RIFAI</td>
                <td width="40%" style="text-align:center;">Kelurahan '.$kelurahan.'</td>
            </tr>
            <tr>
                <td  width="25%">Pengatur</td>
                <td width="35%"> .................... </td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td colspan="2" width="60%"> NIP. 19670424 200901 1 001</td>
                <td width="40%"></td>

            </tr>
            <tr>
                <td colspan="2" width="60%">2. RUDIYANTO, S.H, S.K.H, MM</td>
                <td width="40%"></td>

            </tr>
            <tr>
                <td width="25%">Penata / III c</td>
                <td width="35%"> .................... </td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td colspan="2" width="60%">NIP. 197106031994021001</td>
                <td width="40%"></td>

            </tr>
            <tr>
                <td colspan="2" width="60%">3. JHONNY MANULLANG, A.Md</td>
                <td width="40%" style="text-align:center;">'.$user->nama_ketua.'</td>
            </tr>
            <tr>
                <td width="25%">Penata Muda / III a </td>
                <td width="35%"> ....................</td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td colspan="2" width="60%">NIP. 197201092009011002</td>
                <td width="40%"></td>

            </tr>
            </table>
        ';
        // PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('Rekapitulasi.pdf', 'I');
    }

    public function download_beritakirimpanti()
    {
        $user = Auth::user();

        PDF::SetMargins(15, 15, 15);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', '12');

        $html = '
            <table width="100%" style="text-align:center;">
                <tr>
                    <td style="border-bottom-width:1px;"><b>BERITA ACARA PENYERAHAN DAN PEMERIKSAAN BARANG</b></td>
                </tr>
                <tr>
                    <td>Nomor : ......................................................................</td>
                </tr>
            </table>
            <br><br>
            <table width="100%">
            <tr>
                <td colspan="4" style="text-align:justify; line-height:1.5;">Pada hari ini .......... Tanggal .......... Bulan .......... Tahun 2019 kami yang bertanda tangan dibawah ini :</td>
            </tr>
            <tr>
            <td width="5%"></td>
            <td width="10%">Nama</td>
            <td width="5%">:</td>
            <td width="80%">MARIATUL KHIPTIAH</td>
            </tr>
            <tr>
            <td width="5%"></td>
            <td width="10%">Alamat</td>
            <td width="5%">:</td>
            <td width="80%">JL RAYA NGINDEN 69</td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:justify; line-height:1.5;">Selaku petugas kirim/ Penerima Hasil Pekerjaan penyelenggaraan program Pemberian permakanan bagi Anak Yatim, Piatu dan Yatim Piatu dari Keluarga Miskin dan Terlantar di PANTI ASUHAN AL KAHFI berdasarkan Surat Keputusan Kepala Dinas Sosial Kota Surabaya Nomor : 188.4/12824/436.7.7/2018 tanggal 31 Desember 2018 tentang Penetapan Petugas Kirim Kegiatan Pemenuhan Kebutuhan Dasar Permakanan di Kota Surabaya Tahun 2019 yang selanjutnya disebut sebagai PIHAK KESATU.</td>
            </tr>
            <tr>
            <td width="5%"></td>
            <td width="10%">Nama</td>
            <td width="5%">:</td>
            <td width="80%">Hj. SOEWASIH</td>
            </tr>
            <tr>
            <td width="5%"></td>
            <td width="10%">Jabatan</td>
            <td width="5%">:</td>
            <td width="80%">Ketua PANTI ASUHAN AL KAHFI</td>
            </tr>
            <tr>
            <td width="5%"></td>
            <td width="10%">Alamat</td>
            <td width="5%">:</td>
            <td width="80%">Jl. Nginden Baru IV/22</td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:justify; line-height:1.5;">Selaku Ketua Panti Asuhan/ Yayasan penyelenggaraan Program Pemberian Permakanan bagi Anak Yatim, Piatu dan Yatim Piatu dari Keluarga Miskin dan Terlantar, yang selanjutnya disebut sebagai PIHAK KEDUA.</td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:justify; line-height:1.5;">Telah mengadakan serah terima dan pemeriksaan barang hasil Pekerjaan Penyelenggaraan Program Pemberian Permakanan bagi Anak Yatim, Piatu dan Yatim Piatu dari Keluarga Miskin dan Terlantar dengan ini menyatakan bahwa  :</td>
            </tr>
            <tr>
                <td colspan="4"><ol>
            <li style="text-align:justify; line-height:1.5;">PIHAK KEDUA melakukan penyerahan hasil pekerjaan berupa Permakanan bagi Anak Yatim, Piatu dan Yatim Piatu kepada PIHAK KESATU dalam keadaan baik sejumlah ……… Kotak selama 1 bulan.</li>
            <li style="text-align:justify; line-height:1.5;">PIHAK KESATU menerima dan memeriksa hasil pekerjaan dari PIHAK KEDUA seperti dimaksud pada poin 1 (satu) dan selanjutnya PIHAK KESATU mengirimkan kepada Penerima Manfaat. sejumlah ……… Kotak selama 1 bulan.</li>
            <li style="text-align:justify; line-height:1.5;">Terhadap hal-hal yang berkenaan dengan setelah serah terima barang hasil pekerjaan ini maka menjadi tanggungjawab PIHAK KEDUA.</li>
            <li style="text-align:justify; line-height:1.5;">Berita acara dan lampiran ini merupakan satu kesatuan yang tidak terpisahkan.</li>
            </ol></td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:justify; line-height:1.5; text-indent: 10em; ">Demikian berita acara ini dibuat dengan sebenar-benarnya untuk dipergunakan sebagaimana mestinya.</td>
            </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                <tr>
                    <td>PIHAK KEDUA</td>
                    <td>PIHAK KESATU</td>
                </tr>
                <tr>
                    <td>PANTI ASUHAN AL KAHFI</td>
                    <td>Petugas Kirim</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>( HJ. SOEWASIH)</td>
                    <td>( MARIATUL KHIPTIAH )</td>
                </tr>
            </table>

        ';
        $html2 = '
        <table width="100%" style="text-align:center;">
            <tr>
                <td style="border-bottom-width:1px;"><b>LAMPIRAN BERITA ACARA PENYERAHAN DAN PEMERIKSAAN BARANG</b></td>
            </tr>
            <tr>
                <td>Nomor : ......................................................................</td>
            </tr>
        </table>
        <br><br>
        <table width="100%" border="1" style="text-align: center; vertical-align: middle;">
        <tr>
            <th width="5%" rowspan="2">No</th>
            <th rowspan="2">Tanggal Pengiriman</th>
            <th rowspan="2">Jumlah Anak/ Kotak</th>
            <th rowspan="2">Jumlah Alamat</th>
            <th colspan="2" rowspan="2">Menu Makanan</th>
            <th colspan="2">Tanda Tangan</th>
        </tr>
        <tr>
            <td>Ketua</td>
            <td>Petugas Kirim</td>
        </tr>
        <tr>
            <td rowspan="3">1</td>
            <td rowspan="3">......</td>
            <td rowspan="3">... Kotak</td>
            <td rowspan="3">... Alamat</td>
            <td>Nasi</td>
            <td>Soup</td>
            <td rowspan="3"></td>
            <td rowspan="3"></td>
        </tr>
        <tr>
            <td>Perkedel Daging</td>
            <td>Semangka</td>
        </tr>
        <tr>
            <td>Tahu bb Kecap</td>
            <td>Air Mineral</td>
        </tr>
        <tr>
            <td colspan="2">Total</td>
            <td>... Kotak</td>
            <td>... Alamat</td>
            <td colspan="4"></td>
        </tr>
        </table>
        <br><br>
        <table>
            <tr>
                <td colspan="2" style="text-indent: 10em">Demikian berita acara ini dibuat dengan sebenar-benarnya untuk dipergunakan sebagaimana mestinya.</td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td>PANTI ASUHAN AL KAHFI</td>
                <td>Petugas Kirim</td>
            </tr>
            <tr>
                <td>Ketua</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td>(HJ. SOEWASIH)</td>
                <td>( MARIATUL KHIPTIAH )</td>
            </tr>
        </table>
        ';
        $html3 = '
            <table>
                <tr>
                    <td><u>UNTUK KELENGKAPAN INI TETAP SAMA :</u></td>
                </tr>
                <tr>
                    <td>
                        <ol>
                            <li>SURAT PERNYATAAN</li>
                            <li>DATA ANAK TOTAL</li>
                            <li>DATA ANAK ALAMAT YANG SAMA</li>
                        </ol>
                    </td>
                </tr>
            </table>
        ';
        PDF::SetTitle('Panti Berita Acara Petugas Kirim Baru 2019');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html2, true, false, true, false, '');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html3, true, false, true, false, '');
        PDF::Output('Berita Kirim Panti.pdf', 'I');
    }

    public function download_spjpanti()
    {
        $user = Auth::user();

        PDF::SetMargins(10, 10, 10);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', '12');

        $html = '
            <table width="100%">
                <tr>
                    <td rowspan="19" bgcolor="#bdbac1" width="10%;" style="border-right-width:1;border-left-width:1;border-bottom-width:1;border-top-width:1;"></td>
                    <td colspan="12" align="center" width="90%;" style="border-right-width:1;border-top-width:1;"><h2>KUITANSI</h2></td>
                </tr>
                <tr>
                    <td style="border-bottom-width:1;" colspan="2" width="20%">NO</td>
                    <td style="border-right-width:1;" colspan="10" width="70%" ></td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" >Telah Terima Dari</td>
                    <td width="2%">:</td>
                    <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Pejabat Pembuat Komitmen Dinas Sosial Kota Surabaya</td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" >Uang Sejumlah</td>
                    <td width="2%">:</td>
                    <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%"><h4>Enam Juta Dua Ratus Sembilan Puluh Dua Ribu Rupiah</h4></td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" >Untuk Pembayaran</td>
                    <td width="2%">:</td>
                    <td style="border-right-width:1; border-bottom-width:1; text-align:justify;" colspan="9" width="68%">Belanja Makanan dan Minuman untuk masyarakat dalam rangka Pemenuhan Kebutuhan Dasar Permakanan Bagi anak yatim, piatu dan yatim piatu <b> Panti Asuhan Yayasan Sedekah Membawa Berkah </b> dengan Kode Kegiatan <b> 1.1.1.06.01.0002 </b> dengan perincian sebagai berikut :</td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" ></td>
                    <td width="2%"></td>
                    <td style="" colspan="4" width="33%">Tanggal 01 s/d 10 Pebruari 2019 </td>
                    <td style="border-bottom-width: 1" width="2%" rowspan="2">:</td>
                    <td style="border-right-width: 1; border-bottom-width: 1" colspan="4" width="33%" rowspan="2">Rp.    6,820,000,-</td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" ></td>
                    <td width="2%"></td>
                    <td style="border-bottom-width: 1" colspan="4" width="33%">(62 Anak x 10 hari x Rp.11.000,-)</td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" ></td>
                    <td width="2%" rowspan="2"></td>
                    <td style="border-bottom-width: 1" colspan="4" width="33%" rowspan="2">PPN / SPD 10%</td>
                    <td style="border-bottom-width: 1" width="2%" rowspan="2">:</td>
                    <td style="border-right-width: 1; border-bottom-width: 1" colspan="4" width="33%" rowspan="2">Rp.    682,000,-</td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td colspan="8"></td>
                    <td style="border-right-width:1;" colspan="4">Surabaya,</td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>

                    <td colspan="8"></td>
                    <td style="border-right-width:1;" colspan="4"  align="center"><b>Ketua</b></td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td style="border-top-width:1; border-bottom-width:1;" bgcolor="#e6e6e6" ><h3>Rp.</h3></td>
                    <td style="border-top-width:1; border-bottom-width:1;" colspan="4" bgcolor="#e6e6e6" ><h3>7,502,000,-</h3></td>
                    <td style="border-right-width:1;" colspan="7"></td>
                </tr>
                <tr>
                    <td colspan="8"></td>
                    <td style="border-right-width:1;" colspan="4" align="center"><b><u>NIA TUL FITRI</u></b></td>
                </tr>
                <tr>
                    <td style="border-right-width:1; border-bottom-width:1;" colspan="12"></td>
                </tr>
            </table>
            <br>
            <br>
            <br>
            <table width="100%">
                <tr>
                    <td rowspan="19" bgcolor="#bdbac1" width="10%;" style="border-right-width:1;border-left-width:1;border-bottom-width:1;border-top-width:1;"></td>
                    <td colspan="12" align="center" width="90%;" style="border-right-width:1;border-top-width:1;"><h2>KUITANSI</h2></td>
                </tr>
                <tr>
                    <td style="border-bottom-width:1;" colspan="2" width="20%">NO</td>
                    <td style="border-right-width:1;" colspan="10" width="70%" ></td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" >Telah Terima Dari</td>
                    <td width="2%">:</td>
                    <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Pejabat Pembuat Komitmen Dinas Sosial Kota Surabaya</td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" >Uang Sejumlah</td>
                    <td width="2%">:</td>
                    <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%"><h4>Enam Juta Dua Ratus Sembilan Puluh Dua Ribu Rupiah</h4></td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" >Untuk Pembayaran</td>
                    <td width="2%">:</td>
                    <td style="border-right-width:1; border-bottom-width:1; text-align:justify;" colspan="9" width="68%">Belanja Makanan dan Minuman untuk masyarakat dalam rangka Pemenuhan Kebutuhan Dasar Permakanan Bagi anak yatim, piatu dan yatim piatu <b> Panti Asuhan Yayasan Sedekah Membawa Berkah </b> dengan Kode Kegiatan <b> 1.1.1.06.01.0002 </b> dengan perincian sebagai berikut :</td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" ></td>
                    <td width="2%"></td>
                    <td style="" colspan="4" width="33%">Tanggal 18 s/d 28 Pebruari 2019 </td>
                    <td style="border-bottom-width: 1" width="2%" rowspan="2">:</td>
                    <td style="border-right-width: 1; border-bottom-width: 1" colspan="4" width="33%" rowspan="2">Rp.    12,276,000,-</td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" ></td>
                    <td width="2%"></td>
                    <td style="border-bottom-width: 1" colspan="4" width="33%">(62 Anak x 18 hari x Rp.11.000,-)</td>
                </tr>
                <tr>
                    <td colspan="2" width="20%" ></td>
                    <td width="2%" rowspan="2"></td>
                    <td style="border-bottom-width: 1" colspan="4" width="33%" rowspan="2">PPN / SPD 10%</td>
                    <td style="border-bottom-width: 1" width="2%" rowspan="2">:</td>
                    <td style="border-right-width: 1; border-bottom-width: 1" colspan="4" width="33%" rowspan="2">Rp.    1,227,600,-</td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td colspan="8"></td>
                    <td style="border-right-width:1;" colspan="4">Surabaya,</td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>

                    <td colspan="8"></td>
                    <td style="border-right-width:1;" colspan="4"  align="center"><b>Ketua</b></td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td style="border-right-width:1;" colspan="12"></td>
                </tr>
                <tr>
                    <td style="border-top-width:1; border-bottom-width:1;" bgcolor="#e6e6e6" ><h3>Rp.</h3></td>
                    <td style="border-top-width:1; border-bottom-width:1;" colspan="4" bgcolor="#e6e6e6" ><h3>13,503,600,-</h3></td>
                    <td style="border-right-width:1;" colspan="7"></td>
                </tr>
                <tr>
                    <td colspan="8"></td>
                    <td style="border-right-width:1;" colspan="4" align="center"><b><u>NIA TUL FITRI</u></b></td>
                </tr>
                <tr>
                    <td style="border-right-width:1; border-bottom-width:1;" colspan="12"></td>
                </tr>
            </table>
            <br>
            <br>
            <br>
        ';

        $html2 = '
            <br><br><br>
            <table>
            <tr>
                <td width="300"></td>
                <td>Surabaya, .................</td>
            </tr>
            <tr>
                <td width="60"></td>
                <td width="240"></td>
                <td>Kepada Yth, </td>
            </tr>
            <tr>
                <td width="60">Nomor</td>
                <td width="240">: '."".'</td>
                <td >Pejabat Pembuat Komitmen</td>
            </tr>
            <tr>
                <td width="60">Lampiran</td>
                <td width="240">: '."1 (satu)".'</td>
                <td>Penyelenggaraan Kegiatan</td>
            </tr>
            <tr>
                <td width="60">Perihal</td>
                <td width="240">: '."Permohonan Pencairan".'</td>
                <td>Pemenuhan kebutuhan dasar</td>
            </tr>
            <tr>
                <td width="60"></td>
                <td width="240"> <u>'."Dana Permakanan".'</u></td>
                <td>permakanan bagi anak yatim, <br>Piatu dan Yatim Piatu <br>di- <br><u>SURABAYA</u></td>
            </tr>
            </table>
            <br>
            <br>
            <br>
            <table>
            <tr>
                <td style="text-align:justify;" width="500"><p style="text-indent: 10em; line-height:2;">Berkenaan dengan Program Penyelenggaraan Kegiatan Pemenuhan kebutuhan dasar permakanan bagi anak yatim, piatu, dan yatim piatu di Kota Surabaya, bersama ini kami mengajukan Permohonan Pencairan Dana Permakanan untuk Anak Yatim, Piatu dan Yatim Piatu mulai tanggal 01 sampai dengan 28 Pebruari  2019 sebesar Rp 21,005,600,- (Dua Puluh Satu Juta Lima Ribu Enam Ratus Rupiah), dengan rincian sebagai berikut  : </p></td>
            </tr>
            </table>
            <br>
            <br>
             <br>
            <table width="100%">
                <tr>
                    <th style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height:2;"><b>Jumlah Penerima Manfaat</b></th>
                    <th style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height:2;"><b>Jumlah Hari</b></th>
                    <th style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height:2;"><b>Harga Permakanan</b></th>
                    <th style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Jumlah</b></th>
                </tr>
                <tr>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>62 Orang</b></td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>28 Hari</b></td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Rp. 11.000,-</b></td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Rp. 19,096,000,-</b></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;">PPN / SPD 10%</td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Rp. 1,909,600,-</b></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Jumlah Total</b></td>
                    <td style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; line-height: 30px;"><b>Rp. 21,005,600,-</b></td>
                </tr>
            </table>
            <br><br><br>
            <table>
                <tr>
                <td width="500"><p style="text-indent: 10em; line-height:2;">Demikian atas bantuan dan kerjasamanya disampaikan terima kasih.</p></td>
                </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                    <tr>
                      <td ></td>
                      <td >Surabaya, ...................................</td>
                    </tr>
                    <tr>
                      <td >Mengetahui</td>
                      <td >Panti Asuhan Yayasan Sedekah Membawa Berkah</td>
                    </tr>
                    <tr>
                      <td>Lurah Kemayoran</td>
                      <td>Ketua</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td>( …………………………….. )</td>
                      <td><b><u>NIA TUL FITRI</u></b></td>
                    </tr>
            </table>
            <br><br>
            <table>
                <tr>
                <td width="500"><p><u>Tembusan:</u></p></td>
                </tr>
                <tr>
                <td width="500"><p>Bpk. Kepala Dinas Sosial Kota Surabaya</p></td>
                </tr>
            </table>
        ';

        $html3 ='
            <br><br>
            <table width="100%" style="text-align:center;">
                <tr>
                    <td><b>SURAT PERNYATAAN KESANGGUPAN MENYELESAIKAN KEGIATAN PEMBERIAN <br>
                    PERMAKANAN  BAGI ANAK YATIM, PIATU DAN YATIM PIATU DARI KELUARGA MISKIN <br>
                    DAN TERLANTAR</b><br>
                    NOMOR : ......................................................................
                    </td>
                </tr>
            </table>
            <br><br><br>
            <span>Pada hari ini ……………  tanggal …….. bulan  …………..  Tahun  ………….  Yang bertanda tangan , dibawah ini :</span><br><br>
            <table width="100%" >
            <tr>
                <td width="30%">Nama ketua Panti/Yayasan</td>
                <td width="2%">:</td>
                <td width="68%" style="border-bottom-width: 1; height: 20"><b>NIA TUL FITRI</b></td>
            </tr>
            <tr>
                <td width="30%">Jabatan</td>
                <td width="2%">:</td>
                <td width="68%" style="border-bottom-width: 1; height:20;">Ketua Panti Asuhan Yayasan Sedekah Membawa Berkah, </td>
            </tr>
            </table>
            <br><br>
            <span>Dengan ini menyatakan bahwa:</span>
            <ol>
            <li style="text-align:justify; line-height:1.5;">Sanggup melaksanakan pekerjaan penyelenggaraan kegiatan pemenuhan kebutuhan dasar  permakanan bagi Anak Yatim, Piatu, dan Yatim Piatu dari Keluarga Miskin dan Terlantar terhitung mulai tanggal …….. Sampai tanggal …… bulan ………………  Tahun …………..</li>
            <li style="text-align:justify; line-height:1.5;">Nama anak Anak Yatim, Piatu dan Yatim Piatu yang menerima Permakanan sudah sesuai dengan nama yang tercantum dalam permohonan dana dan SPJ (Surat Pertanggungjawaban )</li>
            <li style="text-align:justify; line-height:1.5;">Harga permakanan adalah paling tinggi sebesar Rp. 11.000,- per orang dengan dibayar PPH Pasal 23.</li>
            <li style="text-align:justify; line-height:1.5;">Pemberian permakanan diberikan dalam bentuk makanan dan sudah sesuai dengan standart menu yang ditetapkan</li>
            <li style="text-align:justify; line-height:1.5;">Pemberian permakanan dilakukan setiap hari tanpa pernah diliburkan.</li>
            <li style="text-align:justify; line-height:1.5;">Membuat berita acara perubahan/ pergantian anak yatim, piatu dan yatim piatu apabila terjadi perubahan atau pergantian penerima manfaat dalam bulan berkenaan.</li>
            </ol>
            <table width="100%" >
            <tr>
            <td style="text-align:justify; width="85%""><p style="text-indent: 10em; ">Demikian surat pernyataan ini dibuat dengan sebenar-benarnya dan penuh tanggung jawab.</p></td>
            </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                    <tr>
                      <td></td>
                      <td >Surabaya, ................................................</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td ></td>
                      <td >Ketua Panti/Yayasan</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><b><u>( NIA TUL FITRI )</u></b></td>
                    </tr>
            </table>
        ';

        $html4 = '
            <br><br><br>
            <table width="100%" style="text-align:center;">
                <tr>
                    <td><b><u>SURAT PERNYATAAN TANGGUNG JAWAB BELANJA MUTLAK</u></b></td>
                </tr>
                <tr>
                    <td>Nomor : ......................................................................</td>
                </tr>
            </table>
            <br><br><br>
            <span>Yang bertanda tangan dibawah ini :</span><br>
            <table width="100%" >
            <tr>
                <td width="10%">Nama</td>
                <td width="2%">:</td>
                <td width="88%">NIA TUL FITRI</td>
            </tr>
            <tr>
                <td width="10%">Jabatan</td>
                <td width="2%">:</td>
                <td width="88%">Ketua Panti Asuhan Yayasan Sedekah Membawa Berkah</td>
            </tr>
            </table>
            <br><br><br>
            <span>Menyatakan dengan sesungguhnya bahwa  :</span>
            <ol>
            <li style="text-align:justify; line-height:1.5;">Bersedia untuk bertanggungjawab mutlak Penyelenggaraan Kegiatan Pemenuhan Kebutuhan Dasar Permakanan Bagi Anak Yatim, Piatu, dan Yatim Piatu dari Keluarga Miskin dan Terlantar sesuai dengan perjanjian antara Dinas Sosial (Pejabat Pembuat Komitmen) Dengan  Ketua Panti Asuhan Yayasan Sedekah Membawa Berkah Kelurahan Keputih Kecamatan Sukolilo Nomor SPK : 460/0022.78/436.7.7/20019 Tanggal 02 Januari 2019</li>
            <li style="text-align:justify; line-height:1.5;">Apabila dikemudian hari terdapat kelebihan anggaran atas kegiatan pemberian Permakanan tersebut diatas, kami bersedia untuk menanggung secara pribadi dan menyetorkannya ke kas Daerah melalui Dinas Sosial.</li>
            </ol>
            <table width="100%" >
            <tr>
            <td style="text-align:justify; width="85%""><p style="text-indent: 10em; ">Demikian surat pernyataan ini dibuat dengan sebenar-benarnya dan tanpa ada unsur paksaan dari pihak manapun</p></td>
            </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                    <tr>
                      <td></td>
                      <td >Surabaya, ................................................</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td ></td>
                      <td >Panti Asuhan Yayasan Sedekah Membawa Berkah</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Ketua</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>( NIA TUL FITRI )</td>
                    </tr>
            </table>
        ';

        $html = '
            <h5 style="text-align:center;"><b>REKAPITULASI JUMLAH PERMAKANAN<br>PANTI ASUHAN YATIM PIATU KARIMAH<br>KELURAHAN PAKIS  KECAMATAN SAWAHAN<br>UNTUK BULAN : JANUARI 2019</b></h5>
            <hr color="black">
            </br><br>
            <table width="100%" border="1" style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; ">
            <tr>
                <th>NO</th>
                <th>TANGGAL</th>
                <th>JUMLAH PENERIMA</th>
                <th>HARGA PERMAKANAN</th>
                <th>JUMLAH + PPN 10%</th>
                <th>JENIS MENU</th>
                <th>KETERANGAN</th>
            </tr>
            <tr>
                <td >1</td>
                <td>1 Januari 2019</td>
                <td>76 Orang</td>
                <td>Rp. 11.000,00</td>
                <td>Rp. 919.600,00</td>
                <td><ol><li>Nasi</li><li>Telur Bumbu Bali</li><li>Tahu Bumbu Bali</li></ol></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">JUMLAH</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </table>
            <br><br>
            <table width="100%">
            <tr>
                <td colspan="2" width="60%"></td>
                <td width="40%" style="text-align:center;">Surabaya, ..... Januari 2019</td>
            </tr>
            <tr>
                <td colspan="2" width="60%">Panitia/Pejabat Penerima Hasil Pekerjaan</td>
                <td width="40%" style="text-align:center;">Ketua Panti Asuhan …………………………..</td>
            </tr>
            <tr>
                <td colspan="2" width="60%">1.JHONNY MANULLANG, A.Md</td>
                <td width="40%" style="text-align:center;"></td>
            </tr>
            <tr>
                <td  width="25%">Penata Muda / III a </td>
                <td width="35%"> .................... </td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td colspan="2" width="60%">NIP. 197201092009011002</td>
                <td width="40%"></td>

            </tr>
            <tr>
                <td colspan="2" width="60%">2. RUDIYANTO, S.H, S.K.H, MM</td>
                <td width="40%"></td>

            </tr>
            <tr>
                <td width="25%">Penata / III c</td>
                <td width="35%"> .................... </td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td colspan="2" width="60%">NIP. 197106031994021001</td>
                <td width="40%"></td>

            </tr>
            <tr>
                <td colspan="2" width="60%">M. RIFAI</td>
                <td width="40%" style="text-align:center;">(………………………………….)</td>
            </tr>
            <tr>
                <td width="25%">Pengatur</td>
                <td width="35%"> ....................</td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td colspan="2" width="60%">NIP. 196704242009011001</td>
                <td width="40%"></td>

            </tr>


            </table>
        ';

        $html6 = '
            <h5 align="center">DOKUMENTASI PERMAKANAN YAYASAN . PANTI ASUHAN ……………………</h5>
        ';

        PDF::SetTitle('SPJ Panti');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html2, true, false, true, false, '');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html3, true, false, true, false, '');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html4, true, false, true, false, '');
        PDF::AddPage('L', 'A4');
        PDF::writeHTML($html5, true, false, true, false, '');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html6, true, false, true, false, '');
        PDF::Output('SPJ Panti.pdf', 'I');
    }

    public function download_rekapspjpanti()
    {
        $user = Auth::user();

        PDF::SetMargins(5, 10, 5);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', '12');

        $html = '
            <h5 align="center">REKAPITULASI PERMAKANAN ANAK YATIM,PIATU DAN YATIM PIATU <br>
                BULAN : JANUARI 2019 <br>
                SURABAYA BARAT
            </h5>
            <table style="text-align: center; vertical-align: middle; line-height: 1.5;" width="100%" border="1">
                <tr>
                    <td rowspan="2" width="3%">No</td>
                    <td rowspan="2">Kecamatan</td>
                    <td rowspan="2">ID<br> Pekerjaan</td>
                    <td rowspan="2">Nama Panti</td>
                    <td rowspan="2">Nomor<br>Rekening</td>
                    <td rowspan="2">Jumlah<br>Orang</td>
                    <td rowspan="2">Jumlah<br>Hari</td>
                    <td colspan="2">SPK</td>
                    <td rowspan="2">Jumlah<br>Kotor</td>
                    <td rowspan="2">Pajak<br>Daerah</td>
                    <td rowspan="2">PPh<br>Pasal 23</td>
                    <td rowspan="2">Jumlah<br>Transfer</td>
                </tr>
                <tr>
                    <td>Nomor</td>
                    <td>Tanggal</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color: #e0e0eb;" colspan="13"></td>
                </tr>
            </table>
            <br><br><br>
            <table width="100%" style="text-align: center;">
                <tr>
                    <td>Pejabat Pembuat Komitmen</td>
                    <td colspan="2"></td>
                    <td>Pejabat Pelaksana Teknis Kegiatan</td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"></td>
                    <td></td>
                </tr>
                <tr>
                    <td><u>Eny Zuliati, SE, MM</u></td>
                    <td colspan="2"></td>
                    <td><u>Sunarko, S.Ag., M.Si</u></td>
                </tr>
                <tr>
                    <td>Pembina</td>
                    <td colspan="2"></td>
                    <td>Penata</td>
                </tr>
                <tr>
                    <td>NIP. 19630515 199003 2 010</td>
                    <td colspan="2"></td>
                    <td>NIP. 19740110 200901 1 002</td>
                </tr>
            </table>
        ';
        PDF::SetTitle('Rekapitulasi SPJ Panti');
        PDF::AddPage('L', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('RekapitulasiSPJPanti.pdf', 'I');
    }

    public function download_kwitansiipsm()
    {
        $user = Auth::user();

        PDF::SetMargins(5, 10, 5);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', '12');

        $html = '
        <table width="100%" >
            <tr>
                <td rowspan="19" bgcolor="#bdbac1" width="10%;" style="border-right-width:1;border-left-width:1;border-bottom-width:1;border-top-width:1;"></td>
                <td colspan="12" align="center" width="90%;" style="border-right-width:1;border-top-width:1;"><h2>KUITANSI</h2></td>
            </tr>
            <tr>
                <td style="border-bottom-width:1;" colspan="2" width="20%">NO</td>
                <td style="border-right-width:1;" colspan="10" width="70%" ></td>
            </tr>
            <tr>
                <td colspan="2" width="20%" >Telah Terima Dari</td>
                <td width="2%">:</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Pejabat Pembuat Komitmen Penyelenggaraan Kegiatan Pemenuhan <br> Kebutuhan Dasar Permakanan  Bagi Penyandang Disabilitas dan Penyakit <br> Tertentu </td>
            </tr>
            <tr>
                <td colspan="2" width="20%" >Uang Sejumlah</td>
                <td width="2%">:</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%"><h4>Enam Juta Dua Ratus Sembilan Puluh Dua Ribu Rupiah</h4></td>
            </tr>
            <tr>
                <td colspan="2" width="20%" >Untuk Pembayaran</td>
                <td width="2%">:</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Penyelenggaraan  Kegiatan Pemenuhan Kebutuhan Dasar Permakanan</td>
            </tr>
            <tr>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="12">Bagi Penyandang Disabilitas dan Penyakit Tertentu  di kelurahan Putat Jaya Untuk Bulan</td>
            </tr>
            <tr>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="12">Februari  2019 dengan Kode Kegiatan  1.1.1.06.01.0004</td>
            </tr>
            <tr>
                <td style="border-bottom-width:1;" colspan="2" width="20%" >Rincian Transfer Tgl</td>
                <td style="border-bottom-width:1;" width="2%">:</td>
                <td style="border-bottom-width:1;" width="5%">Rp.</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="8" width="63%"></td>
            </tr>
            <tr>
                <td style="border-bottom-width:1;" colspan="2" width="20%" >PPN / SPD 10%</td>
                <td style="border-bottom-width:1;" width="2%">:</td>
                <td style="border-bottom-width:1;" width="5%">Rp.</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="8" width="63%"></td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>
                <td colspan="8"></td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="4">Surabaya,</td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>

                <td colspan="8"></td>
                <td style="border-right-width:1;" colspan="4"  align="center"></td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>
                <td style="border-top-width:1; border-bottom-width:1;" bgcolor="#e6e6e6" ><h3>Rp.</h3></td>
                <td style="border-top-width:1; border-bottom-width:1;" colspan="4" bgcolor="#e6e6e6" ><h3>6.292.000,00</h3></td>
                <td style="border-right-width:1;" colspan="7"></td>
            </tr>
            <tr>
                <td colspan="8"></td>
                <td style="border-right-width:1;" colspan="4" align="center"></td>
            </tr>
            <tr>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="12"></td>
            </tr>
        </table>
        <br>
        <br>
        <br>
                <table width="100%" >
            <tr>
                <td rowspan="19" bgcolor="#bdbac1" width="10%;" style="border-right-width:1;border-left-width:1;border-bottom-width:1;border-top-width:1;"></td>
                <td colspan="12" align="center" width="90%;" style="border-right-width:1;border-top-width:1;"><h2>KUITANSI</h2></td>
            </tr>
            <tr>
                <td style="border-bottom-width:1;" colspan="2" width="20%">NO</td>
                <td style="border-right-width:1;" colspan="10" width="70%" ></td>
            </tr>
            <tr>
                <td colspan="2" width="20%" >Telah Terima Dari</td>
                <td width="2%">:</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Pejabat Pembuat Komitmen Penyelenggaraan Kegiatan Pemenuhan <br> Kebutuhan Dasar Permakanan  Bagi Penyandang Disabilitas dan Penyakit <br> Tertentu </td>
            </tr>
            <tr>
                <td colspan="2" width="20%" >Uang Sejumlah</td>
                <td width="2%">:</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%"><h4>Enam Juta Dua Ratus Sembilan Puluh Dua Ribu Rupiah</h4></td>
            </tr>
            <tr>
                <td colspan="2" width="20%" >Untuk Pembayaran</td>
                <td width="2%">:</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="9" width="68%">Penyelenggaraan  Kegiatan Pemenuhan Kebutuhan Dasar Permakanan</td>
            </tr>
            <tr>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="12">Bagi Penyandang Disabilitas dan Penyakit Tertentu  di kelurahan Putat Jaya Untuk Bulan</td>
            </tr>
            <tr>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="12">Februari  2019 dengan Kode Kegiatan  1.1.1.06.01.0004</td>
            </tr>
            <tr>
                <td style="border-bottom-width:1;" colspan="2" width="20%" >Rincian Transfer Tgl</td>
                <td style="border-bottom-width:1;" width="2%">:</td>
                <td style="border-bottom-width:1;" width="5%">Rp.</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="8" width="63%"></td>
            </tr>
            <tr>
                <td style="border-bottom-width:1;" colspan="2" width="20%" >PPN / SPD 10%</td>
                <td style="border-bottom-width:1;" width="2%">:</td>
                <td style="border-bottom-width:1;" width="5%">Rp.</td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="8" width="63%"></td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>
                <td colspan="8"></td>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="4">Surabaya,</td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>

                <td colspan="8"></td>
                <td style="border-right-width:1;" colspan="4"  align="center"></td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>
                <td style="border-right-width:1;" colspan="12"></td>
            </tr>
            <tr>
                <td style="border-top-width:1; border-bottom-width:1;" bgcolor="#e6e6e6" ><h3>Rp.</h3></td>
                <td style="border-top-width:1; border-bottom-width:1;" colspan="4" bgcolor="#e6e6e6" ><h3>6.292.000,00</h3></td>
                <td style="border-right-width:1;" colspan="7"></td>
            </tr>
            <tr>
                <td colspan="8"></td>
                <td style="border-right-width:1;" colspan="4" align="center"></td>
            </tr>
            <tr>
                <td style="border-right-width:1; border-bottom-width:1;" colspan="12"></td>
            </tr>
        </table>
        ';

        PDF::SetTitle('Kwintansi IPSM');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('KwintansiIPSM.pdf', 'I');
    }

    public function download_rekappaca()
    {
        $user = Auth::user();

        PDF::SetMargins(5, 10, 5);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', '12');

        $html = '
            <h5 style="text-align:center;"><b>IPSM KELURAHAN MANYAR SABRANGAN<br>KECAMATAN MULYOREJO<br>UNTUK BULAN  DESEMBER 2018</b></h5>
            <hr color="black">
            </br><br>
            <table width="100%" border="1" style="text-align:center; vertical-align: middle; border: 1px solid black;border-collapse: collapse; ">
            <tr>
                <th width="5%">NO</th>
                <th width="15%">TANGGAL</th>
                <th width="15%">JUMLAH PENERIMA</th>
                <th width="15%">HARGA PERMAKANAN</th>
                <th width="15%">JUMLAH + PPN 10%</th>
                <th width="15%">JENIS MENU</th>
                <th width="20%">KETERANGAN</th>
            </tr>
            <tr>
                <td >1</td>
                <td>1 Januari 2019</td>
                <td>76 Orang</td>
                <td>Rp. 11.000,00</td>
                <td>Rp. 919.600,00</td>
                <td><ol><li>Nasi</li><li>Telur Bumbu Bali</li><li>Tahu Bumbu Bali</li></ol></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">JUMLAH</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </table>
            <br><br>
            <table width="100%">
            <tr>
                <td colspan="2" width="60%"></td>
                <td width="40%" style="text-align:center;">Surabaya, ..... Januari 2019</td>
            </tr>
            <tr>
                <td colspan="2" width="60%">Panitia/Pejabat Penerima Hasil Pekerjaan</td>
                <td width="40%" style="text-align:center;">Ketua IPSM Kelurahan Manyar Sabrangan</td>
            </tr>
            <tr>
                <td colspan="2" width="60%">1. Rudiyanto,S.H,S.K.H,MM</td>
                <td width="40%" style="text-align:center;"></td>
            </tr>
            <tr>
                <td  width="25%">Penata/III c</td>
                <td width="35%"> .................... </td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td colspan="2" width="60%">NIP.197106031994021001</td>
                <td width="40%"></td>

            </tr>
            <tr>
                <td colspan="2" width="60%">2. M.Rifai</td>
                <td width="40%"></td>

            </tr>
            <tr>
                <td width="25%">Pengatur</td>
                <td width="35%"> .................... </td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td colspan="2" width="60%">NIP.196704242009011001</td>
                <td width="40%"></td>

            </tr>
            <tr>
                <td colspan="2" width="60%">3. JHONNY MANULLANG, A.Md</td>
                <td width="40%" style="text-align:center;"><u>Siti Aisyah</u></td>
            </tr>
            <tr>
                <td width="25%">Penata Muda / III a </td>
                <td width="35%"> ....................</td>
                <td width="40%"></td>
            </tr>
            <tr>
                <td colspan="2" width="60%">NIP. 197201092009011002</td>
                <td width="40%"></td>

            </tr>


            </table>
        ';
        PDF::SetTitle('Rekapitulasi PACA');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('RekapitulasiPACA.pdf', 'I');
    }

    public function download_spjipsm()
    {
        $user = Auth::user();

        PDF::SetMargins(15, 15, 15);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', '12');

        $html = '
            <table width="100%">
                <tr>
                    <td align="center" width="10%"><img SRC="/images/ipsm.jpg" height="60" width="60"></td>
                    <td align="center" width="90%"><h1>IKATAN PEKERJA SOSIAL MASYARAKAT</h1><br>
                    <b>Kelurahan Kedung Baruk Kecamatan Rungkut</b><br>
                    </td>
                </tr>
            </table>
            <hr color="black">
            </br>
            <br>
            <table width="100%" style="text-align:center;">
                <tr>
                    <td>SURAT PERNYATAAN KESANGGUPAN MENYELESAIKAN PEKERJAAN <br>
                    PENYELENGGARAAN KEGIATAN PEMENUHAN KEBUTUHAN DASAR PERMAKANAN<br>
                    BAGI PENYANDANG DISABILITAS DAN PENYAKIT TERTENTU <br>
                    NOMOR : ......................................................................
                    </td>
                </tr>
            </table>
            <br><br><br>
            <span>Pada hari ini Jum’at  tanggal 1, bulan Februari  tahun 2019 yang bertanda tangan dibawah ini :</span><br><br>
            <table width="100%" >
            <tr>
                <td width="10%">Nama</td>
                <td width="2%">:</td>
                <td width="88%">Nur Fauzah</td>
            </tr>
            <tr>
                <td width="10%">Jabatan</td>
                <td width="2%">:</td>
                <td width="88%">Ketua IPSM Kelurahan Kedung Baruk Kecamatan Rungkut</td>
            </tr>
            </table>
            <br><br>
            <span>Dengan ini menyatakan bahwa:</span>
            <ol>
            <li style="text-align:justify; line-height:1.5;">Sanggup melaksanakan pekerjaan penyelenggaraan kegiatan pemenuhan kebutuhan dasar Permakanan bagi Penyandang Disabilitas dan Penyakit Tertentu di Kelurahan Kedung Baruk Kecamatan Rungkut terhitung mulai tanggal 1 sampai tanggal 28 bulan Februari  tahun 2019</li>
            <li style="text-align:justify; line-height:1.5;">Nama penyandang disabilitas dan penyakit tertentu yang menerima permakanan sesuai dengan nama yang tercantum dalam permohonan pencairan dana dan SPJ (Surat Pertanggungjawaban)</li>
            <li style="text-align:justify; line-height:1.5;">Harga permakanan adalah paling tinggi sebesar Rp 11.000,- per orang dengan dipungut PPh pasal 23.</li>
            <li style="text-align:justify; line-height:1.5;">4. Pemberian permakanan diberikan dalam bentuk makanan dan sesuai dengan standar yang ditetapkan.</li>
            <li style="text-align:justify; line-height:1.5;">Pemberian permakanan dilakukan setiap hari tanpa pernah diliburkan.</li>
            <li style="text-align:justify; line-height:1.5;">Membuat Berita Acara perubahan/pergantian penyandang disabilitas dan penyakit tertentu apabila terjadi perubahan atau pergantian penerima manfaat dalam bulan berkenaan </li>
            </ol>
            <table width="100%" >
            <tr>
            <td style="text-align:justify; width="85%""><p style="text-indent: 10em; ">Demikian surat pernyataan ini dibuat dengan sebenar-benarnya dan penuh tanggung jawab.</p></td>
            </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td ></td>
                      <td >Ketua IPSM</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td> Kelurahan  Kedung Baruk</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><u>Nur Fauzah</u></td>
                    </tr>
            </table>
        ';

        $html2 ='
            <table width="100%">
                <tr>
                    <td align="center" width="10%"><img SRC="/images/ipsm.jpg" height="60" width="60"></td>
                    <td align="center" width="90%"><h1>IKATAN PEKERJA SOSIAL MASYARAKAT</h1><br>
                    <b>Kelurahan Kedung Baruk Kecamatan Rungkut</b><br>
                    </td>
                </tr>
            </table>
            <hr color="black">
            </br>
            <br>
            <table width="100%">
            <tr>
                <td width="10%">Nomor</td>
                <td width="3%">:</td>
                <td width="47%"></td>
                <td width="40%">Kepada Yth.</td>
            </tr>
            <tr>
                <td width="10%">Sifat</td>
                <td width="3%">:</td>
                <td width="47%">-</td>
                <td width="40%">Pejabat Pembuat Komitmen</td>
            </tr>
            <tr>
                <td width="10%">Perihal</td>
                <td width="3%">:</td>
                <td width="47%">Permohonan Pencairan Dana Permakanan</td>
                <td width="40%">Penyelenggaraan Kegiatan Pemenuhan</td>
            </tr>
            <tr>
                <td width="10%"></td>
                <td width="3%"></td>
                <td width="47%"></td>
                <td width="40%">Kebutuhan Dasar Permakanan Bagi<br>Penyandang Disabilitas dan Penyakit<br>Tertentu<br>Di-<br><u>SURABAYA</u></td>
            </tr>
            </table>
            <br>
            <br>
            <br>
            <table width="100%">
            <tr>
                <td style="text-align:justify;"><p style="text-indent: 10em; line-height:2;">Berkenaan dengan penyelenggaraan kegiatan pemenuhan kebutuhan dasar permakanan bagi penyandang disabilitas dan penyakit tertentu di Kota Surabaya, bersama ini kami mengajukan Permohonan pencairan dana untuk tanggal 1 sampai dengan tanggal 28  Bulan Februari Tahun 2019 sebesar Rp 12.535.600,- Dengan rincian sebagai berikut :</p></td>
            </tr>
            </table>
            <br>
            <br>
            <br>
            <table width="100%" border="1" style="text-align: center;">
                <tr>
                    <th width="5%">No.</th>
                    <th width="40%">Jumlah Penyandang Disabilitas  dan Penyakit Tertentu</th>
                    <th width="15%">Jumlah Hari</th>
                    <th width="40%">Total Harga x Rp11.000 ,-</th>
                </tr>
                <tr>
                    <td><b>1</b></td>
                    <td><b>37 Orang</b></td>
                    <td><b>28 Hari <br> SSPD 10%</b></td>
                    <td><b>Rp 11.396.000 <br> Rp 1.139.600</b></td>
                </tr>
            </table>
            <br><br><br>
            <table>
                <tr>
                <td width="500"><p style="text-indent: 10em; line-height:2;">Demikian atas perhatiannya kami ucapkan terima kasih.</p></td>
                </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                    <tr>
                      <td ></td>
                      <td >Surabaya, ...................................</td>
                    </tr>
                    <tr>
                      <td >Mengetahui</td>
                      <td >Ketua IPSM</td>
                    </tr>
                    <tr>
                      <td>Lurah Kedung Baruk</td>
                      <td>Kel Kedung Baruk</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td>( …………………………….. )</td>
                      <td><b><u> Nur Fauzah</u></b></td>
                    </tr>
            </table>
            <br><br>
            <table>
                <tr>
                <td width="500"><p><u>Tembusan:</u></p></td>
                </tr>
                <tr>
                <td width="500"><p>Bpk. Kepala Dinas Sosial Kota Surabaya</p></td>
                </tr>
            </table>
        ';

        $html3 = '
            <table width="100%">
                <tr>
                    <td align="center" width="10%"><img SRC="/images/ipsm.jpg" height="60" width="60"></td>
                    <td align="center" width="90%"><h1>IKATAN PEKERJA SOSIAL MASYARAKAT</h1><br>
                    <b>Kelurahan Kedung Baruk Kecamatan Rungkut</b><br>
                    </td>
                </tr>
            </table>
            <hr color="black">
            </br>
            <br>
            <table width="100%" style="text-align:center;">
                <tr>
                    <td><b><u>SURAT PERNYATAAN TANGGUNG JAWAB BELANJA MUTLAK</u></b></td>
                </tr>
                <tr>
                    <td>Nomor : ......................................................................</td>
                </tr>
            </table>
            <br><br><br>
            <span>Yang bertanda tangan dibawah ini :</span><br>
            <table width="100%" >
            <tr>
                <td width="10%">Nama</td>
                <td width="2%">:</td>
                <td width="88%">Nur Fauzah</td>
            </tr>
            <tr>
                <td width="10%">Jabatan</td>
                <td width="2%">:</td>
                <td width="88%">Ketua IPSM Kelurahan Kedung Baruk dalam hal ini bertindak dan atas nama</td>
            </tr>
            <tr>
                <td width="10%"></td>
                <td width="2%"></td>
                <td width="88%">IPSM Kelurahan Kedung Baruk Kecamatan Rungkut Kota Surabaya</td>
            </tr>
            </table>
            <br><br><br>
            <span>Menyatakan dengan sesungguhnya bahwa  :</span>
            <ol>
            <li style="text-align:justify; line-height:1.5;">Bersedia untuk bertanggung jawab mutlak penyelenggaraan kegiatan pemenuhan kebutuhan dasar Permakanan bagi Penyandang Disabilitas dan Penyakit Tertentu pada bulan Februari 2019 di wilayah Kelurahan Kedung Baruk sesuai dengan surat Perjanjian Kerjasama/Kontrak  antara Dinas Sosial Kota Surabaya (Pejabat Pembuat Komitmen) dengan Ketua IPSM Kelurahan Kedung Baruk  Kecamatan Rungkut Kota Surabaya Nomor : 460/0015.109 /436.7.7/2019 Tanggal : 2 Januari 2019</li>
            <li style="text-align:justify; line-height:1.5;">Apabila dikemudian hari terdapat kelebihan anggaran atas kegiatan penyelenggaraan program tersebut di atas, kami bersedia untuk menanggung secara pribadi dan menyetorkannya ke Kas Daerah melalui Dinas Sosial.</li>
            </ol>
            <table width="100%" >
            <tr>
            <td style="text-align:justify; width="85%""><p style="text-indent: 10em; ">Demikian surat pernyataan ini dibuat dengan sebenar-benarnya dan tanpa ada unsur paksaan dari pihak manapun</p></td>
            </tr>
            </table>
            <br><br><br>
            <table style="text-align:center">
                    <tr>
                      <td></td>
                      <td >Surabaya, ................................................</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td ></td>
                      <td >Ketua</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>IPSM Kelurahan Kedung Baruk</td>
                    </tr>
                    <tr>
                      <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                       <td colspan="2"></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><b><u>Nur Fauzah</u></b></td>
                    </tr>
            </table>

        ';

        $html4 = '
            <h5 style="text-align: center;">DAFTAR NAMA PENERIMA MANFAAT PENYELENGGARAAN KEGIATAN PEMENUHAN DASAR <br>PERMAKANAN BAGI PENYANDANG DISABILITAS DAN PENYAKIT TERTENTU <br>KELURAHAN KEJAWAN PUTIH TAMBAK KECAMATAN MULYOREJO<br>BULAN NOVEMBER 2016</h5>
            <br><br>
            <table width="100%" border="1">
                <tr>
                    <th width="5%">No.</th>
                    <th width="10%">NIPM</th>
                    <th width="20%">NIK</th>
                    <th width="20%">NAMA</th>
                    <th width="20%">ALAMAT</th>
                    <th width="10%">TGL <br>LAHIR</th>
                    <th width="15%">JENIS<br>KECACATAN</th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <br><br>
            <table width="100%" style="text-align: center;">
                <tr>
                    <td></td>
                    <td>Surabaya, ……………………. 2018</td>
                </tr>
                <tr>
                    <td>Mengetahui,</td>
                    <td>Ketua IPSM</td>
                </tr>
                <tr>
                    <td>Lurah Kejawan Putih Tambak</td>
                    <td>Kel Kejawan Putih Tambak</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>( ………………………..)</td>
                    <td>( MOCH. IRFAN)</td>
                </tr>
            </table>
        ';

        PDF::SetTitle('SPJ IPSM');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html2, true, false, true, false, '');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html3, true, false, true, false, '');
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html4, true, false, true, false, '');
        PDF::Output('spjIPSM.pdf', 'I');
    }

    public function download_pdf()
    {
        $user = Auth::user();

        PDF::SetMargins(5, 10, 5);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(25);
        PDF::setStartingPageNumber(1);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::SetFont('times', '', '12');

        $html = '

        ';
        PDF::SetTitle('Rekapitulasi');
        PDF::AddPage('L', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('Rekapitulasi.pdf', 'I');
    }
}
