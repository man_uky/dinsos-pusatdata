<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\ClientIntervensi;
use App\Intervensi;
use App\Bidang;
use App\Pmks;
use App\PmksForm;
use App\FormKhusus;
use App\Kelurahan;
use App\Kecamatan;
use App\File;
use Auth;
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clients = new Client;
        $user = Auth::user();
        $intervensi = new Intervensi;
        $pmks = new Pmks;
        if($user->role_id==ROLE_BIDANG)
        {
            $clients = $clients->whereHas('client_intervensi',function($query) use($user){
                $query->where('bidang_id',$user->bidang_id);
            });
            $clients = $clients->with(['client_intervensi'=>function($query) use($user) {
                $query->where('bidang_id',$user->bidang_id);
            }]);
            $intervensi = $intervensi->where('bidang_id',$user->bidang_id);
            //$pmks = $pmks->where('bidang_id',$user->bidang_id);
        }
        if(in_array($request->status_penanganan,array(1,2,3,4)))
        {
            $clients = $clients->where('status_penanganan_id',$request->status_penanganan);
        }
        else {
            return redirect('/admin/clients?status_penanganan='.STATUS_PENANGANAN_BELUM_DITANGANI);
        }
        $intervensi_id = $request->input('intervensi_id');
        $data['intervensi_id'] = $intervensi_id;
        if($intervensi_id && $intervensi_id!= ''){
            $clients = $clients->whereHas('client_intervensi',function($query) use($intervensi_id){
                $query->where('intervensi_id',$intervensi_id);
            });
        }
        $searchField = ['nik','nama','kelurahan_id','kecamatan_id','pmks_id'];
        foreach ($searchField as $field) {
            $val = $request->input($field);
            $data[$field] = $val;
            if ($val && $val != '') {
                if(strpos($field,'_id'))$clients = $clients->where($field,$val);
                else $clients =$clients->where($field, 'like', '%'.$val.'%');
            }
        }
        $clients =$clients->orderBy('created_at', 'asc')->paginate(10)->onEachSide(1);
        $data['user'] = $user;
        $data['clients'] = $clients;
        $data['kelurahans'] = Kelurahan::all();
        $data['kecamatans'] = Kecamatan::all();
        $data['intervensis'] = $intervensi->get();
        $data['pmks'] = $pmks->get();
        return view('admin.data.client.index',$data);
    }

    public function activate($id)
    {
        $client = Client::find($id);
        $client->status_penanganan_id = STATUS_PENANGANAN_BELUM_DITANGANI;
        $client->save();
        return redirect('/admin/clients?status_penanganan='.STATUS_PENANGANAN_BELUM_DITANGANI)->with('activate_success',true);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pmks_all'] = Pmks::with('pmks_form')->get();
        $data['bidangs'] = Bidang::get();
        $data['jenis_pmks'] = Pmks::get();
        $data['kelurahans'] = Kelurahan::get();
        $data['kecamatans'] = Kecamatan::get();
        return view('admin.data.client.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $requestData = $request->all();
        $requestData['petugas_id'] = $user->id;
        $requestData['tanggal_usulan'] = date_database($request->tanggal_usulan);
        if($user->role_id==ROLE_ADMIN)$requestData['status_penanganan_id'] = 2;
        else if($user->role_id==ROLE_BIDANG)$requestData['status_penanganan_id'] = 1;
        $photo = $request->file('photo');
        if($photo)
        {
            $file = File::upload($photo);
            $requestData['photo_id'] = $file->id;
        }
        $client = Client::create($requestData);
        $client_intervensi = new ClientIntervensi;
        $client_intervensi->client_id = $client->id;
        $client_intervensi->bidang_id = $requestData['bidang_id'];
        $client_intervensi->save();
        foreach ($requestData as $key => $value) {
            if(strstr($key,"tambahan_"))
            {
                $str = explode("tambahan_", $key);
                $form_khusus = new FormKhusus;
                $form_khusus->nilai = $value;
                $form_khusus->pmks_form_id = $str[1];
                $form_khusus->pmks_id = $requestData['pmks_id'];
                $form_khusus->client_id = $client->id;
                $form_khusus->save();
            }
        }
        if(in_array($request->status_penanganan,array(1,2,3,4)))
        {
            return redirect('/admin/clients?status_penanganan='.$request->status_penanganan)->with('create_success',true);
        }
        else {
            return redirect('/admin/clients?status_penanganan='.STATUS_PENANGANAN_BELUM_DITANGANI)->with('create_success',true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['client'] = Client::find($id);
        return view('admin.data.client.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
