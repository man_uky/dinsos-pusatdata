<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Intervensi;
use App\Kelurahan;
use App\Kecamatan;
use App\Client;
use App\ClientIntervensi;
use App\PergantianPmks;

class PergantianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user =Auth::user();
        $pergantian_pmks = new PergantianPmks;
        $intervensi = new Intervensi;
        $searchField = ['nik1','nama1','nipm1','nik2','nama2','nipm2','kelurahan_id','kecamatan_id','intervensi_id'];
        foreach ($searchField as $field) {
            $val = $request->input($field);
            $data[$field] = $val;
            if ($val && $val != '') {
                if(strpos($field,'_id'))$pergantian_pmks = $pergantian_pmks->where($field,$val);
                else $pergantian_pmks = $pergantian_pmks->where($field, 'like', '%'.$val.'%');
            }
        }
        if($user->role_id==ROLE_BIDANG)
        {
            $intervensi = Intervensi::where('bidang_id',$user->bidang_id);
        }
        $data['intervensis'] = $intervensi->get();
        $data['kelurahans'] = Kelurahan::all();
        $data['kecamatans'] = Kecamatan::all();
        $data['pergantians'] = $pergantian_pmks->orderBy('updated_at', 'desc')->paginate(10)->onEachSide(1);
        return view('admin.data.pergantian.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $intervensi = new Intervensi;
        $clients_1 = new Client;
        $clients_2 = new Client;
        $user =Auth::user();
        $area_type = $request->area_type;
        $intervensi_id = $request->input('intervensi_id');
        $searchField = ['kelurahan_id','kecamatan_id'];
        foreach ($searchField as $field) {
            $val = $request->input($field);
            $data[$field] = $val;
            if ($val && $val != '') {
                $clients_1 = $clients_1->where($field,$val);
                $clients_2 = $clients_2->where($field,$val);
            }
        }
        if($intervensi_id && $intervensi_id!= ''){
            $clients_1 = $clients_1->whereHas('client_intervensi',function($query) use($intervensi_id){
                $query->where('intervensi_id',$intervensi_id);
                $query->where('is_active',1);
            });
            $clients_2 = $clients_2->whereHas('client_intervensi',function($query) use($user){
                $query->where('bidang_id',$user->bidang_id);
            });
            $clients_2 = $clients_2->where('status_penanganan_id',STATUS_PENANGANAN_BELUM_DITANGANI);
            if($request->kelurahan_id && $request->kelurahan_id!='' || $request->kecamatan_id && $request->kecamatan_id!='')
            {
                $data['clients_1'] = $clients_1->get();
                $data['clients_2'] = $clients_2->get();
            }
        }
        if($user->role_id==ROLE_BIDANG)
        {
            $intervensi = Intervensi::where('bidang_id',$user->bidang_id);
        }

        $data['intervensis'] = $intervensi->get();
        $data['intervensi_id'] = $intervensi_id;
        $data['kelurahans'] = Kelurahan::all();
        $data['kecamatans'] = Kecamatan::all();
        return view('admin.data.pergantian.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user =Auth::user();
        $requestData = $request->all();
        $requestData['petugas_id'] = $user->id;
        $requestData['bidang_id'] = $user->bidang_id;
        $requestData['tanggal_ba'] = date_database($request->tanggal_ba);
        $requestData['tanggal_berhenti'] = date_database($request->tanggal_berhenti);
        if($requestData['tanggal_menerima'])$requestData['tanggal_menerima'] = date_database($request->tanggal_menerima);

        if($requestData['pmks_1']){
            $client_intervensi = ClientIntervensi::where('client_id',$requestData['pmks_1'])
            ->where('intervensi_id',$requestData['intervensi_id'])->first();
            $client_intervensi->tanggal_selesai = $requestData['tanggal_berhenti'];
            $client_intervensi->keterangan = $requestData['keterangan'];
            $client_intervensi->is_active = 0;
            $client_intervensi->status_penanganan = "TIDAK AKTIF";
            $client_intervensi->save();
            $client = Client::where('nik',$requestData['nik1'])->first();
            $client->status_penanganan_id=STATUS_PENANGANAN_TIDAK_AKTIF;
            $client->save();
            $requestData['kelurahan_id'] = $client_intervensi->client->kelurahan_id;
            $requestData['kecamatan_id'] = $client_intervensi->client->kecamatan_id;
        }
        if($requestData['pmks_2']){
            $client_intervensi = ClientIntervensi::where('client_id',$requestData['pmks_2'])
            ->where('bidang_id',$requestData['bidang_id'])->first();
            $client_intervensi->tanggal_mulai = $requestData['tanggal_menerima'];
            $client_intervensi->is_active = 1;
            $client_intervensi->intervensi_id = $requestData['intervensi_id'];
            $client_intervensi->nipm = $requestData['nipm2'];
            $client_intervensi->status_penanganan = "DITANGANI";
            $client_intervensi->save();
            $client = Client::where('nik',$requestData['nik2'])->first();
            $client->status_penanganan_id=STATUS_PENANGANAN_AKTIF;
            $client->save();
            $requestData['kelurahan_id'] = $client_intervensi->client->kelurahan_id;
            $requestData['kecamatan_id'] = $client_intervensi->client->kecamatan_id;
        }
        $pergantian_pmks = PergantianPmks::create($requestData);
        return back()->with('create_success',true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
