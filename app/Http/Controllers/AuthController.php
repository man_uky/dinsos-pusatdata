<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class AuthController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $email = $email."@dinsos.com";
        if (Auth::attempt(['email'=>$email,'password'=>$password])) {
            if(in_array(Auth::user()->role_id,[ROLE_ADMIN,ROLE_BIDANG,ROLE_PENYEDIA]))return redirect('admin');
            else return redirect('/home');
        } else {
            return redirect()->back()
                ->with('login', 'Email atau password salah.')
                ->withInput();
        }
    }

    /**
     * Handle a login request to the application.
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }
}
