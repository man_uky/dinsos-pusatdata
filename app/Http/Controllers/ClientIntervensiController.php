<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Bidang;
use App\Intervensi;
use App\Penyedia;
use App\ClientIntervensi;
use Auth;
use Illuminate\Support\Facades\Artisan;

class ClientIntervensiController extends Controller
{
    public function index($id)
    {
    	$user = Auth::user();
    	$client = Client::find($id);
    	$data['client'] = $client;
        $data['bidangs'] = Bidang::all();
    	$client_intervensis = new ClientIntervensi;
    	$intervensis = new Intervensi;
    	if($user->role_id==ROLE_BIDANG){
    		$client_intervensis = $client_intervensis->where('bidang_id',$user->bidang_id);
            $intervensis = $intervensis->where('bidang_id',$user->bidang_id);
    	}
    	$client_intervensis = $client_intervensis->where('client_id',$id);
    	$client_intervensis = $client_intervensis->orderBy('updated_at', 'desc')->paginate(10)->onEachSide(1);
    	$data['client_intervensis'] = $client_intervensis;
        $data['intervensis'] = $intervensis->get();
        $data['penyedias'] = Penyedia::with('user')->get();
        return view('admin.data.intervensi.index',$data);
    }
    public function store(Request $request,$id)
    {
        $client_intervensi = new ClientIntervensi;
        $client_intervensi->bidang_id = $request->bidang_id;
        $client_intervensi->client_id = $id;
        $client_intervensi->save();
        $client = Client::find($id);
        if(Auth::user()->role_id==ROLE_ADMIN)$client->status_penanganan_id = STATUS_PENANGANAN_BELUM_DITANGANI;
        else $client->status_penanganan_id = STATUS_PENANGANAN_AKTIF;
        $client->save();
        return redirect('/admin/clients/'.$id.'/intervensi?status_penanganan='.$request->status_penanganan)->with('create_success',true);
    }
    public function update(Request $request,$id)
    {
    	$requestData = $request->all();
        $client_intervensi = ClientIntervensi::find($requestData['id']);
        $requestData['tanggal_mulai'] = date_database($request->tanggal_mulai);
        $requestData['is_active'] = 1;
        $client_intervensi->update($requestData);
        if($client_intervensi){
            $client = Client::find($client_intervensi->client_id);
            $client->status_penanganan_id = STATUS_PENANGANAN_AKTIF;
            $client->save();
            $exitCode = Artisan::call('update:kuota', [
                'penyedia_id' => $requestData['penyedia_id']
            ]);
        }
        return redirect('/admin/clients/'.$id.'/intervensi?status_penanganan='.$request->status_penanganan)->with('create_success',true);
    }
    public function delete(Request $request,$id)
    {
    	if(Auth::user()->role_id==ROLE_ADMIN || Auth::user()->role_id==ROLE_BIDANG)
    	{
            $client_intervensi = ClientIntervensi::find($id);
            $client_intervensi->status_penanganan = "Tidak Aktif";
            $client_intervensi->is_active = 0;
            $client_intervensi->tanggal_berhenti = date_database($request->tanggal_berhenti);
            $client_intervensi->save();
            if($client_intervensi){
                $client = Client::find($client_intervensi->client_id);
                $client->status_penanganan_id = STATUS_PENANGANAN_TIDAK_AKTIF;
                $client->save();
                $exitCode = Artisan::call('update:kuota', [
                    'penyedia_id' => $client_intervensi->penyedia_id
                ]);
            }
    	}
    	return redirect('/admin/clients/'.$id.'/intervensi?status_penanganan='.$request->status_penanganan)->with('delete_success',true);
    }
}
