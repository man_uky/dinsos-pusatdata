<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Intervensi;
use App\Penyedia;
use App\KuotaKelurahan;
use App\KuotaKecamatan;
use DB;
class KuotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $intervensi_id = $request->intervensi_id;
        $kuota_type = $request->kuota_type;
        $user =Auth::user();
        $data['kuotas'] = array();
        if($kuota_type)$data['kuotas'] = Penyedia::where('tipe',$kuota_type)->get();
        $data['kuota_type'] = $kuota_type;
        return view('admin.data.kuota.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kuota_type = $request->kuota_type;
        $kuota_baru = $request->kuota_baru;
        if($kuota_baru)
        {
            $penyedia = Penyedia::find($id);
            $penyedia->kuota = $kuota_baru;
            $penyedia->save();

        }
        return back()->with('create_success',true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
