<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
class IndexController extends Controller
{
    public function admin(Request $request){
    	return view('admin.index');
    }
    public function photo(Request $request,$id){
        $file = File::find($id);
        return response()->file('../storage/app/' . $file->path);
    }
}
