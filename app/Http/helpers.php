<?php

use Carbon\Carbon;

/**
 * Ngubah date dari format database ke yang mudah dibaca
 * 2017-1-31 jadi 31-1-2017
 */

function date_view($date, $format = 'd-m-Y')
{
    $time = strtotime($date);
    if (!$time) {
        return '';
    }
    return date($format, $time);
}
function date_database($date, $format = 'd-m-Y')
{
    try {
        return Carbon::createFromFormat($format, $date)->toDateTimeString();
    } catch (Exception $e) {
        return null;
    }
}

function url_qrcode($size="300x300",$url="muezzadev.com")
{
    // CHart Type
    $cht = "qr";
    // CHart Size
    $chs = $size;
    // CHart Link
    // the url-encoded string you want to change into a QR code
    $chl = urlencode($url);
    // CHart Output Encoding (optional)
    // default: UTF-8
    $choe = "UTF-8";
    $qrcode = 'https://chart.googleapis.com/chart?cht=' . $cht . '&chs=' . $chs . '&chl=' . $chl . '&choe=' . $choe;
    return $qrcode;
}
function cetak_css()
{
    return '<style>
    .font-kecil td{
        font-size: 9px;
    }
    .font-kecil th{
        font-size: 9px;
    }
    </style>';
}
function terbilang($x)
{
    $abil = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
    if ($x < 12)
        return " " . $abil[$x];
    elseif ($x < 20)
        return terbilang($x - 10) . " belas";
    elseif ($x < 100)
        return terbilang($x / 10) . " puluh" . terbilang($x % 10);
    elseif ($x < 200)
        return " seratus" . terbilang($x - 100);
    elseif ($x < 1000)
        return terbilang($x / 100) . " ratus" . terbilang($x % 100);
    elseif ($x < 2000)
        return " seribu" . terbilang($x - 1000);
    elseif ($x < 1000000)
        return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    elseif ($x < 1000000000)
        return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
}
