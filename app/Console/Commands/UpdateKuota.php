<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ClientIntervensi;
use App\KuotaKelurahan;
use App\KuotaKecamatan;
use App\Penyedia;
use DB;
class UpdateKuota extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:kuota {penyedia_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $penyedia_id = $this->argument('penyedia_id');
        $count= ClientIntervensi::where('penyedia_id',$penyedia_id)
        ->count();
        $kuota = Penyedia::find($penyedia_id);
        $kuota->terisi=$count;
        $kuota->save();
    }
}
