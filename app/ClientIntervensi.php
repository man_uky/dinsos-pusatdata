<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientIntervensi extends Model
{
	protected $fillable =['jenis_penanganan','jenis_rehabilitasi','status_rehabilitasi','deskripsi_rehabilitasi','status_pengentasan','psks_pembantu','perkembangan_terbaru','intervensi_id','status_penanganan','nipm','tanggal_mulai','tanggal_selesai','is_active','no_berita_acara','penyedia_id'];
    public function bidang()
    {
    	return $this->belongsTo('App\Bidang');
    }
    public function intervensi()
    {
    	return $this->belongsTo('App\Intervensi');
    }
    public function client()
    {
    	return $this->belongsTo('App\Client');
    }
}
