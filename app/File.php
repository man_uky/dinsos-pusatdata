<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $guarded = [];

    public static function upload($uploaded_file, $path = 'uploaded')
    {
        $file = new File;
        $file->ext = $uploaded_file->getClientOriginalExtension();
        $file->name = $uploaded_file->getClientOriginalName();
        $file->mime = $uploaded_file->getClientMimeType();
        $file->random_name = str_random(23) . '.' . $file->ext;

        $file->path = $uploaded_file->storeAs($path, $file->random_name);
        $file->save();
        return $file;
    }

}
