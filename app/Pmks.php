<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pmks extends Model
{
    public function pmks_form()
    {
    	return $this->hasMany('App\PmksForm');
    }
}
