<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PergantianPmks extends Model
{
    protected $fillable =['nik1','nama1','alamat1','nipm1','nik2','nama2','alamat2','nipm2','tanggal_ba','tanggal_berhenti','tanggal_menerima','keterangan','kelurahan_id','kecamatan_id','intervensi_id','bidang_id','petugas_id'];
    public function intervensi()
    {
    	return $this->belongsTo('App\Intervensi');
    }
    public function kelurahan()
    {
    	return $this->belongsTo('App\Kelurahan');
    }
    public function kecamatan()
    {
    	return $this->belongsTo('App\Kecamatan');
    }

}
