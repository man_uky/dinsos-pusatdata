<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['nik','nama','no_kk','alamat_kk','asal','jenis_kelamin','status_perkawinan','pendidikan_tertinggi','kecamatan_id','kelurahan_id','pekerjaan','pendapatan','status_kepemilikan','luas_lantai','jenis_lantai','jenis_dinding','jenis_atap','jumlah_kamar_tidur','sumber_air_minum','memperoleh_air_minum','sumber_penerangan','daya_terpasang','bahan_bakar_memasak','tempat_bab','pembuangan_bab','biaya_sampah','biaya_sewa','biaya_listrik','biaya_air','biaya_gas','telp_rumah','tabung_gas','lemari_es','ac','pemanas_air','telepon','tv','emas','komputer','sepeda','motor','mobil','perahu','motor_tempel','perahu_motor','kapal','kks','kip','bpjs_kesehatan_mandiri','bpjs_ketenagakerjaan_mandiri','bpjs_kesehatan_pbi','asuransi_kesehatan_lain','pkh','raskin','kur','tanggal_usulan','permasalahan','harapan','tahun','pmks_id','bidang_id','status_penanganan_id','photo_id'];


    public function scopeExclude($query,$value = array()) 
    {
        return $query->select( array_diff( $this->fillable,(array) $value) );
    }
    public function pmks()
    {
    	return $this->belongsTo('App\Pmks');
    }
    public function petugas()
    {
        return $this->belongsTo('App\User','petugas_id','id');
    }
    public function bidang()
    {
    	return $this->belongsTo('App\Bidang');
    }
    public function client_intervensi()
    {
    	return $this->hasMany('App\ClientIntervensi');
    }
    public function kecamatan()
    {
    	return $this->belongsTo('App\Kecamatan');
    }
    public function kelurahan()
    {
    	return $this->belongsTo('App\Kelurahan');
    }
    public function status_penanganan()
    {
    	return $this->belongsTo('App\StatusPenanganan');
    }
}
