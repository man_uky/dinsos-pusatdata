INSERT INTO `kecamatans` (`id`, `nama`, `created_at`, `updated_at`) VALUES
	(1, 'SAWAHAN', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(2, 'WIYUNG', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(3, 'WONOKROMO', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(4, 'PABEAN CANTIAN', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(5, 'TAMBAKSARI', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(6, 'GENTENG', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(7, 'BUBUTAN', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(8, 'DUKUH PAKIS', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(9, 'BULAK', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(10, 'BENOWO', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(11, 'GUBENG', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(12, 'GUNUNG ANYAR', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(13, 'KENJERAN', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(14, 'KREMBANGAN', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(15, 'LAKARSANTRI', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(16, 'RUNGKUT', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(17, 'SAMBI KEREP', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(18, 'TANDES', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(19, 'TEGALSARI', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(20, 'TENGGILIS MEJOYO', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(21, 'ASEMROWO', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(22, 'GAYUNGAN', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(23, 'JAMBANGAN', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(24, 'KARANG PILANG', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(25, 'MULYOREJO', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(26, 'PAKAL', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(27, 'SEMAMPIR', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(28, 'SIMOKERTO', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(29, 'SUKOLILO', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(30, 'SUKOMANUNGGAL', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(31, 'WONOCOLO', '2019-03-16 04:02:15', '2019-03-16 04:02:15'),
	(32, 'GUNDIH', '2019-03-16 04:02:15', '2019-03-16 04:02:15');
