<?php

use Illuminate\Database\Seeder;
use App\Penyedia;
class PenyediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($j=1;$j<=154;$j++){
            Penyedia::create([
                'intervensi_id'=>6,
                'bidang_id'=>6,
                'tipe'=>1
            ]);
        }
        for($j=1+154;$j<=1+154+152;$j++){
            Penyedia::create([
                'intervensi_id'=>7,
                'bidang_id'=>6,
                'tipe'=>2
            ]);
        }
        for($j=1+1+154+152;$j<=1+1+154+152+30;$j++){
            Penyedia::create([
                'intervensi_id'=>8,
                'bidang_id'=>6,
                'tipe'=>3
            ]);
        }
    }
}
