<?php

use Illuminate\Database\Seeder;
use App\Bidang;
class BidangsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Bidang::create([
			'nama'=>'UPTD GRIYA WERDA'
		]);
		Bidang::create([
			'nama'=>'UPTD BABAT JERAWAT'
		]);
		Bidang::create([
			'nama'=>'UPTD KALIJUDAN'
		]);
		Bidang::create([
			'nama'=>'UPTD KEPUTIH'
		]);
		Bidang::create([
			'nama'=>'UPTD KAMPUNG ANAK NEGERI'
		]);
		Bidang::create([
			'nama'=>'RESOS'
		]);
		Bidang::create([
			'nama'=>'KESOS'
		]);
		Bidang::create([
			'nama'=>'PUSDASOS'
		]);

    }
}
