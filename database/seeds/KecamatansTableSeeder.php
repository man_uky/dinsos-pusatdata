<?php

use Illuminate\Database\Seeder;
use App\Kecamatan;
class KecamatansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kecamatan::create([
            'nama' => 'ASEMROWO'
        ]);
        Kecamatan::create([
            'nama' => 'BENOWO'
        ]);
        Kecamatan::create([
            'nama' => 'BUBUTAN'
        ]);
        Kecamatan::create([
            'nama' => 'BULAK'
        ]);
        Kecamatan::create([
            'nama' => 'DUKUH PAKIS'
        ]);
        Kecamatan::create([
            'nama' => 'GAYUNGAN'
        ]);
        Kecamatan::create([
            'nama' => 'GENTENG'
        ]);
        Kecamatan::create([
            'nama' => 'GUBENG'
        ]);
        Kecamatan::create([
            'nama' => 'GUNUNGANYAR'
        ]);
        Kecamatan::create([
            'nama' => 'JAMBANGAN'
        ]);
        Kecamatan::create([
            'nama' => 'KARANG PILANG'
        ]);
        Kecamatan::create([
            'nama' => 'KENJERAN'
        ]);
        Kecamatan::create([
            'nama' => 'KREMBANGAN'
        ]);
        Kecamatan::create([
            'nama' => 'LAKARSANTRI'
        ]);
        Kecamatan::create([
            'nama' => 'MULYOREJO'
        ]);
        Kecamatan::create([
            'nama' => 'PABEAN CANTIAN'
        ]);
        Kecamatan::create([
            'nama' => 'PAKAL'
        ]);
        Kecamatan::create([
            'nama' => 'RUNGKUT'
        ]);
        Kecamatan::create([
            'nama' => 'SAMBIKEREP'
        ]);
        Kecamatan::create([
            'nama' => 'SAWAHAN'
        ]);
        Kecamatan::create([
            'nama' => 'SEMAMPIR'
        ]);
        Kecamatan::create([
            'nama' => 'SIMOKERTO'
        ]);
        Kecamatan::create([
            'nama' => 'SUKOLILO'
        ]);
        Kecamatan::create([
            'nama' => 'SUKOMANUNGGAL'
        ]);
        Kecamatan::create([
            'nama' => 'TAMBAKSARI'
        ]);
        Kecamatan::create([
            'nama' => 'TANDES'
        ]);
        Kecamatan::create([
            'nama' => 'TEGALSARI'
        ]);
        Kecamatan::create([
            'nama' => 'TENGGILISMEJOYO'
        ]);
        Kecamatan::create([
            'nama' => 'WIYUNG'
        ]);
        Kecamatan::create([
            'nama' => 'WONOCOLO'
        ]);
        Kecamatan::create([
            'nama' => 'WONOKROMO'
        ]);


    }
}
