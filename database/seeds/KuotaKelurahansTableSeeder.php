<?php

use Illuminate\Database\Seeder;
use App\KuotaKelurahan;
class KuotaKelurahansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=18;$i++)
        {
            for($j=1;$j<=157;$j++){
                KuotaKelurahan::create([
                    'intervensi_id'=>$i,
                    'kelurahan_id'=>$j
                ]);
            }
        }
    }
}
