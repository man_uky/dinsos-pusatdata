<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class ClientsTableSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
	{
		$this->table = 'clients';
		$this->filename = base_path().'/database/seeds/csv/dbdinsos.csv';
	}
    public function run()
    {
        // Recommended when importing larger CSVs
		DB::disableQueryLog();
		// Uncomment the below to wipe the table clean before populating
		DB::table($this->table)->truncate();
		parent::run();
		/*DB::table($this->table)
		->update([
			'asal' => DB::raw("REPLACE(asal,'\"','')"),
			'no_kk' => DB::raw("REPLACE(no_kk,'\"','')"),
			'alamat_kk' => DB::raw("REPLACE(alamat_kk,'\"','')"),
			'nama' => DB::raw("REPLACE(nama,'\"','')"),
			'nik' => DB::raw("REPLACE(nik,'\"','')"),
			'status_penanganan' => DB::raw("REPLACE(status_penanganan,'\"','')")
		]);
		*/
    }
}
