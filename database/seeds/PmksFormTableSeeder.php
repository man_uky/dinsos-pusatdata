<?php

use Illuminate\Database\Seeder;
use App\PmksForm;
class PmksFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PmksForm::create([
			"title" => "Nama Orang Tua",
			"nama" => "ortu",
			"type" => "text",
			"pmks_id" => 1
		]);
		PmksForm::create([
			"title" => "Penyebab Terlantar",
			"nama" => "penyebab_terlantar",
			"type" => "text",
			"pmks_id" => 1
		]);
		PmksForm::create([
			"title" => "Jika berumur 0-2 th, mendapat ASI/ Susu Pengganti 24 jam terakhir",
			"nama" => "asi_pengganti",
			"type" => "select",
			"pmks_id" => 1
		]);
		PmksForm::create([
			"title" => "Mendapat Imunisasi sesuai usianya",
			"nama" => "imunisasi",
			"type" => "select",
			"pmks_id" => 1
		]);

		PmksForm::create([
			"title" => "Memperoleh makanan 4 sehat seminggu yang lalu",
			"nama" => "makanan_sehat",
			"type" => "select",
			"pmks_id" => 1
		]);

		PmksForm::create([
			"title" => "Jika sakit, dirawat/diobati secara medis",
			"nama" => "rawat_medis",
			"type" => "select",
			"pmks_id" => 1
		]);
		PmksForm::create([
			"title" => "Mengalami Eksploitasi ",
			"nama" => "mengalami_exploitasi",
			"type" => "select",
			"pmks_id" => 1
		]);

		PmksForm::create([
			"title" => "Nama Orang Tua",
			"nama" => "ortu",
			"type" => "text",
			"pmks_id" => 2
		]);

		PmksForm::create([
			"title" => "Penyebab Terlantar",
			"nama" => "penyebab_terlantar",
			"type" => "text",
			"pmks_id" => 2
		]);

		PmksForm::create([
			"title" => "Mendapat Imunisasi sesuai usianya",
			"nama" => "imunisasi",
			"type" => "select",
			"pmks_id" => 2
		]);
		PmksForm::create([
			"title" => "Memperoleh makanan 4 sehat seminggu yang lalu",
			"nama" => "makanan_sehat",
			"type" => "select",
			"pmks_id" => 2
		]);
		PmksForm::create([
			"title" => "Mendapat pendidikan formal dasar 9 tahun sesuai usianya",
			"nama" => "pendidikan",
			"type" => "select",
			"pmks_id" => 2
		]);

		PmksForm::create([
			"title" => "Kalo iya, apa Pendidikan terakhir",
			"nama" => "pendidikan_terakhir",
			"type" => "text",
			"pmks_id" => 2
		]);

		PmksForm::create([
			"title" => "Jika sakit, dirawat/diobati secara medis",
			"nama" => "rawat_medis",
			"type" => "select",
			"pmks_id" => 2
		]);

		PmksForm::create([
			"title" => "Jenis Disabilitas",
			"nama" => "jenis_disabilitas",
			"type" => "text",
			"pmks_id" => 4
		]);

		PmksForm::create([
			"title" => "Penyebab Kecacatan",
			"nama" => "penyebab_kecacatan",
			"type" => "text",
			"pmks_id" => 4
		]);

		PmksForm::create([
			"title" => "Mendapat Imunisasi sesuai usianya",
			"nama" => "imunisasi",
			"type" => "select",
			"pmks_id" => 4
		]);
		PmksForm::create([
			"title" => "Memperoleh makanan 4 sehat seminggu yang lalu",
			"nama" => "makanan_sehat",
			"type" => "select",
			"pmks_id" => 4
		]);
		PmksForm::create([
			"title" => "Mendapat pendidikan formal dasar 9 tahun sesuai usianya",
			"nama" => "pendidikan",
			"type" => "select",
			"pmks_id" => 4
		]);

		PmksForm::create([
			"title" => "Kalo iya, apa Pendidikan terakhir",
			"nama" => "pendidikan_terakhir",
			"type" => "text",
			"pmks_id" => 4
		]);

		PmksForm::create([
			"title" => "Jika sakit, dirawat/diobati secara medis",
			"nama" => "rawat_medis",
			"type" => "select",
			"pmks_id" => 4
		]);

		PmksForm::create([
			"title" => "Jenis Disabilitas",
			"nama" => "jenis_disabilitas",
			"type" => "text",
			"pmks_id" => 5
		]);

		PmksForm::create([
			"title" => "Penyebab Kecacatan",
			"nama" => "penyebab_kecacatan",
			"type" => "text",
			"pmks_id" => 5
		]);

		PmksForm::create([
			"title" => "Status Tinggal",
			"nama" => "status_tinggal",
			"type" => "text",
			"pmks_id" => 5
		]);

		PmksForm::create([
			"title" => "Memperoleh makanan 4 sehat seminggu yang lalu",
			"nama" => "makanan_sehat",
			"type" => "select",
			"pmks_id" => 5
		]);

		PmksForm::create([
			"title" => "Jika sakit, dirawat/diobati secara medis",
			"nama" => "rawat_medis",
			"type" => "select",
			"pmks_id" => 5
		]);

		PmksForm::create([
			"title" => "Pernah Melakukan Pelanggaran Hukum",
			"nama" => "pelanggaran_hukum",
			"type" => "select",
			"pmks_id" => 6
		]);

		PmksForm::create([
			"title" => "Pernah Mengikuti Proses Pengadilan",
			"nama" => "proses_pengadilan",
			"type" => "select",
			"pmks_id" => 6
		]);

		PmksForm::create([
			"title" => "Pernah Menjalani Masa Hukuman/ Mengikuti Pembinaan dalam Bimbingan Lapas",
			"nama" => "menjalani_hukuman",
			"type" => "select",
			"pmks_id" => 6
		]);

		PmksForm::create([
			"title" => "Korban Perbuatan Pelanggaran Hukum",
			"nama" => "korban_hukum",
			"type" => "select",
			"pmks_id" => 6
		]);

		PmksForm::create([
			"title" => "Korban sengketa Hukum akibat Perceraian Orang Tua",
			"nama" => "korban_sengketa_perceraian",
			"type" => "select",
			"pmks_id" => 6
		]);

		PmksForm::create([
			"title" => "Pernah Menjadi Saksi Tindak Pidana",
			"nama" => "menjadi_saksi",
			"type" => "select",
			"pmks_id" => 6
		]);

		PmksForm::create([
			"title" => "Korban perdagangan/penculikan",
			"nama" => "korban_penculikan",
			"type" => "select",
			"pmks_id" => 7
		]);

		PmksForm::create([
			"title" => "Korban kekerasan fisik/mental",
			"nama" => "korban_kekerasan",
			"type" => "select",
			"pmks_id" => 7
		]);

		PmksForm::create([
			"title" => "Korban Eksploitasi",
			"nama" => "korban_exploitasi",
			"type" => "select",
			"pmks_id" => 7
		]);

		PmksForm::create([
			"title" => "Berasal kelompok minoritas dan terisolasi serta dari komunitas adat terpencil",
			"nama" => "kelompok_minoritas",
			"type" => "select",
			"pmks_id" => 7
		]);

		PmksForm::create([
			"title" => "Korban penyalahgunaan NAPZA",
			"nama" => "korban_napza",
			"type" => "select",
			"pmks_id" => 7
		]);

		PmksForm::create([
			"title" => "Terinfeksi HIV/AIDS",
			"nama" => "terinfeksi_hiv",
			"type" => "select",
			"pmks_id" => 7
		]);

		PmksForm::create([
			"title" => "KEGIATAN YANG DILAKUKAN DIJALAN",
			"nama" => "kegiatan",
			"type" => "text",
			"pmks_id" => 8
		]);

		PmksForm::create([
			"title" => "LOKASI / TEMPAT MANGKAL",
			"nama" => "lokasi",
			"type" => "text",
			"pmks_id" => 8
		]);

		PmksForm::create([
			"title" => "LATAR BELAKANG HIDUP DI JALAN",
			"nama" => "latar_belakang",
			"type" => "text",
			"pmks_id" => 8
		]);

		PmksForm::create([
			"title" => "KEKERASAN YANG DIALAMI",
			"nama" => "kekerasan_dialami",
			"type" => "text",
			"pmks_id" => 9
		]);

		PmksForm::create([
			"title" => "KEKERASAN DILAKUKAN OLEH",
			"nama" => "pelaku_kekerasan",
			"type" => "text",
			"pmks_id" => 9
		]);

		PmksForm::create([
			"title" => "TINDAK KEKERASAN YANG DIALAMI",
			"nama" => "kekerasan_dialami",
			"type" => "text",
			"pmks_id" => 10
		]);

		PmksForm::create([
			"title" => "Eksploitasi Seksual",
			"nama" => "eksploitasi_seksual",
			"type" => "text",
			"pmks_id" => 11
		]);

		PmksForm::create([
			"title" => "Penelantaran",
			"nama" => "penelantaran",
			"type" => "text",
			"pmks_id" => 11
		]);

		PmksForm::create([
			"title" => "Pengusiran",
			"nama" => "pengusiran",
			"type" => "text",
			"pmks_id" => 11
		]);

		PmksForm::create([
			"title" => "TEMPAT MELAKUKAN KEGIATAN",
			"nama" => "tempat_kegiatan",
			"type" => "text",
			"pmks_id" => 12
		]);

		PmksForm::create([
			"title" => "ALASAN MELAKUKAN PEKERJAAN TERSEBUT",
			"nama" => "alasan",
			"type" => "text",
			"pmks_id" => 12
		]);

		PmksForm::create([
			"title" => "TEMPAT MENJALANI PROFESI",
			"nama" => "tempat_profesi",
			"type" => "text",
			"pmks_id" => 13
		]);

		PmksForm::create([
			"title" => "CARA MENJALANI PROFESI",
			"nama" => "cara_menjalani_profesi",
			"type" => "text",
			"pmks_id" => 13
		]);

		PmksForm::create([
			"title" => "LOKASI PEKERJAAN",
			"nama" => "lokasi_pekerjaan",
			"type" => "text",
			"pmks_id" => 14
		]);

		PmksForm::create([
			"title" => "JENIS PROFESI",
			"nama" => "jenis_profesi",
			"type" => "text",
			"pmks_id" => 15
		]);

		PmksForm::create([
			"title" => "JENIS BARANG YANG DIKUMPULKAN",
			"nama" => "jenis_barang",
			"type" => "text",
			"pmks_id" => 15
		]);

		PmksForm::create([
			"title" => "LAMA HUKUMAN YANG PERNAH DIJALANI",
			"nama" => "lama_hukuman",
			"type" => "text",
			"pmks_id" => 16
		]);

		PmksForm::create([
			"title" => "KETERAMPILAN YANG DIPEROLEH DI LP",
			"nama" => "keterampilan",
			"type" => "text",
			"pmks_id" => 16
		]);

		PmksForm::create([
			"title" => "PERNAH MENDAPAT PELAYANAN DARI PEMERINTAH SETELAH KELUAR DARI LP?",
			"nama" => "pasca_lp",
			"type" => "text",
			"pmks_id" => 16
		]);

		PmksForm::create([
			"title" => "JENIS OBAT-OBATAN YANG DIGUNAKAN",
			"nama" => "jenis_obat",
			"type" => "text",
			"pmks_id" => 17
		]);

		PmksForm::create([
			"title" => "JENIS MASALAH YANG SERING DIHADAPI",
			"nama" => "jenis_masalah",
			"type" => "text",
			"pmks_id" => 18
		]);

		PmksForm::create([
			"title" => "JENIS BENCANA",
			"nama" => "jenis_bencana",
			"type" => "text",
			"pmks_id" => 19
		]);

		PmksForm::create([
			"title" => "BANTUAN YANG DIBUTUHKAN",
			"nama" => "bantuan_dibutuhkan",
			"type" => "text",
			"pmks_id" => 19
		]);

		PmksForm::create([
			"title" => "KERUGIAN AKIBAT BENCANA",
			"nama" => "kerugian_bencana",
			"type" => "text",
			"pmks_id" => 19
		]);

		PmksForm::create([
			"title" => "PERMASALAHAN YANG DIHADAPI",
			"nama" => "permasalah_dihadapi",
			"type" => "text",
			"pmks_id" => 20
		]);

		PmksForm::create([
			"title" => "BANTUAN YANG DIBUTUHKAN",
			"nama" => "bantuan_dibutuhkan",
			"type" => "text",
			"pmks_id" => 20
		]);

		PmksForm::create([
			"title" => "JENIS PEKERJA MIGRAN",
			"nama" => "jenis_pekerja",
			"type" => "text",
			"pmks_id" => 21
		]);

		PmksForm::create([
			"title" => "PENYEBAB MASALAH SOSIAL",
			"nama" => "penyebab_masalah",
			"type" => "text",
			"pmks_id" => 21
		]);

		PmksForm::create([
			"title" => "STADIUM",
			"nama" => "stadium",
			"type" => "text",
			"pmks_id" => 22
		]);

		PmksForm::create([
			"title" => "PENYEBAB TERTULAR",
			"nama" => "penyebab_tertular",
			"type" => "text",
			"pmks_id" => 22
		]);

		PmksForm::create([
			"title" => "BANTUAN YANG DIBUTUHKAN",
			"nama" => "bantuan_dibutuhkan",
			"type" => "text",
			"pmks_id" => 22
		]);

		PmksForm::create([
			"title" => "Memperoleh makanan 4 sehat seminggu yang lalu",
			"nama" => "makanan_sehat",
			"type" => "select",
			"pmks_id" => 26
		]);

		PmksForm::create([
			"title" => "Jika sakit, dirawat/diobati secara medis",
			"nama" => "rawat_medis",
			"type" => "select",
			"pmks_id" => 26
		]);

		PmksForm::create([
			"title" => "Ada keluarga yang mau dan mampu mengurus",
			"nama" => "ada_keluarga",
			"type" => "select",
			"pmks_id" => 26
		]);

    }
}
