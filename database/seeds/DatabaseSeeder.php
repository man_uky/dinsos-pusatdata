<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(IntervensisTableSeeder::class);
        $this->call(KecamatansTableSeeder::class);
        $this->call(KelurahansTableSeeder::class);
        $this->call(BidangsTableSeeder::class);
        $this->call(PmksTableSeeder::class);
        $this->call(StatusPenanganansTableSeeder::class);
        $this->call(PmksFormTableSeeder::class);
        $this->call(PenyediaTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        //$this->call(KuotaKelurahansTableSeeder::class);
        //$this->call(KuotaKecamatansTableSeeder::class);

      //10 tabel master_keluarga
      // $a = 'database/users.sql';
      // DB::unprepared(file_get_contents($a));
      // $this->command->info('insert users done !');

      // $b = 'database/clients.sql';
      // DB::unprepared(file_get_contents($b));
      // $this->command->info('insert clients done !');

      // $c = 'database/bidangs.sql';
      // DB::unprepared(file_get_contents($c));
      // $this->command->info('insert bidangs done !');

      // $d = 'database/intervensis.sql';
      // DB::unprepared(file_get_contents($d));
      // $this->command->info('insert intervensis done !');

      // $f = 'database/kecamatans.sql';
      // DB::unprepared(file_get_contents($f));
      // $this->command->info('insert kecamatans done !');

      // $e = 'database/kelurahans.sql';
      // DB::unprepared(file_get_contents($e));
      // $this->command->info('insert kelurahans done !');

      // $g = 'database/files.sql';
      // DB::unprepared(file_get_contents($g));
      // $this->command->info('insert files done !');

      // $h = 'database/client_intervensis.sql';
      // DB::unprepared(file_get_contents($h));
      // $this->command->info('insert client intervensis done !');

      // $i = 'database/pmks_forms.sql';
      // DB::unprepared(file_get_contents($i));
      // $this->command->info('insert pmks forms done !');

      // $j = 'database/form_khususes.sql';
      // DB::unprepared(file_get_contents($j));
      // $this->command->info('insert form khususes done !');

      // $k = 'database/penyedias.sql';
      // DB::unprepared(file_get_contents($k));
      // $this->command->info('insert penyedias done !');
    }
}
