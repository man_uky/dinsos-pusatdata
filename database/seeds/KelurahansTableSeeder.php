<?php

use Illuminate\Database\Seeder;
use App\Kelurahan;
class KelurahansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kelurahan::create([
            "nama" => "ASEMROWO",
            "kecamatan_id"=>"1"
        ]);
        Kelurahan::create([
            "nama" => "GENTING KALIANAK",
            "kecamatan_id"=>"1"
        ]);
        Kelurahan::create([
            "nama" => "TAMBAK SARIOSO",
            "kecamatan_id"=>"1"
        ]);
        Kelurahan::create([
            "nama" => "KANDANGAN",
            "kecamatan_id"=>"2"
        ]);
        Kelurahan::create([
            "nama" => "ROMOKALISARI",
            "kecamatan_id"=>"2"
        ]);
        Kelurahan::create([
            "nama" => "SEMEMI",
            "kecamatan_id"=>"2"
        ]);
        Kelurahan::create([
            "nama" => "TAMBAK OSOWILANGUN",
            "kecamatan_id"=>"2"
        ]);
        Kelurahan::create([
            "nama" => "ALUN-ALUN CONTONG",
            "kecamatan_id"=>"3"
        ]);
        Kelurahan::create([
            "nama" => "BUBUTAN",
            "kecamatan_id"=>"3"
        ]);
        Kelurahan::create([
            "nama" => "GUNDIH",
            "kecamatan_id"=>"3"
        ]);
        Kelurahan::create([
            "nama" => "JEPARA",
            "kecamatan_id"=>"3"
        ]);
        Kelurahan::create([
            "nama" => "TEMBOK DUKUH",
            "kecamatan_id"=>"3"
        ]);
        Kelurahan::create([
            "nama" => "BULAK",
            "kecamatan_id"=>"4"
        ]);
        Kelurahan::create([
            "nama" => "KEDUNG COWEK",
            "kecamatan_id"=>"4"
        ]);
        Kelurahan::create([
            "nama" => "KENJERAN",
            "kecamatan_id"=>"4"
        ]);
        Kelurahan::create([
            "nama" => "SUKOLILO BARU",
            "kecamatan_id"=>"4"
        ]);
        Kelurahan::create([
            "nama" => "DUKUH KUPANG",
            "kecamatan_id"=>"5"
        ]);
        Kelurahan::create([
            "nama" => "DUKUH PAKIS",
            "kecamatan_id"=>"5"
        ]);
        Kelurahan::create([
            "nama" => "GUNUNGSARI",
            "kecamatan_id"=>"5"
        ]);
        Kelurahan::create([
            "nama" => "PRADAH KALI KENDAL",
            "kecamatan_id"=>"5"
        ]);
        Kelurahan::create([
            "nama" => "DUKUH MENANGGAL",
            "kecamatan_id"=>"6"
        ]);
        Kelurahan::create([
            "nama" => "GAYUNGAN",
            "kecamatan_id"=>"6"
        ]);
        Kelurahan::create([
            "nama" => "KETINTANG",
            "kecamatan_id"=>"6"
        ]);
        Kelurahan::create([
            "nama" => "MENANGGAL",
            "kecamatan_id"=>"6"
        ]);
        Kelurahan::create([
            "nama" => "EMBONG KALIASIN",
            "kecamatan_id"=>"7"
        ]);
        Kelurahan::create([
            "nama" => "GENTENG",
            "kecamatan_id"=>"7"
        ]);
        Kelurahan::create([
            "nama" => "KAPASARI",
            "kecamatan_id"=>"7"
        ]);
        Kelurahan::create([
            "nama" => "KETABANG",
            "kecamatan_id"=>"7"
        ]);
        Kelurahan::create([
            "nama" => "PENELEH",
            "kecamatan_id"=>"7"
        ]);
        Kelurahan::create([
            "nama" => "AIRLANGGA",
            "kecamatan_id"=>"8"
        ]);
        Kelurahan::create([
            "nama" => "BARATA JAYA",
            "kecamatan_id"=>"8"
        ]);
        Kelurahan::create([
            "nama" => "GUBENG",
            "kecamatan_id"=>"8"
        ]);
        Kelurahan::create([
            "nama" => "KERTAJAYA",
            "kecamatan_id"=>"8"
        ]);
        Kelurahan::create([
            "nama" => "MOJO",
            "kecamatan_id"=>"8"
        ]);
        Kelurahan::create([
            "nama" => "PUCANG SEWU",
            "kecamatan_id"=>"8"
        ]);
        Kelurahan::create([
            "nama" => "GUNUNGANYAR",
            "kecamatan_id"=>"9"
        ]);
        Kelurahan::create([
            "nama" => "GUNUNGANYAR TAMBAK",
            "kecamatan_id"=>"9"
        ]);
        Kelurahan::create([
            "nama" => "RUNGKUT MENANGGAL",
            "kecamatan_id"=>"9"
        ]);
        Kelurahan::create([
            "nama" => "RUNGKUT TENGAH",
            "kecamatan_id"=>"9"
        ]);
        Kelurahan::create([
            "nama" => "JAMBANGAN",
            "kecamatan_id"=>"10"
        ]);
        Kelurahan::create([
            "nama" => "KARAH",
            "kecamatan_id"=>"10"
        ]);
        Kelurahan::create([
            "nama" => "KEBONSARI",
            "kecamatan_id"=>"10"
        ]);
        Kelurahan::create([
            "nama" => "PAGESANGAN",
            "kecamatan_id"=>"10"
        ]);
        Kelurahan::create([
            "nama" => "KARANG PILANG",
            "kecamatan_id"=>"11"
        ]);
        Kelurahan::create([
            "nama" => "KEBRAON",
            "kecamatan_id"=>"11"
        ]);
        Kelurahan::create([
            "nama" => "KEDURUS",
            "kecamatan_id"=>"11"
        ]);
        Kelurahan::create([
            "nama" => "WARUGUNUNG",
            "kecamatan_id"=>"11"
        ]);
        Kelurahan::create([
            "nama" => "BULAK BANTENG",
            "kecamatan_id"=>"12"
        ]);
        Kelurahan::create([
            "nama" => "SIDOTOPO WETAN",
            "kecamatan_id"=>"12"
        ]);
        Kelurahan::create([
            "nama" => "TAMBAK WEDI",
            "kecamatan_id"=>"12"
        ]);
        Kelurahan::create([
            "nama" => "TANAH KALI KEDINDING",
            "kecamatan_id"=>"12"
        ]);
        Kelurahan::create([
            "nama" => "DUPAK",
            "kecamatan_id"=>"13"
        ]);
        Kelurahan::create([
            "nama" => "KEMAYORAN",
            "kecamatan_id"=>"13"
        ]);
        Kelurahan::create([
            "nama" => "KREMBANGAN SELATAN",
            "kecamatan_id"=>"13"
        ]);
        Kelurahan::create([
            "nama" => "MOROKREMBANGAN",
            "kecamatan_id"=>"13"
        ]);
        Kelurahan::create([
            "nama" => "PERAK BARAT",
            "kecamatan_id"=>"13"
        ]);
        Kelurahan::create([
            "nama" => "BANGKINGAN",
            "kecamatan_id"=>"14"
        ]);
        Kelurahan::create([
            "nama" => "JERUK",
            "kecamatan_id"=>"14"
        ]);
        Kelurahan::create([
            "nama" => "LAKARSANTRI",
            "kecamatan_id"=>"14"
        ]);
        Kelurahan::create([
            "nama" => "LIDAH KULON",
            "kecamatan_id"=>"14"
        ]);
        Kelurahan::create([
            "nama" => "LIDAH WETAN",
            "kecamatan_id"=>"14"
        ]);
        Kelurahan::create([
            "nama" => "SUMUR WELUT",
            "kecamatan_id"=>"14"
        ]);
        Kelurahan::create([
            "nama" => "DUKUH SUTOREJO",
            "kecamatan_id"=>"15"
        ]);
        Kelurahan::create([
            "nama" => "KALIJUDAN",
            "kecamatan_id"=>"15"
        ]);
        Kelurahan::create([
            "nama" => "KALISARI",
            "kecamatan_id"=>"15"
        ]);
        Kelurahan::create([
            "nama" => "KEJAWAN PUTIH TAMBAK",
            "kecamatan_id"=>"15"
        ]);
        Kelurahan::create([
            "nama" => "MANYAR SABRANGAN",
            "kecamatan_id"=>"15"
        ]);
        Kelurahan::create([
            "nama" => "MULYOREJO",
            "kecamatan_id"=>"15"
        ]);
        Kelurahan::create([
            "nama" => "BONGKARAN",
            "kecamatan_id"=>"16"
        ]);
        Kelurahan::create([
            "nama" => "KREMBANGAN UTARA",
            "kecamatan_id"=>"16"
        ]);
        Kelurahan::create([
            "nama" => "NYAMPLUNGAN",
            "kecamatan_id"=>"16"
        ]);
        Kelurahan::create([
            "nama" => "PERAK TIMUR",
            "kecamatan_id"=>"16"
        ]);
        Kelurahan::create([
            "nama" => "PERAK UTARA",
            "kecamatan_id"=>"16"
        ]);
        Kelurahan::create([
            "nama" => "BABAT JERAWAT",
            "kecamatan_id"=>"17"
        ]);
        Kelurahan::create([
            "nama" => "BENOWO",
            "kecamatan_id"=>"17"
        ]);
        Kelurahan::create([
            "nama" => "PAKAL",
            "kecamatan_id"=>"17"
        ]);
        Kelurahan::create([
            "nama" => "SUMBEREJO",
            "kecamatan_id"=>"17"
        ]);
        Kelurahan::create([
            "nama" => "KALIRUNGKUT",
            "kecamatan_id"=>"18"
        ]);
        Kelurahan::create([
            "nama" => "KEDUNG BARUK",
            "kecamatan_id"=>"18"
        ]);
        Kelurahan::create([
            "nama" => "MEDOKAN AYU",
            "kecamatan_id"=>"18"
        ]);
        Kelurahan::create([
            "nama" => "PENJARINGANSARI",
            "kecamatan_id"=>"18"
        ]);
        Kelurahan::create([
            "nama" => "RUNGKUT KIDUL",
            "kecamatan_id"=>"18"
        ]);
        Kelurahan::create([
            "nama" => "WONOREJO",
            "kecamatan_id"=>"18"
        ]);
        Kelurahan::create([
            "nama" => "BRINGIN",
            "kecamatan_id"=>"19"
        ]);
        Kelurahan::create([
            "nama" => "LONTAR",
            "kecamatan_id"=>"19"
        ]);
        Kelurahan::create([
            "nama" => "MADE",
            "kecamatan_id"=>"19"
        ]);
        Kelurahan::create([
            "nama" => "SAMBIKEREP",
            "kecamatan_id"=>"19"
        ]);
        Kelurahan::create([
            "nama" => "BANYU URIP",
            "kecamatan_id"=>"20"
        ]);
        Kelurahan::create([
            "nama" => "KUPANG KRAJAN",
            "kecamatan_id"=>"20"
        ]);
        Kelurahan::create([
            "nama" => "PAKIS",
            "kecamatan_id"=>"20"
        ]);
        Kelurahan::create([
            "nama" => "PETEMON",
            "kecamatan_id"=>"20"
        ]);
        Kelurahan::create([
            "nama" => "PUTAT JAYA",
            "kecamatan_id"=>"20"
        ]);
        Kelurahan::create([
            "nama" => "SAWAHAN",
            "kecamatan_id"=>"20"
        ]);
        Kelurahan::create([
            "nama" => "AMPEL",
            "kecamatan_id"=>"21"
        ]);
        Kelurahan::create([
            "nama" => "PEGIRIAN",
            "kecamatan_id"=>"21"
        ]);
        Kelurahan::create([
            "nama" => "SIDOTOPO",
            "kecamatan_id"=>"21"
        ]);
        Kelurahan::create([
            "nama" => "UJUNG",
            "kecamatan_id"=>"21"
        ]);
        Kelurahan::create([
            "nama" => "WONOKUSUMO",
            "kecamatan_id"=>"21"
        ]);
        Kelurahan::create([
            "nama" => "KAPASAN",
            "kecamatan_id"=>"22"
        ]);
        Kelurahan::create([
            "nama" => "SIDODADI",
            "kecamatan_id"=>"22"
        ]);
        Kelurahan::create([
            "nama" => "SIMOKERTO",
            "kecamatan_id"=>"22"
        ]);
        Kelurahan::create([
            "nama" => "SIMOLAWANG",
            "kecamatan_id"=>"22"
        ]);
        Kelurahan::create([
            "nama" => "TAMBAK REJO",
            "kecamatan_id"=>"22"
        ]);
        Kelurahan::create([
            "nama" => "GEBANG PUTIH",
            "kecamatan_id"=>"23"
        ]);
        Kelurahan::create([
            "nama" => "KEPUTIH",
            "kecamatan_id"=>"23"
        ]);
        Kelurahan::create([
            "nama" => "KLAMPIS NGASEM",
            "kecamatan_id"=>"23"
        ]);
        Kelurahan::create([
            "nama" => "MEDOKAN SEMAMPIR",
            "kecamatan_id"=>"23"
        ]);
        Kelurahan::create([
            "nama" => "MENUR PUMPUNGAN",
            "kecamatan_id"=>"23"
        ]);
        Kelurahan::create([
            "nama" => "NGINDEN JANGKUNGAN",
            "kecamatan_id"=>"23"
        ]);
        Kelurahan::create([
            "nama" => "SEMOLOWARU",
            "kecamatan_id"=>"23"
        ]);
        Kelurahan::create([
            "nama" => "PUTAT GEDE",
            "kecamatan_id"=>"24"
        ]);
        Kelurahan::create([
            "nama" => "SIMOMULYO",
            "kecamatan_id"=>"24"
        ]);
        Kelurahan::create([
            "nama" => "SIMOMULYO BARU",
            "kecamatan_id"=>"24"
        ]);
        Kelurahan::create([
            "nama" => "SONOKWIJENAN",
            "kecamatan_id"=>"24"
        ]);
        Kelurahan::create([
            "nama" => "SUKOMANUNGGAL",
            "kecamatan_id"=>"24"
        ]);
        Kelurahan::create([
            "nama" => "TANJUNGSARI",
            "kecamatan_id"=>"24"
        ]);
        Kelurahan::create([
            "nama" => "DUKUH SETRO",
            "kecamatan_id"=>"25"
        ]);
        Kelurahan::create([
            "nama" => "GADING",
            "kecamatan_id"=>"25"
        ]);
        Kelurahan::create([
            "nama" => "KAPASMADYA BARU",
            "kecamatan_id"=>"25"
        ]);
        Kelurahan::create([
            "nama" => "PACAR KELING",
            "kecamatan_id"=>"25"
        ]);
        Kelurahan::create([
            "nama" => "PACAR KEMBANG",
            "kecamatan_id"=>"25"
        ]);
        Kelurahan::create([
            "nama" => "PLOSO",
            "kecamatan_id"=>"25"
        ]);
        Kelurahan::create([
            "nama" => "RANGKAH",
            "kecamatan_id"=>"25"
        ]);
        Kelurahan::create([
            "nama" => "TAMBAKSARI",
            "kecamatan_id"=>"25"
        ]);
        Kelurahan::create([
            "nama" => "BALONGSARI",
            "kecamatan_id"=>"26"
        ]);
        Kelurahan::create([
            "nama" => "BANJARSUGIHAN",
            "kecamatan_id"=>"26"
        ]);
        Kelurahan::create([
            "nama" => "KARANG POH",
            "kecamatan_id"=>"26"
        ]);
        Kelurahan::create([
            "nama" => "MANUKAN KULON",
            "kecamatan_id"=>"26"
        ]);
        Kelurahan::create([
            "nama" => "MANUKAN WETAN",
            "kecamatan_id"=>"26"
        ]);
        Kelurahan::create([
            "nama" => "TANDES",
            "kecamatan_id"=>"26"
        ]);
        Kelurahan::create([
            "nama" => "Dr. SOETOMO",
            "kecamatan_id"=>"27"
        ]);
        Kelurahan::create([
            "nama" => "KEDUNGDORO",
            "kecamatan_id"=>"27"
        ]);
        Kelurahan::create([
            "nama" => "KEPUTRAN",
            "kecamatan_id"=>"27"
        ]);
        Kelurahan::create([
            "nama" => "TEGALSARI",
            "kecamatan_id"=>"27"
        ]);
        Kelurahan::create([
            "nama" => "KENDANGSARI",
            "kecamatan_id"=>"28"
        ]);
        Kelurahan::create([
            "nama" => "KUTISARI",
            "kecamatan_id"=>"28"
        ]);
        Kelurahan::create([
            "nama" => "PANJANG JIWO",
            "kecamatan_id"=>"28"
        ]);
        Kelurahan::create([
            "nama" => "TENGGILISMEJOYO",
            "kecamatan_id"=>"28"
        ]);
        Kelurahan::create([
            "nama" => "BABATAN",
            "kecamatan_id"=>"29"
        ]);
        Kelurahan::create([
            "nama" => "BALAS KLUMPRIK",
            "kecamatan_id"=>"29"
        ]);
        Kelurahan::create([
            "nama" => "JAJAR TUNGGAL",
            "kecamatan_id"=>"29"
        ]);
        Kelurahan::create([
            "nama" => "WIYUNG",
            "kecamatan_id"=>"29"
        ]);
        Kelurahan::create([
            "nama" => "BENDUL MERISI",
            "kecamatan_id"=>"30"
        ]);
        Kelurahan::create([
            "nama" => "JEMUR WONOSARI",
            "kecamatan_id"=>"30"
        ]);
        Kelurahan::create([
            "nama" => "MARGOREJO",
            "kecamatan_id"=>"30"
        ]);
        Kelurahan::create([
            "nama" => "SIDOSERMO",
            "kecamatan_id"=>"30"
        ]);
        Kelurahan::create([
            "nama" => "SIWALANKERTO",
            "kecamatan_id"=>"30"
        ]);
        Kelurahan::create([
            "nama" => "BENOWO",
            "kecamatan_id"=>"31"
        ]);
        Kelurahan::create([
            "nama" => "DARMO",
            "kecamatan_id"=>"31"
        ]);
        Kelurahan::create([
            "nama" => "JAGIR",
            "kecamatan_id"=>"31"
        ]);
        Kelurahan::create([
            "nama" => "NGAGEL",
            "kecamatan_id"=>"31"
        ]);
        Kelurahan::create([
            "nama" => "NGAGEL REJO",
            "kecamatan_id"=>"31"
        ]);
        Kelurahan::create([
            "nama" => "SAWUNGGALING",
            "kecamatan_id"=>"31"
        ]);
        Kelurahan::create([
            "nama" => "WONOKROMO",
            "kecamatan_id"=>"31"
        ]);

    }
}
