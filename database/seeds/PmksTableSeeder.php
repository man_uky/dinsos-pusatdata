<?php

use Illuminate\Database\Seeder;
use App\Pmks;
class PmksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Pmks::create([
			"nama" => "ANAK BALITA TERLANTAR"
		]);
		Pmks::create([
			"nama" => "ANAK TERLANTAR"
		]);
		Pmks::create([
			"nama" => "PEREMPUAN RAWAN SOSIAL EKONOMI"
		]);
		Pmks::create([
			"nama" => "ANAK DENGAN KEDISABILITASAN(ADK)"
		]);
		Pmks::create([
			"nama" => "PENYANDANG DISABILITAS"
		]);
		Pmks::create([
			"nama" => "ANAK BERHADAPAN DENGAN HUKUM"
		]);
		Pmks::create([
			"nama" => "ANAK YANG MEMERLUKAN PERLINDUNGAN KHUSUS"
		]);
		Pmks::create([
			"nama" => "ANAK JALANAN"
		]);
		Pmks::create([
			"nama" => "ANAK KORBAN TINDAK KEKERASAN"
		]);
		Pmks::create([
			"nama" => "KORBAN TINDAK KEKERASAN"
		]);
		Pmks::create([
			"nama" => "KORBAN TRAFFICKING"
		]);
		Pmks::create([
			"nama" => "TUNA SUSILA"
		]);
		Pmks::create([
			"nama" => "PENGEMIS"
		]);
		Pmks::create([
			"nama" => "GELANDANGAN"
		]);
		Pmks::create([
			"nama" => "PEMULUNG"
		]);
		Pmks::create([
			"nama" => "BEKAS WARGA BINAAN LEMBAGA PEMASYARAKATAN (BWBLP)"
		]);
		Pmks::create([
			"nama" => "KORBAN PENYALAHGUNAAN NAPZA"
		]);
		Pmks::create([
			"nama" => "KELUARGA BERMASALAH PSIKOLOGIS"
		]);
		Pmks::create([
			"nama" => "KORBAN BENCANA ALAM"
		]);
		Pmks::create([
			"nama" => "KORBAN BENCANA SOSIAL"
		]);
		Pmks::create([
			"nama" => "PEKERJA MIGRAN BERMASALAH SOSIAL (PMBS)"
		]);
		Pmks::create([
			"nama" => "ORANG DENGAN HIV / ODHA"
		]);
		Pmks::create([
			"nama" => "KELUARGA FAKIR MISKIN"
		]);
		Pmks::create([
			"nama" => "KELUARGA BERUMAH TAK LAYAK HUNI"
		]);
		Pmks::create([
			"nama" => "KELOMPOK MINORITAS"
		]);
		Pmks::create([
			"nama" => "LANSIA TERLANTAR"
        ]);
        Pmks::create([
			"nama" => "KELUARGA RENTAN SOSIAL"
		]);

    }
}
