<?php

use Illuminate\Database\Seeder;
use App\Intervensi;
class IntervensisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Intervensi::create([
        	'nama'=>'PERMAKANAN GRIYA WERDA',
            'bidang_id'=>1
        ]);
        Intervensi::create([
        	'nama'=>'PERMAKANAN BABAT JERAWAT',
            'bidang_id'=>2
        ]);
        Intervensi::create([
        	'nama'=>'PERMAKANAN KALIJUDAN',
            'bidang_id'=>3
        ]);
        Intervensi::create([
        	'nama'=>'PERMAKANAN KEPUTIH',
            'bidang_id'=>4
        ]);
        Intervensi::create([
        	'nama'=>'PERMAKANAN KAMPUNG ANAK NEGERI',
            'bidang_id'=>5
        ]);
        Intervensi::create([
        	'nama'=>'PERMAKANAN LANSIA',
            'bidang_id'=>6
        ]);
        Intervensi::create([
            'nama'=>'PERMAKANAN PACA',
            'bidang_id'=>6
        ]);
        Intervensi::create([
            'nama'=>'PERMAKANAN YATIM',
            'bidang_id'=>6
        ]);
        Intervensi::create([
            'nama'=>'ISBAT NIKAH',
            'bidang_id'=>6
        ]);
        Intervensi::create([
            'nama'=>'CSR',
            'bidang_id'=>6
        ]);
        Intervensi::create([
            'nama'=>'PEMULANGAN',
            'bidang_id'=>6
        ]);
        Intervensi::create([
            'nama'=>'DLL/OUTREACH',
            'bidang_id'=>6
        ]);
        Intervensi::create([
            'nama'=>'RSDK',
            'bidang_id'=>7
        ]);
        Intervensi::create([
            'nama'=>'PEMAKAMAN',
            'need_verified'=>0,
            'bidang_id'=>7
        ]);
        Intervensi::create([
            'nama'=>'AMBULAN',
            'need_verified'=>0,
            'bidang_id'=>7
        ]);
        Intervensi::create([
            'nama'=>'DLL/OUTREACH',
            'bidang_id'=>7
        ]);
        Intervensi::create([
            'nama'=>'NON APBD',
            'bidang_id'=>8
        ]);
        Intervensi::create([
            'nama'=>'DLL/OUTREACH',
            'bidang_id'=>8
        ]);

    }
}
