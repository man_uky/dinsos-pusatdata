<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'nama' => 'Admin',
            'email' => 'admin@dinsos.com',
            'password' => bcrypt('kucinglucu'),
            'role_id' => ROLE_ADMIN,
        ]);

        User::create([
            'nama' => 'UPTD GRIYA WERDA',
            'email' => 'uptd_griya_werda@dinsos.com',
            'password' => bcrypt('kucinglucu'),
            'role_id' => ROLE_BIDANG,
            'bidang_id' => 1
        ]);

        User::create([
            'nama' => 'UPTD BABAT JERAWAT',
            'email' => 'uptd_babat_jerawat@dinsos.com',
            'password' => bcrypt('kucinglucu'),
            'role_id' => ROLE_BIDANG,
            'bidang_id' => 2
        ]);

        User::create([
            'nama' => 'UPTD KALIJUDAN',
            'email' => 'uptd_kalijudan@dinsos.com',
            'password' => bcrypt('kucinglucu'),
            'role_id' => ROLE_BIDANG,
            'bidang_id' => 3
        ]);

        User::create([
            'nama' => 'UPTD KEPUTIH',
            'email' => 'uptd_keputih@dinsos.com',
            'password' => bcrypt('kucinglucu'),
            'role_id' => ROLE_BIDANG,
            'bidang_id' => 4
        ]);

        User::create([
            'nama' => 'UPTD KAMPUNG ANAK NEGERI',
            'email' => 'uptd_kampung_anak@dinsos.com',
            'password' => bcrypt('kucinglucu'),
            'role_id' => ROLE_BIDANG,
            'bidang_id' => 5
        ]);

        User::create([
            'nama' => 'RESOS',
            'email' => 'resos@dinsos.com',
            'password' => bcrypt('kucinglucu'),
            'role_id' => ROLE_BIDANG,
            'bidang_id' => 6
        ]);

        User::create([
            'nama' => 'KESOS',
            'email' => 'kesos@dinsos.com',
            'password' => bcrypt('kucinglucu'),
            'role_id' => ROLE_BIDANG,
            'bidang_id' => 7
        ]);

        User::create([
            'nama' => 'PUSDASOS',
            'email' => 'pusdasos@dinsos.com',
            'password' => bcrypt('kucinglucu'),
            'role_id' => ROLE_BIDANG,
            'bidang_id' => 8
        ]);

        User::create([
            "email" => "karwed1@dinsos.com",
            "nama_ketua"=>"SUSETIAWATY",
            "nama"=>"Karang Werda Flamboyan (Asemrowo)",
            "alamat"=>"Jl. Asem Raya No. 2 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 1
        ]);

        User::create([
            "email" => "karwed2@dinsos.com",
            "nama_ketua"=>"TAMPI",
            "nama"=>"Karang Werda Kalpataru (Genting Kalianak)",
            "alamat"=>"Jl. Kalianak Barat No. 66 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 2
        ]);

        User::create([
            "email" => "karwed3@dinsos.com",
            "nama_ketua"=>"M. YUSUF",
            "nama"=>"Karang Werda Gesit (Tambak Sarioso)",
            "alamat"=>"Jl. Raya Tambak Langon 45 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 3
        ]);

        User::create([
            "email" => "karwed4@dinsos.com",
            "nama_ketua"=>"SHOFIYATUN ",
            "nama"=>"Karang Werda Pandan Sari (Kandangan)",
            "alamat"=>"Jl. Raya Kandangan No. 18 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 4
        ]);

        User::create([
            "email" => "karwed5@dinsos.com",
            "nama_ketua"=>"SUSILOWATI",
            "nama"=>"Karang Werda Sememi Sehat (Sememi)",
            "alamat"=>"Jl. Raya Kendung Sememi Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 5
        ]);

        User::create([
            "email" => "karwed6@dinsos.com",
            "nama_ketua"=>"ASAN",
            "nama"=>"Karang Werda Romo Gendong (Romo Kalisari)",
            "alamat"=>"Jl. Romokalisari No. 40 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 6
        ]);

        User::create([
            "email" => "karwed7@dinsos.com",
            "nama_ketua"=>"SULATIN",
            "nama"=>"Karang Werda Sedap Malam (Tambak Osowilangun)",
            "alamat"=>"Jl. Tambak Osowilangun Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 7
        ]);

        User::create([
            "email" => "karwed8@dinsos.com",
            "nama_ketua"=>"H. MOHAMMAD SOLEH",
            "nama"=>"Karang Werda Jepara Sejahtera (Jepara)",
            "alamat"=>"Jl. Demak No. 276 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 8
        ]);

        User::create([
            "email" => "karwed9@dinsos.com",
            "nama_ketua"=>"H. HARTOYO,SE,MM",
            "nama"=>"Karang Werda Berguna (Gundih)",
            "alamat"=>"Jl. Gundih V/12 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 9
        ]);

        User::create([
            "email" => "karwed10@dinsos.com",
            "nama_ketua"=>"SABAR SOEASTONO",
            "nama"=>"Karang Werda Blimbing Berseri (Bubutan)",
            "alamat"=>"Jl. Yasan Praja No.20",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 10
        ]);

        User::create([
            "email" => "karwed11@dinsos.com",
            "nama_ketua"=>"BUDI ISWARINI",
            "nama"=>"Karang Werda Tiara Kusuma (Tembok Dukuh)",
            "alamat"=>"Jl. Demak Selatan V/2 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 11
        ]);

        User::create([
            "email" => "karwed12@dinsos.com",
            "nama_ketua"=>"LAILA ENDRIATI",
            "nama"=>"Karang Werda Contong Sejahtera (Alun-Alun Contong)",
            "alamat"=>"Jl. Bubutan V/19 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 12
        ]);

        User::create([
            "email" => "karwed13@dinsos.com",
            "nama_ketua"=>"SITI AISJIJAH",
            "nama"=>"Karang Werda Ceria (Kedung Cowek)",
            "alamat"=>"Jl. Nambangan No. 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 13
        ]);

        User::create([
            "email" => "karwed14@dinsos.com",
            "nama_ketua"=>"MUHADI",
            "nama"=>"Karang Werda Bahari (Sukolilo Baru)",
            "alamat"=>"Jl. Sukolilo No. 7-8 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 14
        ]);

        User::create([
            "email" => "karwed15@dinsos.com",
            "nama_ketua"=>"SOEMADI",
            "nama"=>"Karang Werda Ramayana (Bulak)",
            "alamat"=>"Jl. Kyai Tambak Deres No. 04 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 15
        ]);

        User::create([
            "email" => "karwed16@dinsos.com",
            "nama_ketua"=>"ROSARIA SULAMI",
            "nama"=>"Karang Werda Lautan Berlian (Kenjeran)",
            "alamat"=>"Jl. Tambak Deres No. 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 16
        ]);

        User::create([
            "email" => "karwed17@dinsos.com",
            "nama_ketua"=>"DARMANI",
            "nama"=>"Karang Werda Among Tresno (Gunung Sari)",
            "alamat"=>"Jl. Kencanasari Timur XI/101 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 17
        ]);

        User::create([
            "email" => "karwed18@dinsos.com",
            "nama_ketua"=>"SUDIRMAT",
            "nama"=>"Karang Werda Dharma Sentosa (Pradah Kali Kendal)",
            "alamat"=>"Jl. Hr.Muhammad No. 167 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 18
        ]);

        User::create([
            "email" => "karwed19@dinsos.com",
            "nama_ketua"=>"SOEDARMAN",
            "nama"=>"Karang Werda Bugar Mandiri (Dukuh Kupang)",
            "alamat"=>"Jl. Dukuh Kupang XI/1A Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 19
        ]);

        User::create([
            "email" => "karwed20@dinsos.com",
            "nama_ketua"=>"SAMAD",
            "nama"=>"Karang Werda Sejahtera (Dukuh Pakis)",
            "alamat"=>"Jl. Dukuh Pakis I/21 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 20
        ]);

        User::create([
            "email" => "karwed21@dinsos.com",
            "nama_ketua"=>"DRS. WIDODO",
            "nama"=>"Karang Werda Setya Budi (Ketintang)",
            "alamat"=>"Jl. Ketintang Madya No. 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 21
        ]);

        User::create([
            "email" => "karwed22@dinsos.com",
            "nama_ketua"=>"SARDJONO",
            "nama"=>"Karang Werda Bimo Suci (Gayungan)",
            "alamat"=>"Jl. Achmad Yani No. 222-A Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 22
        ]);

        User::create([
            "email" => "karwed23@dinsos.com",
            "nama_ketua"=>"EUNICE PUJIANTI",
            "nama"=>"Karang Werda Cahyo Suminar (Menanggal)",
            "alamat"=>"Jl. Cipta Menanggal I/11 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 23
        ]);

        User::create([
            "email" => "karwed24@dinsos.com",
            "nama_ketua"=>"ARIESTA KARMADI",
            "nama"=>"Karang Werda Sekar Widuri (Dukuh Menanggal)",
            "alamat"=>"Jl. Dukuh Menanggal 12/06 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 24
        ]);

        User::create([
            "email" => "karwed25@dinsos.com",
            "nama_ketua"=>"HARIYANTO ",
            "nama"=>"Karang Werda Yudhistira (Ketabang)",
            "alamat"=>"Jl. Ambengan No. 36 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 25
        ]);

        User::create([
            "email" => "karwed26@dinsos.com",
            "nama_ketua"=>"NISAP PRAJITNO",
            "nama"=>"Karang Werda Yudhistira (Kapasari)",
            "alamat"=>"Jl. Kapasari III/27 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 26
        ]);

        User::create([
            "email" => "karwed27@dinsos.com",
            "nama_ketua"=>"ABDUL GANI FARCHAN",
            "nama"=>"Karang Werda Kel. Peneleh (Peneleh)",
            "alamat"=>"Jl. Mas Soendjoto No. 4 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 27
        ]);

        User::create([
            "email" => "karwed28@dinsos.com",
            "nama_ketua"=>"H. MOCH SALAMUN ",
            "nama"=>"Karang Werda Sehat Sejahtera (Embong Kaliasin)",
            "alamat"=>"Jl. Embong Sawo No. 10 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 28
        ]);

        User::create([
            "email" => "karwed29@dinsos.com",
            "nama_ketua"=>"SITI HALIMAH ",
            "nama"=>"Karang Werda Lestari (Genteng)",
            "alamat"=>"Jl. Genteng Muhammadiyah No. 9 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 29
        ]);

        User::create([
            "email" => "karwed30@dinsos.com",
            "nama_ketua"=>"MOCHAMAD UMBARI,SE",
            "nama"=>"Karang Werda Gagak Rimang (Gubeng)",
            "alamat"=>"Jl. Nias No. 24 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 30
        ]);

        User::create([
            "email" => "karwed31@dinsos.com",
            "nama_ketua"=>"STEVANUS SOEGIHARTO,SH",
            "nama"=>"Karang Werda Pucang Kencana (Pucang Sewu)",
            "alamat"=>"Jl. Pucang Sewu No. 8 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 31
        ]);

        User::create([
            "email" => "karwed32@dinsos.com",
            "nama_ketua"=>"H. HAROEN,DRS AAAI-J ",
            "nama"=>"Karang Werda Rahayu (Mojo)",
            "alamat"=>"Jl. Kalidami 41 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 32
        ]);

        User::create([
            "email" => "karwed33@dinsos.com",
            "nama_ketua"=>"R. JERRY SUDYONO,SE",
            "nama"=>"Karang Werda Airlangga (Airlangga)",
            "alamat"=>"Jl. Gubeng Kertajaya 9-C/42 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 33
        ]);

        User::create([
            "email" => "karwed34@dinsos.com",
            "nama_ketua"=>"B. PURNOMO ",
            "nama"=>"Karang Werda Sekar Anggrek (Kertajaya)",
            "alamat"=>"Jl. Pucang Adi 116 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 34
        ]);

        User::create([
            "email" => "karwed35@dinsos.com",
            "nama_ketua"=>"DRS. NURSALIM SISWO",
            "nama"=>"Karang Werda Bokor Kencono (Barata Jaya)",
            "alamat"=>"Jl. Manyar No. 80 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 35
        ]);

        User::create([
            "email" => "karwed36@dinsos.com",
            "nama_ketua"=>"SITI PURWANINGSIH",
            "nama"=>"Karang Werda Mawar Indah (Gunung Anyar Tambak)",
            "alamat"=>"Jl. Raya Wiguna Timur No. 66-A Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 36
        ]);

        User::create([
            "email" => "karwed37@dinsos.com",
            "nama_ketua"=>"HJ. SITI SHOLIHAH",
            "nama"=>"Karang Werda Sejahtera (Gunung Anyar )",
            "alamat"=>"Jl. Gununganyar Timur No. 64 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 37
        ]);

        User::create([
            "email" => "karwed38@dinsos.com",
            "nama_ketua"=>"SUPARMAN",
            "nama"=>"Karang Werda Resi Bimo (Rungkut Tengah)",
            "alamat"=>"Jl. Rungkut Permai II/1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 38
        ]);

        User::create([
            "email" => "karwed39@dinsos.com",
            "nama_ketua"=>"TONTOWI DJAUHARI",
            "nama"=>"Karang Werda Menanggal Jaya (Rungkut Menanggal)",
            "alamat"=>"Jl. Raya Rungkut Menanggal No. 11 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 39
        ]);

        User::create([
            "email" => "karwed40@dinsos.com",
            "nama_ketua"=>"DRA. SUMIYATI,M.SI",
            "nama"=>"Karang Werda Wiguna Karya (Kebonsari)",
            "alamat"=>"Jl. Kebonsari Manunggal No. 22 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 40
        ]);

        User::create([
            "email" => "karwed41@dinsos.com",
            "nama_ketua"=>"WASITO",
            "nama"=>"Karang Werda Senja Sejahtera (Jambangan)",
            "alamat"=>"Jl. Jambangan Sawah 1-3 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 41
        ]);

        User::create([
            "email" => "karwed42@dinsos.com",
            "nama_ketua"=>"PRAHMANA RIZA",
            "nama"=>"Karang Werda Wulan Bayu Aji (Karah)",
            "alamat"=>"Jl. Bibis Karah No. 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 42
        ]);

        User::create([
            "email" => "karwed43@dinsos.com",
            "nama_ketua"=>"SUNGKONO",
            "nama"=>"Karang Werda Budi Mulya (Pagesangan)",
            "alamat"=>"Jl. Pagesangan III/36 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 43
        ]);

        User::create([
            "email" => "karwed44@dinsos.com",
            "nama_ketua"=>"SOLI",
            "nama"=>"Karang Werda Bima Shakti (Kebraon)",
            "alamat"=>"Jl. Griya Kebraon No. 2 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 44
        ]);

        User::create([
            "email" => "karwed45@dinsos.com",
            "nama_ketua"=>"SAIMIN",
            "nama"=>"Karang Werda Bima Shakti (Kedurus)",
            "alamat"=>"Jl. Mastrip Kedurus No. 34 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 45
        ]);

        User::create([
            "email" => "karwed46@dinsos.com",
            "nama_ketua"=>"MANIRAN",
            "nama"=>"Karang Werda Anggrek (Karang Pilang)",
            "alamat"=>"Jl. Ksatria No. 10 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 46
        ]);

        User::create([
            "email" => "karwed47@dinsos.com",
            "nama_ketua"=>"MALAR",
            "nama"=>"Karang Werda Seger Waras (Warugunung)",
            "alamat"=>"Jl. Mastrip Warugunung Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 47
        ]);

        User::create([
            "email" => "karwed48@dinsos.com",
            "nama_ketua"=>"TUMINI",
            "nama"=>"Karang Werda Puspa Indah (Sidotopo Wetan)",
            "alamat"=>"Jl. Sidotopo Wetan 1-L/ No. 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 48
        ]);

        User::create([
            "email" => "karwed49@dinsos.com",
            "nama_ketua"=>"H. DJOKO SUWIRYO,DRS",
            "nama"=>"Karang Werda Jumanis (Tanah Kali Kedinding)",
            "alamat"=>"Jl. Kedung Cowek 348 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 49
        ]);

        User::create([
            "email" => "karwed50@dinsos.com",
            "nama_ketua"=>"I NYOMAN NENDRA",
            "nama"=>"Karang Werda Yuswo Aji (Bulak Banteng)",
            "alamat"=>"Jl. Bulak Banteng Lor 1/27 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 50
        ]);

        User::create([
            "email" => "karwed51@dinsos.com",
            "nama_ketua"=>"EDHI RIYANTO",
            "nama"=>"Karang Werda Bima Sakti (Tambak Wedi)",
            "alamat"=>"Jl. Tambak Wedi No. 135 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 51
        ]);

        User::create([
            "email" => "karwed52@dinsos.com",
            "nama_ketua"=>"SUWARTINI ",
            "nama"=>"Karang Werda Batara Krisna (Dupak)",
            "alamat"=>"Jl. Dupak Bandarejo I/11 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 52
        ]);

        User::create([
            "email" => "karwed53@dinsos.com",
            "nama_ketua"=>"YOHANES SOEKARDJONO",
            "nama"=>"Karang Werda Kirana (Kemayoran)",
            "alamat"=>"Jl. Krembangan Baru No. 49 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 53
        ]);

        User::create([
            "email" => "karwed54@dinsos.com",
            "nama_ketua"=>"RAMELAN",
            "nama"=>"Karang Werda Kerto Raharjo (Krembangan Selatan)",
            "alamat"=>"Jl. Pesapen Selatan No. 4",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 54
        ]);

        User::create([
            "email" => "karwed55@dinsos.com",
            "nama_ketua"=>"T. HENDARTININGSIH ",
            "nama"=>"Karang Werda Kel. Perak Barat (Perak Barat)",
            "alamat"=>"Jl. Ikan Dorang No. 17-19 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 55
        ]);

        User::create([
            "email" => "karwed56@dinsos.com",
            "nama_ketua"=>"SISWO SUMADI,IR",
            "nama"=>"Karang Werda Pandu Dewonoto (Morokrembangan)",
            "alamat"=>"Jl. Sedayu IV/30 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 56
        ]);

        User::create([
            "email" => "karwed57@dinsos.com",
            "nama_ketua"=>"ARIE TRI WIDYASARI",
            "nama"=>"Karang Werda Mitra Sepuh (Sumur Welut)",
            "alamat"=>"Jl. Sumur Welut No. 2 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 57
        ]);

        User::create([
            "email" => "karwed58@dinsos.com",
            "nama_ketua"=>"SUKARDI",
            "nama"=>"Karang Werda Sawunggaling (Lidah Wetan)",
            "alamat"=>"Jl. Raya Menganti No. 27 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 58
        ]);

        User::create([
            "email" => "karwed59@dinsos.com",
            "nama_ketua"=>"SUTOMO AGUS HARIANTO",
            "nama"=>"Karang Werda Krisna (Lidah Kulon)",
            "alamat"=>"Jl. Raya Menganti Lidah Kulon Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 59
        ]);

        User::create([
            "email" => "karwed60@dinsos.com",
            "nama_ketua"=>"KARMINEM",
            "nama"=>"Karang Werda Jeruk Ceria (Jeruk)",
            "alamat"=>"Jl. Raya Menganti No. 125 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 60
        ]);

        User::create([
            "email" => "karwed61@dinsos.com",
            "nama_ketua"=>"ETIK SURYANINGSIH",
            "nama"=>"Karang Werda Lakarsantri Jaya (Lakarsantri)",
            "alamat"=>"Jl. Raya Lakarsantri No. 36 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 61
        ]);

        User::create([
            "email" => "karwed62@dinsos.com",
            "nama_ketua"=>"SUWADI",
            "nama"=>"Karang Werda Aji Soko (Bangkingan)",
            "alamat"=>"Jl. Bangkingan Barat Gang Asep Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 62
        ]);

        User::create([
            "email" => "karwed63@dinsos.com",
            "nama_ketua"=>"KASTINATUN MUNAWAROH ",
            "nama"=>"Karang Werda Mulyorejo Mandiri (Mulyorejo)",
            "alamat"=>"Jl. Mulyorejo Utara 201 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 63
        ]);

        User::create([
            "email" => "karwed64@dinsos.com",
            "nama_ketua"=>"AMIK SLAMET",
            "nama"=>"Karang Werda Bima Suci (Manyar Sabrangan)",
            "alamat"=>"Jl. Manyar Kertoadi No. 14 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 64
        ]);

        User::create([
            "email" => "karwed65@dinsos.com",
            "nama_ketua"=>"DADIJANA",
            "nama"=>"Karang Werda Sugih Karya (Kjawan Putih Tambak)",
            "alamat"=>"Jl. Kejawanputih Tambak No. 48 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 65
        ]);

        User::create([
            "email" => "karwed66@dinsos.com",
            "nama_ketua"=>"KASPIADI",
            "nama"=>"Karang Werda Mekar Sari (Kalisari)",
            "alamat"=>"Jl. Mulyosari Tengah VII/76 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 66
        ]);

        User::create([
            "email" => "karwed67@dinsos.com",
            "nama_ketua"=>"H. EDDY RUSIANTO,SH. MH",
            "nama"=>"Karang Werda Tunggal Wisma (Dukuh Sutorejo)",
            "alamat"=>"Jl. Labansari No. 01 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 67
        ]);

        User::create([
            "email" => "karwed68@dinsos.com",
            "nama_ketua"=>"SENTOT BUDIARTO",
            "nama"=>"Karang Werda Sejahtera (Kalijudan)",
            "alamat"=>"Jl. Kalijudan No. 123",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 68
        ]);

        User::create([
            "email" => "karwed69@dinsos.com",
            "nama_ketua"=>"JALALI",
            "nama"=>"Karang Werda Kel. Nyamplungan (Nyamplungan)",
            "alamat"=>"Jl. Kh. Mansyur I/73 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 69
        ]);

        User::create([
            "email" => "karwed70@dinsos.com",
            "nama_ketua"=>"SIPIN ",
            "nama"=>"Karang Werda Kresna (Perak Timur)",
            "alamat"=>"Jl. Johor I-A Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 70
        ]);

        User::create([
            "email" => "karwed71@dinsos.com",
            "nama_ketua"=>"SUWARNO",
            "nama"=>"Karang Werda Ismoyo (Perak Utara)",
            "alamat"=>"Jl. Teluk Kumai Barat No 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 71
        ]);

        User::create([
            "email" => "karwed72@dinsos.com",
            "nama_ketua"=>"AGUS SOEYONO",
            "nama"=>"Karang Werda Kel. Krembangan Utara (Krembangan Utara)",
            "alamat"=>"Jl. Kalisosok No. 27 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 72
        ]);

        User::create([
            "email" => "karwed73@dinsos.com",
            "nama_ketua"=>"SUNDARI",
            "nama"=>"Karang Werda Bahagia (Bongkaran)",
            "alamat"=>"Jl. Coklat No. 5 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 73
        ]);

        User::create([
            "email" => "karwed74@dinsos.com",
            "nama_ketua"=>"H. MOCH. THOHIR GHOZALI",
            "nama"=>"Karang Werda Setia Budi (Sumberejo)",
            "alamat"=>"Jl. Raya Sumberrejo Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 74
        ]);

        User::create([
            "email" => "karwed75@dinsos.com",
            "nama_ketua"=>"MUJAROH",
            "nama"=>"Karang Werda Purna Bhakti (Pakal)",
            "alamat"=>"Jl. Pakal Amd Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 75
        ]);

        User::create([
            "email" => "karwed76@dinsos.com",
            "nama_ketua"=>"RADJIMAN",
            "nama"=>"Karang Werda Gotong Royong (Babat Jerawat)",
            "alamat"=>"Jl. Raya Babat Jerawat No. 2 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 76
        ]);

        User::create([
            "email" => "karwed77@dinsos.com",
            "nama_ketua"=>"H. UMAN MAKSUM",
            "nama"=>"Karang Werda Budi Luhur (Benowo)",
            "alamat"=>"Jl. Raya Benowo No. 88 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 77
        ]);

        User::create([
            "email" => "karwed78@dinsos.com",
            "nama_ketua"=>"ENDANG SRIHARINI",
            "nama"=>"Karang Werda Rojo Mino (Medokan Ayu)",
            "alamat"=>"Jl. Medokan Asri Utara Iv No. 35 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 78
        ]);

        User::create([
            "email" => "karwed79@dinsos.com",
            "nama_ketua"=>"DARCHO SOEDARSONO,SH",
            "nama"=>"Karang Werda Rahayu (Kedung Baruk)",
            "alamat"=>"Jl. Raya Kedung Baruk No. 6 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 79
        ]);

        User::create([
            "email" => "karwed80@dinsos.com",
            "nama_ketua"=>"SOEHARTO",
            "nama"=>"Karang Werda Setia Bhakti (Rungkut Kidul)",
            "alamat"=>"Jl. Rungkut Asri No. 3 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 80
        ]);

        User::create([
            "email" => "karwed81@dinsos.com",
            "nama_ketua"=>"ISMI'ATAN ",
            "nama"=>"Karang Werda Teratai (Penjaringansari)",
            "alamat"=>"Jl. Kendalsari Selatan No. 28 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 81
        ]);

        User::create([
            "email" => "karwed82@dinsos.com",
            "nama_ketua"=>"MUHAMAD AS'ARIE JAUHARI",
            "nama"=>"Karang Werda Sapto Argo (Wonorejo)",
            "alamat"=>"Jl. Raya Wonorejo No. 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 82
        ]);

        User::create([
            "email" => "karwed83@dinsos.com",
            "nama_ketua"=>"KUSNARYADI ",
            "nama"=>"Karang Werda Kel. Kali Rungkut (Kali Rungkut)",
            "alamat"=>"Jl. Rungkut Asri Utara1/2 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 83
        ]);

        User::create([
            "email" => "karwed84@dinsos.com",
            "nama_ketua"=>"MARDIO",
            "nama"=>"Karang Werda Dahlia (Beringin)",
            "alamat"=>"Jl. Raya Bringin No. 11 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 84
        ]);

        User::create([
            "email" => "karwed85@dinsos.com",
            "nama_ketua"=>"NUNIK SITI SUNDARI,SKM,M. Kes",
            "nama"=>"Karang Werda Sawunggaling (Sambikerep)",
            "alamat"=>"Jl. Raya Sambikerep No. 121 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 85
        ]);

        User::create([
            "email" => "karwed86@dinsos.com",
            "nama_ketua"=>"SADI",
            "nama"=>"Karang Werda Karya Mandiri (Made)",
            "alamat"=>"Jl. Raya Made No. 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 86
        ]);

        User::create([
            "email" => "karwed87@dinsos.com",
            "nama_ketua"=>"DRS. SOEROTO",
            "nama"=>"Karang Werda Budi Luhur (Lontar)",
            "alamat"=>"Jl. Raya Lontar No. 5 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 87
        ]);

        User::create([
            "email" => "karwed88@dinsos.com",
            "nama_ketua"=>"SRADDHA VIRA SUGIJONO",
            "nama"=>"Karang Werda Sadewa (Pakis)",
            "alamat"=>"Jl. Dukuh Kupang Timur XX/791 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 88
        ]);

        User::create([
            "email" => "karwed89@dinsos.com",
            "nama_ketua"=>"SOEKALIL",
            "nama"=>"Karang Werda Abimanyu (Kupang Krajan)",
            "alamat"=>"Jl. Petemon Barat 187 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 89
        ]);

        User::create([
            "email" => "karwed90@dinsos.com",
            "nama_ketua"=>"AGUNG PAMUDJI",
            "nama"=>"Karang Werda Arjuna (Putat Jaya)",
            "alamat"=>"Jl. Raya Dukuh Kupang No. 5 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 90
        ]);

        User::create([
            "email" => "karwed91@dinsos.com",
            "nama_ketua"=>"ROESKAN",
            "nama"=>"Karang Werda Bima (Banyu Urip)",
            "alamat"=>"Jl. Banyu Urip Kidul IV/39 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 91
        ]);

        User::create([
            "email" => "karwed92@dinsos.com",
            "nama_ketua"=>"MOCH ERFAN",
            "nama"=>"Karang Werda Yudhistira (Sawahan)",
            "alamat"=>"Jl. Raya Arjuna No. 121 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 92
        ]);

        User::create([
            "email" => "karwed93@dinsos.com",
            "nama_ketua"=>"SOEWARSONO IR,MT",
            "nama"=>"Karang Werda Budi Rahayu (Petemon)",
            "alamat"=>"Jl. Petemon Barat No. 132 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 93
        ]);

        User::create([
            "email" => "karwed94@dinsos.com",
            "nama_ketua"=>"ACHMAD BASHORI",
            "nama"=>"Karang Werda Sakinah (Ampel)",
            "alamat"=>"Jl. Pegirian No. 240-244 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 94
        ]);

        User::create([
            "email" => "karwed95@dinsos.com",
            "nama_ketua"=>"BERDIK RULIYANTO",
            "nama"=>"Karang Werda Pilar Warga (Sidotopo)",
            "alamat"=>"Jl. Sidotopo Kulon No. 331 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 95
        ]);

        User::create([
            "email" => "karwed96@dinsos.com",
            "nama_ketua"=>"TAMADJI",
            "nama"=>"Karang Werda Abiyoso (Pegirian )",
            "alamat"=>"Jl. Wonokusumo Kidul 42 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 96
        ]);

        User::create([
            "email" => "karwed97@dinsos.com",
            "nama_ketua"=>"ISNARNI",
            "nama"=>"Karang Werda Kusuma (Wonokusumo )",
            "alamat"=>"Jl. Bulaksari V/14 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 97
        ]);

        User::create([
            "email" => "karwed98@dinsos.com",
            "nama_ketua"=>"SLAMET SOEMENDI",
            "nama"=>"Karang Werda Ujung Jaya Bersatu (Ujung)",
            "alamat"=>"Jl. Sawah Pulo Sr No. 2 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 98
        ]);

        User::create([
            "email" => "karwed99@dinsos.com",
            "nama_ketua"=>"MAPE",
            "nama"=>"Karang Werda Bugar Sejahtera (Simolawang)",
            "alamat"=>"Jl. Sidodadi IV No. 20 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 99
        ]);

        User::create([
            "email" => "karwed100@dinsos.com",
            "nama_ketua"=>"SOEWARNO",
            "nama"=>"Karang Werda Kel. Simokerto (Simokerto)",
            "alamat"=>"Jl. Simokerto No. 77 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 100
        ]);

        User::create([
            "email" => "karwed101@dinsos.com",
            "nama_ketua"=>"MARCHUMAH",
            "nama"=>"Karang Werda Sidodadi Sehat Sejahtera (Sidodadi)",
            "alamat"=>"Jl. Sidodadi IX/6 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 101
        ]);

        User::create([
            "email" => "karwed102@dinsos.com",
            "nama_ketua"=>"SUKAMTO",
            "nama"=>"Karang Werda Sehat Sejahtera (Kapasan)",
            "alamat"=>"Jl. Kapasan Dalam 3/4 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 102
        ]);

        User::create([
            "email" => "karwed103@dinsos.com",
            "nama_ketua"=>"PAUL LUMANSIK",
            "nama"=>"Karang Werda Sehat Bahagia (Tambak Rejo)",
            "alamat"=>"Jl. Ngaglik No. 87 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 103
        ]);

        User::create([
            "email" => "karwed104@dinsos.com",
            "nama_ketua"=>"SUJADI",
            "nama"=>"Karang Werda Mesem (Medokan Semampir)",
            "alamat"=>"Jl. Semampir Tengah No. 63 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 104
        ]);

        User::create([
            "email" => "karwed105@dinsos.com",
            "nama_ketua"=>"H. MOHAMMAD DACHLAN,IR",
            "nama"=>"Karang Werda Nginden Ceria (Nginden Jangkungan)",
            "alamat"=>"Jl. Nginden Baru VI/28 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 105
        ]);

        User::create([
            "email" => "karwed106@dinsos.com",
            "nama_ketua"=>"IDA ELISTYOWATI",
            "nama"=>"Karang Werda Pandu Dewanata (Gebang Putih)",
            "alamat"=>"Jl. Gebang Putih No. 62 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 106
        ]);

        User::create([
            "email" => "karwed107@dinsos.com",
            "nama_ketua"=>"H. MULYADI",
            "nama"=>"Karang Werda Mutiara (Klampis Ngasem)",
            "alamat"=>"Jl. Arief Rahman Hakim No. 103-C Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 107
        ]);

        User::create([
            "email" => "karwed108@dinsos.com",
            "nama_ketua"=>"MOH. HADI",
            "nama"=>"Karang Werda Asih Sejahtera (Menur Pumpungan)",
            "alamat"=>"Jl. Manyar Jaya VIII Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 108
        ]);

        User::create([
            "email" => "karwed109@dinsos.com",
            "nama_ketua"=>"MOHAMMAD IKHSAN",
            "nama"=>"Karang Werda Sehat Sejahtera (Keputih)",
            "alamat"=>"Jl. Keputih Tegal No. 25 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 109
        ]);

        User::create([
            "email" => "karwed110@dinsos.com",
            "nama_ketua"=>"MIDIN",
            "nama"=>"Karang Werda Prima Jaya (Semolowaru)",
            "alamat"=>"Jl. Semolowaru No.160 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 110
        ]);

        User::create([
            "email" => "karwed111@dinsos.com",
            "nama_ketua"=>"SURAMIN KARMEN",
            "nama"=>"Karang Werda Mulyo Raharjo (Simomulyo)",
            "alamat"=>"Jl. Simomulyo I No. 59 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 111
        ]);

        User::create([
            "email" => "karwed112@dinsos.com",
            "nama_ketua"=>"SAMPUN PRAYITNO",
            "nama"=>"Karang Werda Mulyo Sembodo (Simomulyo Baru)",
            "alamat"=>"Jl. Simo Gunung Barat Tol 1/65 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 112
        ]);

        User::create([
            "email" => "karwed113@dinsos.com",
            "nama_ketua"=>"H. KASTARI,BA",
            "nama"=>"Karang Werda Bahagia (Sukomanunggal)",
            "alamat"=>"Jl. Donowati 1 Nomer 8 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 113
        ]);

        User::create([
            "email" => "karwed114@dinsos.com",
            "nama_ketua"=>"HERI SUPRIANTO",
            "nama"=>"Karang Werda Pendowo (Putat Gede)",
            "alamat"=>"Jl. Hr Muhammad No. 30 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 114
        ]);

        User::create([
            "email" => "karwed115@dinsos.com",
            "nama_ketua"=>"KASTINI",
            "nama"=>"Karang Werda Sono Werda (Sono Kawijenan)",
            "alamat"=>"Jl. Kupang Jaya I No. 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 115
        ]);

        User::create([
            "email" => "karwed116@dinsos.com",
            "nama_ketua"=>"JATNO",
            "nama"=>"Karang Werda Punto Dewo (Tanjungsari)",
            "alamat"=>"Jl. Tanjungsari No. 72 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 116
        ]);

        User::create([
            "email" => "karwed117@dinsos.com",
            "nama_ketua"=>"YOHANES NUGROHO",
            "nama"=>"Karang Werda Melati (Pacar Kembang)",
            "alamat"=>"Jl. Bronggalan II No. 24 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 117
        ]);

        User::create([
            "email" => "karwed118@dinsos.com",
            "nama_ketua"=>"DJAMAL",
            "nama"=>"Karang Werda Bimo Seno (Rangkah)",
            "alamat"=>"Jl. Alun-Alun Rangkah No. 25 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 118
        ]);

        User::create([
            "email" => "karwed119@dinsos.com",
            "nama_ketua"=>"SITI SOVIA ",
            "nama"=>"Karang Werda Abimanyu (Pacar Keling)",
            "alamat"=>"Jl. Jolotundo Baru III/14 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 119
        ]);

        User::create([
            "email" => "karwed120@dinsos.com",
            "nama_ketua"=>"NANIK HARINI",
            "nama"=>"Karang Werda Gading Murni (Gading)",
            "alamat"=>"Jl. Kenjeran No. 424 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 120
        ]);

        User::create([
            "email" => "karwed121@dinsos.com",
            "nama_ketua"=>"SARITO",
            "nama"=>"Karang Werda Merpati Putih (Dukuh Setro)",
            "alamat"=>"Jl. Setro Baru X No. 2 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 121
        ]);

        User::create([
            "email" => "karwed122@dinsos.com",
            "nama_ketua"=>"MUDJIONO",
            "nama"=>"Karang Werda Tetep Seger (Kapasmadya Baru)",
            "alamat"=>"Jl. Kapasmadya II No. 54 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 122
        ]);

        User::create([
            "email" => "karwed123@dinsos.com",
            "nama_ketua"=>"IMMI DIAN TUMI HANDAYANI",
            "nama"=>"Karang Werda Kusuma (Ploso)",
            "alamat"=>"Jl. Ploso Bogen 43 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 123
        ]);

        User::create([
            "email" => "karwed124@dinsos.com",
            "nama_ketua"=>"SOEDIBYO ",
            "nama"=>"Karang Werda Pandu (Tambaksari)",
            "alamat"=>"Jl. Tambaksari No. 35 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 124
        ]);

        User::create([
            "email" => "karwed125@dinsos.com",
            "nama_ketua"=>"SAPI'I",
            "nama"=>"Karang Werda Kutilang (Manukan Wetan)",
            "alamat"=>"Jl. Sikatan II No. 71 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 125
        ]);

        User::create([
            "email" => "karwed126@dinsos.com",
            "nama_ketua"=>"JULUNG AMBARWULAN",
            "nama"=>"Karang Werda Amalia (Karang Poh)",
            "alamat"=>"Jl. Darmo Indah Asri No. 2 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 126
        ]);

        User::create([
            "email" => "karwed127@dinsos.com",
            "nama_ketua"=>"ABDUL AZIS,S.Sos",
            "nama"=>"Karang Werda Ismoyo (Tandes)",
            "alamat"=>"Jl. Darmo Indah Blok K/10 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 127
        ]);

        User::create([
            "email" => "karwed128@dinsos.com",
            "nama_ketua"=>"SUDARJANTO",
            "nama"=>"Karang Werda Cipto Ning (Balongsari)",
            "alamat"=>"Jl. Balongsari Krajan II/123 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 128
        ]);

        User::create([
            "email" => "karwed129@dinsos.com",
            "nama_ketua"=>"NASIR HADRI",
            "nama"=>"Karang Werda Perskama (Manukan Kulon)",
            "alamat"=>"Jl. Manukanasri I-A Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 129
        ]);

        User::create([
            "email" => "karwed130@dinsos.com",
            "nama_ketua"=>"ASPI'IE",
            "nama"=>"Karang Werda Manikmoyo (Banjarsugihan)",
            "alamat"=>"Jl. Raya Banjarsugihan No. 18 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 130
        ]);

        User::create([
            "email" => "karwed131@dinsos.com",
            "nama_ketua"=>"BAMBANG WULYO H.",
            "nama"=>"Karang Werda Kresna (Keputran)",
            "alamat"=>"Jl. Doho No. 20 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 131
        ]);

        User::create([
            "email" => "karwed132@dinsos.com",
            "nama_ketua"=>"NJANA",
            "nama"=>"Karang Werda Bodronoyo (Tegalsari)",
            "alamat"=>"Jl. Kampung Malang Tengah I/9 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 132
        ]);

        User::create([
            "email" => "karwed133@dinsos.com",
            "nama_ketua"=>"ENDANG BUDIHARSI",
            "nama"=>"Karang Werda Janoko (Kedungdoro)",
            "alamat"=>"Jl. Plemahan VI No. 1-3 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 133
        ]);

        User::create([
            "email" => "karwed134@dinsos.com",
            "nama_ketua"=>"SOEGIARTO",
            "nama"=>"Karang Werda Whisnu (Dr. Soetomo)",
            "alamat"=>"Jl. Grudo (Rusunawa Grudo) Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 134
        ]);

        User::create([
            "email" => "karwed135@dinsos.com",
            "nama_ketua"=>"NIEK SUMARNI",
            "nama"=>"Karang Werda Bima Shakti (Wonorejo)",
            "alamat"=>"Jl.Wonorejo IV/48 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 135
        ]);

        User::create([
            "email" => "karwed136@dinsos.com",
            "nama_ketua"=>"HJ. TJETJEP SUPARDI",
            "nama"=>"Karang Werda Puspita Sari (Kutisari)",
            "alamat"=>"Jl. Kutisari Utara No. 69 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 136
        ]);

        User::create([
            "email" => "karwed137@dinsos.com",
            "nama_ketua"=>"LELONO ",
            "nama"=>"Karang Werda Panji Sapto Utomo (Panjang Jiwo)",
            "alamat"=>"Jl. Panjang Jiwo Permai II No. 64 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 137
        ]);

        User::create([
            "email" => "karwed138@dinsos.com",
            "nama_ketua"=>"ADI PURNOMO",
            "nama"=>"Karang Werda Yudistira (Tenggilis Mejoyo)",
            "alamat"=>"Jl. Tenggilis Mejoyo Bb/1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 138
        ]);

        User::create([
            "email" => "karwed139@dinsos.com",
            "nama_ketua"=>"SUMIATI ",
            "nama"=>"Karang Werda Temen Tinemu (Kendangsari)",
            "alamat"=>"Jl. Kendangsari IV No. 01 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 139
        ]);

        User::create([
            "email" => "karwed140@dinsos.com",
            "nama_ketua"=>"SUTANTO",
            "nama"=>"Karang Werda Bima (Balas Klumprik)",
            "alamat"=>"Jl. Balas Klumprik Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 140
        ]);

        User::create([
            "email" => "karwed141@dinsos.com",
            "nama_ketua"=>"B. SUNARDI",
            "nama"=>"Karang Werda Begawan Mintorogo (Jajar Tunggal)",
            "alamat"=>"Jl. Menganti Dukuh Kramat Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 141
        ]);

        User::create([
            "email" => "karwed142@dinsos.com",
            "nama_ketua"=>"M. SYAFII LUBIS",
            "nama"=>"Karang Werda Rajawali (Babatan)",
            "alamat"=>"Jl. Raya Menganti Babatan Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 142
        ]);

        User::create([
            "email" => "karwed143@dinsos.com",
            "nama_ketua"=>"SUPARMI S.",
            "nama"=>"Karang Werda Srikandi (Wiyung)",
            "alamat"=>"Wiyung RT. 03 RW. 01",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 143
        ]);

        User::create([
            "email" => "karwed144@dinsos.com",
            "nama_ketua"=>"MOCHAMMAD SLIMIN",
            "nama"=>"Karang Werda Sido Luhur (Sidosermo)",
            "alamat"=>"Jl. Sidosermo Pdk IV Kav .147 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 144
        ]);

        User::create([
            "email" => "karwed145@dinsos.com",
            "nama_ketua"=>"SUPANGKAT",
            "nama"=>"Karang Werda Sejahtera (Bendul Merisi)",
            "alamat"=>"Jl. Bendul Merisi Permai Blok L/23-A Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 145
        ]);

        User::create([
            "email" => "karwed146@dinsos.com",
            "nama_ketua"=>"EDDIE KUS",
            "nama"=>"Karang Werda Tua Bahagia (Margorejo)",
            "alamat"=>"Jl. Margorejo Masjid 32 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 146
        ]);

        User::create([
            "email" => "karwed147@dinsos.com",
            "nama_ketua"=>"SUPARDI ANANDHARMA",
            "nama"=>"Karang Werda Bagas Waras (Jemur Wonosari)",
            "alamat"=>"Jl. Jemursari VIII/49 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 147
        ]);

        User::create([
            "email" => "karwed148@dinsos.com",
            "nama_ketua"=>"SLAMET SOEMENDI",
            "nama"=>"Karang Werda Dewi Kunti (Siwalankerto)",
            "alamat"=>"Jl. Siwalankerto 132 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 148
        ]);

        User::create([
            "email" => "karwed149@dinsos.com",
            "nama_ketua"=>"HERIYANTO",
            "nama"=>"Karang Werda Yudhistira (Jagir)",
            "alamat"=>"Jl. Bedul Merisi No. 52 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 149
        ]);

        User::create([
            "email" => "karwed150@dinsos.com",
            "nama_ketua"=>"BAMBANG SUMPENO,MCH. ST",
            "nama"=>"Karang Werda Rekso Werdho (Wonokromo)",
            "alamat"=>"Jl. Pulo Wonokromo 253-B Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 150
        ]);

        User::create([
            "email" => "karwed151@dinsos.com",
            "nama_ketua"=>"SAMIADJI",
            "nama"=>"Karang Werda Wijaya Kusuma (Ngagel)",
            "alamat"=>"Jl. Ngagel No. 11 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 151
        ]);

        User::create([
            "email" => "karwed152@dinsos.com",
            "nama_ketua"=>"DRS. H. HARTANTRO",
            "nama"=>"Karang Werda Mulya Sejahtera (Ngagel Rejo)",
            "alamat"=>"Jl. Ngagel Tirtosari No. 1 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 152
        ]);

        User::create([
            "email" => "karwed153@dinsos.com",
            "nama_ketua"=>"TRESIA ADALENA ",
            "nama"=>"Karang Werda Adi Darma (Darmo)",
            "alamat"=>"Jl. Kampar No. 10 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 153
        ]);

        User::create([
            "email" => "karwed154@dinsos.com",
            "nama_ketua"=>"AGAAT WIHONO",
            "nama"=>"Karang Werda Sejahtera (Sawunggaling)",
            "alamat"=>"Jl. Wonoboyo No. 20 Surabaya",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154
        ]);

        User::create([
            "email"=>"ipsm1@dinsos.com",
            "nama"=>"ELLY SUSIANTI (TAMBAK SARIOSO)",
            "alamat"=>"JL. TAMBAKLANGON NO. 36 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+1
        ]);

        User::create([
            "email"=>"ipsm2@dinsos.com",
            "nama"=>"SUSETIAWATY (ASEMROWO)",
            "alamat"=>"JL. TAMBAK MAYOR III/5 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+2
        ]);

        User::create([
            "email"=>"ipsm3@dinsos.com",
            "nama"=>"SUPRIYATI (GENTING KALIANAK)",
            "alamat"=>"KALIANAK BARAT 57 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+3
        ]);

        User::create([
            "email"=>"ipsm4@dinsos.com",
            "nama"=>"FARIDAH HANUM (ROMOKALISARI)",
            "alamat"=>"ROMOKALISARI 14 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+4
        ]);

        User::create([
            "email"=>"ipsm5@dinsos.com",
            "nama"=>"ROMIYAH (TAMBAK OSOWILANGUN)",
            "alamat"=>"TAMBAK OSOWILANGUN 1/11 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+5
        ]);

        User::create([
            "email"=>"ipsm6@dinsos.com",
            "nama"=>"SULASTRI (KANDANGAN)",
            "alamat"=>"KLAKAH REJO 4 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+6
        ]);

        User::create([
            "email"=>"ipsm7@dinsos.com",
            "nama"=>"SUSILOWATI (SEMEMI)",
            "alamat"=>"SEMEMI JAYA 8/12 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+7
        ]);

        User::create([
            "email"=>"ipsm8@dinsos.com",
            "nama"=>"KOESNANDAR  (ALON-ALON CONTONG)",
            "alamat"=>"PRABAN WETAN 3/18 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+8
        ]);

        User::create([
            "email"=>"ipsm9@dinsos.com",
            "nama"=>"EKO ANDRIYANI (GUNDIH)",
            "alamat"=>"MARGORUKUN TENGAH NO. 22 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+9
        ]);

        User::create([
            "email"=>"ipsm10@dinsos.com",
            "nama"=>"DWI SULISTYO RINI (BUBUTAN)",
            "alamat"=>"MASPATI IV/103-105 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+10
        ]);

        User::create([
            "email"=>"ipsm11@dinsos.com",
            "nama"=>"HASTAYANI (TEMBOK DUKUH)",
            "alamat"=>"JL.ASEM BAGUS 3/20 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+11
        ]);

        User::create([
            "email"=>"ipsm12@dinsos.com",
            "nama"=>"MOCH. NASIR (JEPARA)",
            "alamat"=>"DUPAK MASIGIT 10/14 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+12
        ]);

        User::create([
            "email"=>"ipsm13@dinsos.com",
            "nama"=>"NANANG HUMAIDI (SUKOLILO BARU)",
            "alamat"=>"JL. SUKOLILO I-A/37 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+13
        ]);

        User::create([
            "email"=>"ipsm14@dinsos.com",
            "nama"=>"MUSRIANI (KENJERAN)",
            "alamat"=>"BULAK KENJERAN 3/10 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+14
        ]);

        User::create([
            "email"=>"ipsm15@dinsos.com",
            "nama"=>"DIANA YULISTIYA (KEDUNG COWEK)",
            "alamat"=>"KEDUNG COWEK I/12 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+15
        ]);

        User::create([
            "email"=>"ipsm16@dinsos.com",
            "nama"=>"SAMUJI (BULAK  )",
            "alamat"=>"BULAK CUMPAT TIMUR 5/3-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+16
        ]);

        User::create([
            "email"=>"ipsm17@dinsos.com",
            "nama"=>"AINUR ROFIK,SP (GUNUNGSARI)",
            "alamat"=>"MULYOREJO 116 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+17
        ]);

        User::create([
            "email"=>"ipsm18@dinsos.com",
            "nama"=>"SUDIRMAT (PRADAH KALI KENDAL)",
            "alamat"=>"PRADAH PERMAI VI/32 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+18
        ]);

        User::create([
            "email"=>"ipsm19@dinsos.com",
            "nama"=>"SITI FATIMAH A D (DUKUH PAKIS)",
            "alamat"=>"DUKUH PAKIS 6-A/36-C SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+19
        ]);

        User::create([
            "email"=>"ipsm20@dinsos.com",
            "nama"=>"HERRY HARTOYO PUTRO/H.ABD.WAHAB (DUKUH MENANGGAL)",
            "alamat"=>"JL. DUKUH MENANGGAL 12/17 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+20
        ]);

        User::create([
            "email"=>"ipsm21@dinsos.com",
            "nama"=>"SRIWAHYUNI  (GAYUNGAN)",
            "alamat"=>"JL. GAYUNGAN 3/6 B SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+21
        ]);

        User::create([
            "email"=>"ipsm22@dinsos.com",
            "nama"=>"SAKIYONO,SE (MENANGGAL)",
            "alamat"=>"MENANGGAL 2 GG. ANGGUR 11 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+22
        ]);

        User::create([
            "email"=>"ipsm23@dinsos.com",
            "nama"=>"SETYO RADJI (KETINTANG)",
            "alamat"=>"JL. KETINTANG BARU 11/12",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+23
        ]);

        User::create([
            "email"=>"ipsm24@dinsos.com",
            "nama"=>"YAYUK ISMOKORENI (EMBONG KALIASIN)",
            "alamat"=>"KEPUTRAN KEJAMBON 2/32 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+24
        ]);

        User::create([
            "email"=>"ipsm25@dinsos.com",
            "nama"=>"SUTAMI (GENTENG)",
            "alamat"=>"BLAURAN 4/15 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+25
        ]);

        User::create([
            "email"=>"ipsm26@dinsos.com",
            "nama"=>"WINDRIANTO  (PENELEH)",
            "alamat"=>"PENELEH 1/40",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+26
        ]);

        User::create([
            "email"=>"ipsm27@dinsos.com",
            "nama"=>"DRA. LILIS S (KETABANG)",
            "alamat"=>"JL. UNDAAN WETAN 4/24 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+27
        ]);

        User::create([
            "email"=>"ipsm28@dinsos.com",
            "nama"=>"SEKAR HARLIKANINGSIH (KAPASARI)",
            "alamat"=>"JL. KALIANYAR WETAN 3/1 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+28
        ]);

        User::create([
            "email"=>"ipsm29@dinsos.com",
            "nama"=>"ACHMAD NOVIANTO (PUCANG SEWU)",
            "alamat"=>"KALIBOKOR KENCANA 4/7 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+29
        ]);

        User::create([
            "email"=>"ipsm30@dinsos.com",
            "nama"=>"ENDANG SULISTYOWATI (MOJO)",
            "alamat"=>"KEDUNG TARUKAN WETAN 16 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+30
        ]);

        User::create([
            "email"=>"ipsm31@dinsos.com",
            "nama"=>"ENNY SRIASIH,SH (AIRLANGGA)",
            "alamat"=>"GUBENG KERTAJAYA 5-A/65 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+31
        ]);

        User::create([
            "email"=>"ipsm32@dinsos.com",
            "nama"=>"SUTRISNO (4)",
            "alamat"=>"JL. PUCANGAN 3/111 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+32
        ]);

        User::create([
            "email"=>"ipsm33@dinsos.com",
            "nama"=>"MOCH. UMBARI,S.E. (GUBENG)",
            "alamat"=>"JL. GUB. KLINGSINGAN V/16 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+33
        ]);

        User::create([
            "email"=>"ipsm34@dinsos.com",
            "nama"=>"SUPARNO (BARATAJAYA)",
            "alamat"=>"BARATAJAYA 2-A/53 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+34
        ]);

        User::create([
            "email"=>"ipsm35@dinsos.com",
            "nama"=>"ERNA YULIATY (GUNUNG ANYAR)",
            "alamat"=>"GUNUNGANYAR TENGAH 7/4 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+35
        ]);

        User::create([
            "email"=>"ipsm36@dinsos.com",
            "nama"=>"SUNARWAN,SE (RUNGKUT MENANGGAL)",
            "alamat"=>"RUNGKUT MENANGGAL 1-B/3 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+36
        ]);

        User::create([
            "email"=>"ipsm37@dinsos.com",
            "nama"=>"ELOK MASLACHA  (RUNGKUT TENGAH)",
            "alamat"=>"Jl.RAYA RUNGKUT TENGAH 54 B",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+37
        ]);

        User::create([
            "email"=>"ipsm38@dinsos.com",
            "nama"=>"SUYIKNO (GUNUNG ANYAR TAMBAK)",
            "alamat"=>"GUNUNG ANYAR TAMBAK I/54-B ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+38
        ]);

        User::create([
            "email"=>"ipsm39@dinsos.com",
            "nama"=>"SRI WAHYU BUDI HARTINI (PAGESANGAN)",
            "alamat"=>"JL. PAGESANGAN NO. 103 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+39
        ]);

        User::create([
            "email"=>"ipsm40@dinsos.com",
            "nama"=>"HENI PURWATI (JAMBANGAN)",
            "alamat"=>"JL. JAMBANGAN NO. 51 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+40
        ]);

        User::create([
            "email"=>"ipsm41@dinsos.com",
            "nama"=>"RR. ELLY SETIANI (KEBONSARI)",
            "alamat"=>"JL. KEBONSARI BARU NO. 2 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+41
        ]);

        User::create([
            "email"=>"ipsm42@dinsos.com",
            "nama"=>"RUSMIATI (KARAH)",
            "alamat"=>"JL. BIBIS KARAH 53-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+42
        ]);

        User::create([
            "email"=>"ipsm43@dinsos.com",
            "nama"=>"DJAMILAH (KARANG PILANG)",
            "alamat"=>"KARANG PILANG Gg. RAJAWALI 1 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+43
        ]);

        User::create([
            "email"=>"ipsm44@dinsos.com",
            "nama"=>"Dra. ENDAH POEDJI K (KEDURUS)",
            "alamat"=>"GUNUNGSARI INDAH BLOK MM/38 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+44
        ]);

        User::create([
            "email"=>"ipsm45@dinsos.com",
            "nama"=>"SOLI (KEBRAON)",
            "alamat"=>"KEBRAON 3 MUNDU 17-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+45
        ]);

        User::create([
            "email"=>"ipsm46@dinsos.com",
            "nama"=>"IMAM MAHMUDI,S.Sos. (WARUGUNUNG)",
            "alamat"=>"WARUGUNUNG RT.05 RW.03 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+46
        ]);

        User::create([
            "email"=>"ipsm47@dinsos.com",
            "nama"=>"ASFIYAH (TANAH KALI KEDINDING)",
            "alamat"=>"KEDINDING TENGAH JAYA 2/47 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+47
        ]);

        User::create([
            "email"=>"ipsm48@dinsos.com",
            "nama"=>"SITI MUKIMAH (BULAK BANTENG)",
            "alamat"=>"BULAK BANTENG LOR I/137 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+48
        ]);

        User::create([
            "email"=>"ipsm49@dinsos.com",
            "nama"=>"DIYAH RACHMAYANTI (TAMBAK WEDI)",
            "alamat"=>"TAMBAK WEDI BARU XVI/93 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+49
        ]);

        User::create([
            "email"=>"ipsm50@dinsos.com",
            "nama"=>"HARIYATI (SIDOTOPO WETAN)",
            "alamat"=>"BULAK BANTENG KIDUL IX NO. 2 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+50
        ]);

        User::create([
            "email"=>"ipsm51@dinsos.com",
            "nama"=>"TUKUL BINTORO (MOROKREMBANGAN)",
            "alamat"=>"TAMBAK ASRI 224 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+51
        ]);

        User::create([
            "email"=>"ipsm52@dinsos.com",
            "nama"=>"MUDJIATI (DUPAK)",
            "alamat"=>"LASEM 4/7 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+52
        ]);

        User::create([
            "email"=>"ipsm53@dinsos.com",
            "nama"=>"TEGUH TRI ADRIYANTO,SE (KEMAYORAN)",
            "alamat"=>"KREMBANGAN JAYA SELATAN 2-D/8 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+53
        ]);

        User::create([
            "email"=>"ipsm54@dinsos.com",
            "nama"=>"DRS. ROBERT ROMONDOR (PERAK BARAT)",
            "alamat"=>"IKAN KERAPU 2/2 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+54
        ]);

        User::create([
            "email"=>"ipsm55@dinsos.com",
            "nama"=>"ANDRIANSYAH (KREMBANGAN SELATAN)",
            "alamat"=>"KALONGAN 4/11 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+55
        ]);

        User::create([
            "email"=>"ipsm56@dinsos.com",
            "nama"=>"ETIK SURYANINGSIH (LAKARSANTRI)",
            "alamat"=>"LAKARSANTRI RT.001/RW.004 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+56
        ]);

        User::create([
            "email"=>"ipsm57@dinsos.com",
            "nama"=>"KARTIASIH (JERUK)",
            "alamat"=>"JERUK GG. BUNTU NO. 2-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+57
        ]);

        User::create([
            "email"=>"ipsm58@dinsos.com",
            "nama"=>"WASITAH (LIDAH KULON)",
            "alamat"=>"LIDAH KULON RT.003/RW.001 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+58
        ]);

        User::create([
            "email"=>"ipsm59@dinsos.com",
            "nama"=>"HENY TUPATWATI (LIDAH WETAN)",
            "alamat"=>"LIDAH WETAN GG. VIII-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+59
        ]);

        User::create([
            "email"=>"ipsm60@dinsos.com",
            "nama"=>"HEWAN KRISTINA (SUMUR WELUT)",
            "alamat"=>"PESAPEN RT.6 RW.2",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+60
        ]);

        User::create([
            "email"=>"ipsm61@dinsos.com",
            "nama"=>"SATUNI (BANGKINGAN)",
            "alamat"=>"BANGKINGAN RT.03/RW.03 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+61
        ]);

        User::create([
            "email"=>"ipsm62@dinsos.com",
            "nama"=>"MOCH. IRFAN  (KEJAWAN PUTIH TAMBAK)",
            "alamat"=>"KEJAWAN PUTIH TAMBAK 2/3 C",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+62
        ]);

        User::create([
            "email"=>"ipsm63@dinsos.com",
            "nama"=>"SITI AISYAH (MANYAR SABRANGAN)",
            "alamat"=>"JL. MANYAR SABRANGAN 3/68 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+63
        ]);

        User::create([
            "email"=>"ipsm64@dinsos.com",
            "nama"=>"NUR ANISA (DUKUH SUTOREJO)",
            "alamat"=>"JL. LABANSARI NO. 1 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+64
        ]);

        User::create([
            "email"=>"ipsm65@dinsos.com",
            "nama"=>"YULIANA (KALIJUDAN)",
            "alamat"=>"KALIJUDAN 10/55 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+65
        ]);

        User::create([
            "email"=>"ipsm66@dinsos.com",
            "nama"=>"MUHAMMAD SHOIM (KALISARI)",
            "alamat"=>"KALISARI DAMEN 92",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+66
        ]);

        User::create([
            "email"=>"ipsm67@dinsos.com",
            "nama"=>"MOCH. BANI,A.Md. (MULYOREJO)",
            "alamat"=>"MULYOREJO SELATAN 47 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+67
        ]);

        User::create([
            "email"=>"ipsm68@dinsos.com",
            "nama"=>"SRI ENDAH.W (NYAMPLUNGAN)",
            "alamat"=>"JL.BENTENG NO.17 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+68
        ]);

        User::create([
            "email"=>"ipsm69@dinsos.com",
            "nama"=>"HADI SANTOSO (KREMBANGAN UTARA)",
            "alamat"=>"PESAPEN 2/25 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+69
        ]);

        User::create([
            "email"=>"ipsm70@dinsos.com",
            "nama"=>"BAMBANG WIDJANARKO (PERAK UTARA)",
            "alamat"=>"TELUK KUMAI BARAT NO. 1 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+70
        ]);

        User::create([
            "email"=>"ipsm71@dinsos.com",
            "nama"=>"MUDJIANTO (PERAK TIMUR)",
            "alamat"=>"JL. JOHOR NO. 1-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+71
        ]);

        User::create([
            "email"=>"ipsm72@dinsos.com",
            "nama"=>"SUNDARI (BONGKARAN)",
            "alamat"=>"SEMUT 7/8 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+72
        ]);

        User::create([
            "email"=>"ipsm73@dinsos.com",
            "nama"=>"Drs. IDHAM KHALID Hz (SUMBEREJO)",
            "alamat"=>"SUMBEREJO MAKMUR 7/42 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+73
        ]);

        User::create([
            "email"=>"ipsm74@dinsos.com",
            "nama"=>"FIRMAN ASRORI (PAKAL)",
            "alamat"=>"PAKAL MADYA RT.03 RW.02 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+74
        ]);

        User::create([
            "email"=>"ipsm75@dinsos.com",
            "nama"=>"PARWADI PARLAN (BABAT JERAWAT)",
            "alamat"=>"DK. JERAWAT IX NO.29 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+75
        ]);

        User::create([
            "email"=>"ipsm76@dinsos.com",
            "nama"=>"WAWAN DARMAWAN  (BENOWO)",
            "alamat"=>"RAYA BENOWO NO.50 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+76
        ]);

        User::create([
            "email"=>"ipsm77@dinsos.com",
            "nama"=>"NOVI SUSILAWATI RAUF (MEDOKAN AYU)",
            "alamat"=>"MEDAYU UTARA 10/4 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+77
        ]);

        User::create([
            "email"=>"ipsm78@dinsos.com",
            "nama"=>"FATLAKAH (KALI RUNGKUT)",
            "alamat"=>"RUNGKUT LOR 7 MASJID 2-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+78
        ]);

        User::create([
            "email"=>"ipsm79@dinsos.com",
            "nama"=>"SYAMSIATI (PENJARINGAN SARI)",
            "alamat"=>"PANDUGO I/36-B SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+79
        ]);

        User::create([
            "email"=>"ipsm80@dinsos.com",
            "nama"=>"WINARSIH (RUNGKUT KIDUL)",
            "alamat"=>"RUNGKUT KIDUL 2/52 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+80
        ]);

        User::create([
            "email"=>"ipsm81@dinsos.com",
            "nama"=>"SUPARNO  (WONOREJO)",
            "alamat"=>"JL.WONOREJO NO.4",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+81
        ]);

        User::create([
            "email"=>"ipsm82@dinsos.com",
            "nama"=>"NUR FAUZAH (KEDUNG BARUK)",
            "alamat"=>"JL. RAYA KEDUNG ASEM NO. 59 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+82
        ]);

        User::create([
            "email"=>"ipsm83@dinsos.com",
            "nama"=>"SAMI RIANA (BRINGIN)",
            "alamat"=>"BRINGIN JAYA NO. 7-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+83
        ]);

        User::create([
            "email"=>"ipsm84@dinsos.com",
            "nama"=>"SUWARNO (LONTAR)",
            "alamat"=>"JL. RAYA LONTAR NO. 123-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+84
        ]);

        User::create([
            "email"=>"ipsm85@dinsos.com",
            "nama"=>"DASRI (MADE)",
            "alamat"=>"MADE AMD SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+85
        ]);

        User::create([
            "email"=>"ipsm86@dinsos.com",
            "nama"=>"SUTINAH  (SAMBIKEREP)",
            "alamat"=>"JL.RAYA SAMBIKEREP NO.1 RT.12 RW.4",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+86
        ]);

        User::create([
            "email"=>"ipsm87@dinsos.com",
            "nama"=>"MONICA SRI SUNARTI (PUTAT JAYA)",
            "alamat"=>"BANYU URIP WETAN 5/24-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+87
        ]);

        User::create([
            "email"=>"ipsm88@dinsos.com",
            "nama"=>"HJ.MARIYANI (BANYU URIP)",
            "alamat"=>"BANYU URIP WETAN 4-C/37 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+88
        ]);

        User::create([
            "email"=>"ipsm89@dinsos.com",
            "nama"=>"SUPRIJANTO (PAKIS)",
            "alamat"=>"PAKIS WETAN 4/25 A",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+89
        ]);

        User::create([
            "email"=>"ipsm90@dinsos.com",
            "nama"=>"HJ.ENDRARTINI (KUPANG KRAJAN)",
            "alamat"=>"BANYU URIP LOR 9/34",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+90
        ]);

        User::create([
            "email"=>"ipsm91@dinsos.com",
            "nama"=>"RUBIYANTO (PETEMON)",
            "alamat"=>"JL. PETEMON 3/19-B SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+91
        ]);

        User::create([
            "email"=>"ipsm92@dinsos.com",
            "nama"=>"FATHURROTIN,SH (SAWAHAN)",
            "alamat"=>"KEDUNGDORO 7/14 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+92
        ]);

        User::create([
            "email"=>"ipsm93@dinsos.com",
            "nama"=>"MOCH. SYAFI'IH (SIDOTOPO)",
            "alamat"=>"SIDOTOPO KIDUL 62 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+93
        ]);

        User::create([
            "email"=>"ipsm94@dinsos.com",
            "nama"=>"IDA SISWATI (PEGIRIAN)",
            "alamat"=>"WONOKUSUMO JAYA 1/2-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+94
        ]);

        User::create([
            "email"=>"ipsm95@dinsos.com",
            "nama"=>"SUKIDI (WONOKUSUMO)",
            "alamat"=>"WONOSARI WETAN BARU 8/11 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+95
        ]);

        User::create([
            "email"=>"ipsm96@dinsos.com",
            "nama"=>"SLAMET (UJUNG)",
            "alamat"=>"JATISRONO TENGAH 35 ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+96
        ]);

        User::create([
            "email"=>"ipsm97@dinsos.com",
            "nama"=>"FITRIAH (AMPEL)",
            "alamat"=>"PETUKANGAN TENGAH 49 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+97
        ]);

        User::create([
            "email"=>"ipsm98@dinsos.com",
            "nama"=>"RR. SRI RENY HIMAWATI (SIDODADI)",
            "alamat"=>"SIDODADI GG. X/35 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+98
        ]);

        User::create([
            "email"=>"ipsm99@dinsos.com",
            "nama"=>"MALIKI (SIMOLAWANG)",
            "alamat"=>"RUSUN SOMBO BLOK J/108 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+99
        ]);

        User::create([
            "email"=>"ipsm100@dinsos.com",
            "nama"=>"SITI MUAWANAH (KAPASAN)",
            "alamat"=>"GANG GEMBONG 2 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+100
        ]);

        User::create([
            "email"=>"ipsm101@dinsos.com",
            "nama"=>"HARDIMIN (TAMBAKREJO)",
            "alamat"=>"TAMBAK SEGARAN 5/21 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+101
        ]);

        User::create([
            "email"=>"ipsm102@dinsos.com",
            "nama"=>"SITI ROWIYAH  (SIMOKERTO)",
            "alamat"=>"KENJERAN 4A/1",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+102
        ]);

        User::create([
            "email"=>"ipsm103@dinsos.com",
            "nama"=>"GUNARSO (KLAMPIS NGASEM)",
            "alamat"=>"MLETO GG. 1/47 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+103
        ]);

        User::create([
            "email"=>"ipsm104@dinsos.com",
            "nama"=>"NINIK RIA K (KEPUTIH)",
            "alamat"=>"KEPUTIH TEGAL 4/36 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+104
        ]);

        User::create([
            "email"=>"ipsm105@dinsos.com",
            "nama"=>"SITI ANISIYAH DA (GEBANG PUTIH)",
            "alamat"=>"GEBANG WETAN 36 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+105
        ]);

        User::create([
            "email"=>"ipsm106@dinsos.com",
            "nama"=>"YULI SULISTININGSIH (MENUR PUMPUNGAN)",
            "alamat"=>"MENUR I/10 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+106
        ]);

        User::create([
            "email"=>"ipsm107@dinsos.com",
            "nama"=>"R.M HARIJONO (MEDOKAN SEMAMPIR)",
            "alamat"=>"MEDOKAN SEMAMPIR BLOK F/5 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+107
        ]);

        User::create([
            "email"=>"ipsm108@dinsos.com",
            "nama"=>"KUNJONO,SH (NGINDEN JANGKUNGAN)",
            "alamat"=>"JANGKUNGAN I-B NO. 19 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+108
        ]);

        User::create([
            "email"=>"ipsm109@dinsos.com",
            "nama"=>"DIAN ANDRIYANTO (SEMOLOWARU)",
            "alamat"=>"JL. SEMOLOWARU 137 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+109
        ]);

        User::create([
            "email"=>"ipsm110@dinsos.com",
            "nama"=>"H. KASTARI BA (SUKOMANUNGGAL)",
            "alamat"=>"JL. DONOWATI 2/12 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+110
        ]);

        User::create([
            "email"=>"ipsm111@dinsos.com",
            "nama"=>"SAMPUN PRAYITNO (SIMOMULYO BARU)",
            "alamat"=>"JL. TANJUNGSARI 6/15 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+111
        ]);

        User::create([
            "email"=>"ipsm112@dinsos.com",
            "nama"=>"JUMIATI (TANJUNGSARI)",
            "alamat"=>"JL. TANJUNGSARI 6/15 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+112
        ]);

        User::create([
            "email"=>"ipsm113@dinsos.com",
            "nama"=>"IKA NOVIANTI (SIMOMULYO  )",
            "alamat"=>"SIMOREJO 2/51",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+113
        ]);

        User::create([
            "email"=>"ipsm114@dinsos.com",
            "nama"=>"HERI SUPRIANTO (PUTAT GEDE)",
            "alamat"=>"PUTAT GEDE TIMUR 3/34",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+114
        ]);

        User::create([
            "email"=>"ipsm115@dinsos.com",
            "nama"=>"SENIMIN SUSENO HADI (SONOKWIJENAN)",
            "alamat"=>"JL.DARMO BARAT 33",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+115
        ]);

        User::create([
            "email"=>"ipsm116@dinsos.com",
            "nama"=>"GIARTI (KAPASMADYA BARU)",
            "alamat"=>"KAPAS JAYA NO. 25 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+116
        ]);

        User::create([
            "email"=>"ipsm117@dinsos.com",
            "nama"=>"ELYA KASTININGSIH (PACAR KELING)",
            "alamat"=>"TAPAKSIRING NO. 5-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+117
        ]);

        User::create([
            "email"=>"ipsm118@dinsos.com",
            "nama"=>"SETYONO (DUKUH SETRO)",
            "alamat"=>"LEBAK JAYA 5 UTARA RAWASAN KAV. A-21 NO. 63 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+118
        ]);

        User::create([
            "email"=>"ipsm119@dinsos.com",
            "nama"=>"SUNARINGATI  (PACAR KEMBANG)",
            "alamat"=>"PACAR KEMBANG TENGAH NO.1",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+119
        ]);

        User::create([
            "email"=>"ipsm120@dinsos.com",
            "nama"=>"EKO SENTANU (GADING)",
            "alamat"=>"GADING I/35-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+120
        ]);

        User::create([
            "email"=>"ipsm121@dinsos.com",
            "nama"=>"SOEDIBYO (TAMBAKSARI)",
            "alamat"=>"TAMBAKSARI SELATAN LEBAR NO. 10 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+121
        ]);

        User::create([
            "email"=>"ipsm122@dinsos.com",
            "nama"=>"MUSTIKA JULIANTI (PLOSO)",
            "alamat"=>"JL. PLOSO BOGEN NO. 43 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+122
        ]);

        User::create([
            "email"=>"ipsm123@dinsos.com",
            "nama"=>"NANANG SANTOSO (RANGKAH)",
            "alamat"=>"JL. TAMBAK SEGARAN WETAN 19-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+123
        ]);

        User::create([
            "email"=>"ipsm124@dinsos.com",
            "nama"=>"MIA SLAMET SUMEDI (MANUKAN WETAN)",
            "alamat"=>"JL. BIBIS TAMA 4/9 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+124
        ]);

        User::create([
            "email"=>"ipsm125@dinsos.com",
            "nama"=>"ROCHMATIN (BANJARSUGIHAN)",
            "alamat"=>"BANJARSUGIHAN 2/27 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+125
        ]);

        User::create([
            "email"=>"ipsm126@dinsos.com",
            "nama"=>"NUR FADHILAH (MANUKAN KULON)",
            "alamat"=>"MANUKAN KASMAN 30 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+126
        ]);

        User::create([
            "email"=>"ipsm127@dinsos.com",
            "nama"=>"SANGGATASARI S (BALONGSARI)",
            "alamat"=>"BALONGSARI PRAJA I/36 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+127
        ]);

        User::create([
            "email"=>"ipsm128@dinsos.com",
            "nama"=>"ROHASININGSIH (KARANGPOH)",
            "alamat"=>"KARANGPOH 4/8 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+128
        ]);

        User::create([
            "email"=>"ipsm129@dinsos.com",
            "nama"=>"ABDUL AZIS,S.Sos. (TANDES)",
            "alamat"=>"TANDES LOR 2/31 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+129
        ]);

        User::create([
            "email"=>"ipsm130@dinsos.com",
            "nama"=>"SITI AMINAH (DR. SUTOMO)",
            "alamat"=>"KUPANG PRAUPAN 2/18 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+130
        ]);

        User::create([
            "email"=>"ipsm131@dinsos.com",
            "nama"=>"BOEDI SANTOSO (KEDUNGDORO)",
            "alamat"=>"KEDUNG KLINTER 1/88-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+131
        ]);

        User::create([
            "email"=>"ipsm132@dinsos.com",
            "nama"=>"INDAH KUSUMAWATI (WONOREJO)",
            "alamat"=>"JL. WONOREJO IV/102 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+132
        ]);

        User::create([
            "email"=>"ipsm133@dinsos.com",
            "nama"=>"AKH. MUSTAIN EFFENDI (KEPUTRAN)",
            "alamat"=>"DINOYO SEKOLAHAN 1/1 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+133
        ]);

        User::create([
            "email"=>"ipsm134@dinsos.com",
            "nama"=>"NJANA (TEGALSARI)",
            "alamat"=>"PANDEGILING 2 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+134
        ]);

        User::create([
            "email"=>"ipsm135@dinsos.com",
            "nama"=>"DRA. HJ. HARTATIK (KENDANGSARI)",
            "alamat"=>"KENDANGSARI 4/2 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+135
        ]);

        User::create([
            "email"=>"ipsm136@dinsos.com",
            "nama"=>"IR. NANIK ULFAH (TENGGILIS MEJOYO)",
            "alamat"=>"TENGGILIS LAMA II/37 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+136
        ]);

        User::create([
            "email"=>"ipsm137@dinsos.com",
            "nama"=>"INDAH YERI PURWANTI (KUTISARI)",
            "alamat"=>"KUTISARI UTARA 2/2 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+137
        ]);

        User::create([
            "email"=>"ipsm138@dinsos.com",
            "nama"=>"RETNO WARIJANTI (PANJANG JIWO)",
            "alamat"=>"PANDUK V/7-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+138
        ]);

        User::create([
            "email"=>"ipsm139@dinsos.com",
            "nama"=>"SUHADI (BALAS KLUMPRIK)",
            "alamat"=>"KLUMPRIK GG. MAKAM NO. 42 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+139
        ]);

        User::create([
            "email"=>"ipsm140@dinsos.com",
            "nama"=>"AGUS NURHADI (WIYUNG)",
            "alamat"=>"WIYUNG VI/83 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+140
        ]);

        User::create([
            "email"=>"ipsm141@dinsos.com",
            "nama"=>"SRI MUDJI RAHAYU  (JAJAR TUNGGAL)",
            "alamat"=>"DK.GEMOL 1D RT.04 RW.03",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+141
        ]);

        User::create([
            "email"=>"ipsm142@dinsos.com",
            "nama"=>"BINTI ALFIYAH  (BABATAN)",
            "alamat"=>"DK.KARANGAN V/7 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+142
        ]);

        User::create([
            "email"=>"ipsm143@dinsos.com",
            "nama"=>"PURWANTI (SIWALANKERTO)",
            "alamat"=>"SIWALANKERTO UTARA NO. 33 D SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+143
        ]);

        User::create([
            "email"=>"ipsm144@dinsos.com",
            "nama"=>"RIBUT SUSANTI (BENDUL MERISI)",
            "alamat"=>"BENDUL MERISI GG. I UTARA 2 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+144
        ]);

        User::create([
            "email"=>"ipsm145@dinsos.com",
            "nama"=>"NOVIATI RAHAYU  (MARGOREJO)",
            "alamat"=>"JETIS WETAN 2/7 A",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+145
        ]);

        User::create([
            "email"=>"ipsm146@dinsos.com",
            "nama"=>"LULUK ALFICHRIA (SIDOSERMO)",
            "alamat"=>"JL. SIDOSERMO 1/12 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+146
        ]);

        User::create([
            "email"=>"ipsm147@dinsos.com",
            "nama"=>"NURUN AULIYAH (JEMUR WONOSARI)",
            "alamat"=>"JEMURWONOSARI GG. MASJID NO. 32-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+147
        ]);

        User::create([
            "email"=>"ipsm148@dinsos.com",
            "nama"=>"MUDJIATI (DARMO)",
            "alamat"=>"DARMOREJO 5/6",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+148
        ]);

        User::create([
            "email"=>"ipsm149@dinsos.com",
            "nama"=>"SRI SUHARTINI (NGAGEL)",
            "alamat"=>"NGAGEL BARU 2/97-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+149
        ]);

        User::create([
            "email"=>"ipsm150@dinsos.com",
            "nama"=>"RAHAJUNINGSIH (SAWUNGGALING)",
            "alamat"=>"GUNUNGSARI 1 TREM 29-A SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+150
        ]);

        User::create([
            "email"=>"ipsm151@dinsos.com",
            "nama"=>"DRA. RACHMI TRIWAHYUNI (NGAGEL REJO)",
            "alamat"=>"IKAN KERAPU 2/2 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+151
        ]);

        User::create([
            "email"=>"ipsm152@dinsos.com",
            "nama"=>"WIWIK SUYANTIK (WONOKROMO)",
            "alamat"=>"KETINTANG 22 A",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+152
        ]);

        User::create([
            "email"=>"ipsm153@dinsos.com",
            "nama"=>"DRA. MUSATUN (JAGIR)",
            "alamat"=>"GEMBILI 2/33 SURABAYA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153
        ]);

        User::create([
            "email" => "panti1@dinsos.com",
            "nama"=>"Yayasan An Nur(Asemrowo)",
            "nama_ketua"=>"MUDJIATI,BA.",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+1
        ]);

        User::create([
            "email" => "panti2@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiyah Bulak(Kenjeran )",
            "nama_ketua"=>"Drs. H. FAUZAN,M.Pd.i",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+2
        ]);

        User::create([
            "email" => "panti3@dinsos.com",
            "nama"=>"Yayasan Al Hidayah Mabrur Jawa Timur(Tembok Dukuh)",
            "nama_ketua"=>"INDAH FATHONAH,S.HI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+3
        ]);

        User::create([
            "email" => "panti4@dinsos.com",
            "nama"=>"Panti Asuhan Muslimat NU(Gundih)",
            "nama_ketua"=>"Hj. TIMUNAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+4
        ]);

        User::create([
            "email" => "panti5@dinsos.com",
            "nama"=>"Yayasan Baitul Amal(Tembok Dukuh)",
            "nama_ketua"=>"SUWOKO MAWIDHI,SE",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+5
        ]);

        User::create([
            "email" => "panti6@dinsos.com",
            "nama"=>"Panti Asuhan Al - Hikmah(Jepara)",
            "nama_ketua"=>"H. IMAM RAHMAN",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+6
        ]);

        User::create([
            "email" => "panti7@dinsos.com",
            "nama"=>"Panti Asuhan Ibnu Hajar(Sememi)",
            "nama_ketua"=>"Drs. MOCH. HADZIK",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+7
        ]);

        User::create([
            "email" => "panti8@dinsos.com",
            "nama"=>"Panti Asuhan Islamadina(Kandangan)",
            "nama_ketua"=>"S U P R A P T O",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+8
        ]);

        User::create([
            "email" => "panti9@dinsos.com",
            "nama"=>"Panti Asuhan Uswah(Sememi)",
            "nama_ketua"=>"MOHAMMAD IMAM DRAJAD",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+9
        ]);

        User::create([
            "email" => "panti10@dinsos.com",
            "nama"=>"Panti Asuhan LPAY(Dukuh Pakis)",
            "nama_ketua"=>"Umi Masruroh",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+10
        ]);

        User::create([
            "email" => "panti11@dinsos.com",
            "nama"=>"Yayasan Himmatun Ayat Dukuh Kupang(Dukuh Kupang)",
            "nama_ketua"=>"NUR FADILAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+11
        ]);

        User::create([
            "email" => "panti12@dinsos.com",
            "nama"=>"Panti Asuhan As Sa'adah(Peneleh)",
            "nama_ketua"=>"F A R I D A H",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+12
        ]);

        User::create([
            "email" => "panti13@dinsos.com",
            "nama"=>"Panti Asuhan Ruqoiyah(Peneleh)",
            "nama_ketua"=>"Hj. SITI IRAWATI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+13
        ]);

        User::create([
            "email" => "panti14@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiyah Grogol(Peneleh)",
            "nama_ketua"=>"ACHMAD FANANI DAHLAN",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+14
        ]);

        User::create([
            "email" => "panti15@dinsos.com",
            "nama"=>"Panti Asuhan Aisyah I(Baratajaya)",
            "nama_ketua"=>"Hj. UMI SALAMAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+15
        ]);

        User::create([
            "email" => "panti16@dinsos.com",
            "nama"=>"Yayasan Ibnu Sina(Kertajaya)",
            "nama_ketua"=>"NUNUNG INDRAYANI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+16
        ]);

        User::create([
            "email" => "panti17@dinsos.com",
            "nama"=>"Yayasan Bani Ya'qub(Menanggal)",
            "nama_ketua"=>"M. ALY MIMBAR,BA,S.Kom",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+17
        ]);

        User::create([
            "email" => "panti18@dinsos.com",
            "nama"=>"Panti Asuhan Al Muttaqin(Gayungan)",
            "nama_ketua"=>"H. SUKARDJO,TS.",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+18
        ]);

        User::create([
            "email" => "panti19@dinsos.com",
            "nama"=>"Panti Asuhan Al Fatimah(Gunung Anyar Tambak)",
            "nama_ketua"=>"H. BADRUL ALEK AFIFI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+19
        ]);

        User::create([
            "email" => "panti20@dinsos.com",
            "nama"=>"Panti Asuhan Wachid Hasyim(Rungkut Menanggal)",
            "nama_ketua"=>"Hj. SITI FATIMAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+20
        ]);

        User::create([
            "email" => "panti21@dinsos.com",
            "nama"=>"Panti Asuhan Aisyah II(Kebonsari)",
            "nama_ketua"=>"Dr.HJ. SITI RUCHANAH,M.AG",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+21
        ]);

        User::create([
            "email" => "panti22@dinsos.com",
            "nama"=>"Panti Asuhan An Nisa Bani Hawa(Kebonsari)",
            "nama_ketua"=>"KHOIRUL ANAM",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+22
        ]);

        User::create([
            "email" => "panti23@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiyah KH. Achmad Dahlan(Morokrembangan)",
            "nama_ketua"=>"MUHAMMAD TUAN ABBAS",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+23
        ]);

        User::create([
            "email" => "panti24@dinsos.com",
            "nama"=>"Panti Asuhan Putera  Immanuel(Krembangan Selatan)",
            "nama_ketua"=>"NI WAYAN SENENG",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+24
        ]);

        User::create([
            "email" => "panti25@dinsos.com",
            "nama"=>"Panti Asuhan Santa Yulia(Krembangan Selatan)",
            "nama_ketua"=>"Sr. M .RENATA NI MADE HERDIYANTI,SPM",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+25
        ]);

        User::create([
            "email" => "panti26@dinsos.com",
            "nama"=>"Panti Asuhan Yayasan Sedekah Membawa Berkah(Kemayoran)",
            "nama_ketua"=>"NIA TUL FITRI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+26
        ]);

        User::create([
            "email" => "panti27@dinsos.com",
            "nama"=>"Yayasan As Salam(Dupak)",
            "nama_ketua"=>"SUYITNO,M.Ag",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+27
        ]);

        User::create([
            "email" => "panti28@dinsos.com",
            "nama"=>"Yayasan Dharma Putra Swadaya(Bulak Banteng)",
            "nama_ketua"=>"SOEKIJANI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+28
        ]);

        User::create([
            "email" => "panti29@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiyah Kenjeran(Tambak Wedi)",
            "nama_ketua"=>"S U P R I A D I",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+29
        ]);

        User::create([
            "email" => "panti30@dinsos.com",
            "nama"=>"Panti Asuhan Kamang Immanuel(Bulak Banteng)",
            "nama_ketua"=>"S O E G I J O",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+30
        ]);

        User::create([
            "email" => "panti31@dinsos.com",
            "nama"=>"Panti Asuhan Rodhiyatul Jannah(Tambak Wedi)",
            "nama_ketua"=>"ACHMAD NUR FAUZI,S.Sos",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+31
        ]);

        User::create([
            "email" => "panti32@dinsos.com",
            "nama"=>"Yayasan Sohibul Yatim(Tanah Kali Kedinding)",
             "nama_ketua"=>"FADELI ",
             "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+32
        ]);
        User::create([
            "email" => "panti33@dinsos.com",
            "nama"=>"Panti Asuhan Ar Rohmah Wal Barokah(Tanah Kali Kedinding)",
            "nama_ketua"=>"AGUS SETIYO WIBOWO,ST",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+33
        ]);

        User::create([
            "email" => "panti34@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiyah Karangpilang(Karang Pilang)",
            "nama_ketua"=>"ZAINI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+34
        ]);

        User::create([
            "email" => "panti35@dinsos.com",
            "nama"=>"Panti Asuhan Assalam GSI(Kredurus)",
            "nama_ketua"=>"ACHMAD FUAD",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+35
        ]);

        User::create([
            "email" => "panti36@dinsos.com",
            "nama"=>"Panti Asuhan Al Mu'min(Lidah Kulon)",
            "nama_ketua"=>"HR. ABDUS SHOMAD SURYANTO",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+36
        ]);

        User::create([
            "email" => "panti37@dinsos.com",
            "nama"=>"Panti Asuhan Jawiyah Badrie(Lidah Kulon)",
            "nama_ketua"=>"Hj. NUR SEHAN",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+37
        ]);

        User::create([
            "email" => "panti38@dinsos.com",
            "nama"=>"Panti Asuhan Ashabul Kahfi(Manyar Sabrangan)",
            "nama_ketua"=>"Ir. BUCHRAN MUTHAHER",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+38
        ]);

        User::create([
            "email" => "panti39@dinsos.com",
            "nama"=>"Yayasan Peduli Umat Al Uswah(Perak Utara)",
            "nama_ketua"=>"FUAD BASWEDAN,MPD",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+39
        ]);

        User::create([
            "email" => "panti40@dinsos.com",
            "nama"=>"Yayasan Nur Ihsan(Babat Jerawat)",
            "nama_ketua"=>"Hj. YAYUK HERU NADIFAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+40
        ]);

        User::create([
            "email" => "panti41@dinsos.com",
            "nama"=>"Panti Asuhan Thoriqul Jannah(Benowo)",
            "nama_ketua"=>"H. IMAM HAMBALI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+41
        ]);

        User::create([
            "email" => "panti42@dinsos.com",
            "nama"=>"Panti Asuhan Syifaul Qulub(Benowo)",
            "nama_ketua"=>"SUKISNO,S.PdI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+42
        ]);

        User::create([
            "email" => "panti43@dinsos.com",
            "nama"=>"Panti Asuhan Assalafiyah(Kedung Baruk)",
            "nama_ketua"=>"Drs. H. MOCH. LUTFI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+43
        ]);

        User::create([
            "email" => "panti44@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiya Medokan Ayu(Medokan Ayu)",
            "nama_ketua"=>"S U P R A P T O",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+44
        ]);

        User::create([
            "email" => "panti45@dinsos.com",
            "nama"=>"Panti Asuhan Syirotol Mustakim(Medokan Ayu)",
            "nama_ketua"=>"MOCHAMAD ISMUL MUCHLIS",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+45
        ]);

        User::create([
            "email" => "panti46@dinsos.com",
            "nama"=>"Panti Asuhan Al Ashar(Rungkut Kidul)",
            "nama_ketua"=>"MACHRUS ISTICHSAN",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+46
        ]);

        User::create([
            "email" => "panti47@dinsos.com",
            "nama"=>"Panti Asuhan Al Jadiid(Sambikerep)",
            "nama_ketua"=>"Dra. Hj. DEWI NUR SHOLIHAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+47
        ]);

        User::create([
            "email" => "panti48@dinsos.com",
            "nama"=>"Yayasan Ar Riyadloh(Lontar)",
            "nama_ketua"=>"H. NUR WAHIB,SH,MM.",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+48
        ]);

        User::create([
            "email" => "panti49@dinsos.com",
            "nama"=>"Panti Asuhan Ar Rosyid(Lontar)",
            "nama_ketua"=>"H. KHOZIN RIDUWAN",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+49
        ]);

        User::create([
            "email" => "panti50@dinsos.com",
            "nama"=>"Panti Asuhan Khadijah 3(Lontar)",
            "nama_ketua"=>"MASLICHAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+50
        ]);

        User::create([
            "email" => "panti51@dinsos.com",
            "nama"=>"Yayasan An Nahdhiyah(Lontar)",
            "nama_ketua"=>"ACHMAD ICHROM",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+51
        ]);

        User::create([
            "email" => "panti52@dinsos.com",
            "nama"=>"Panti Asuhan Al Hidayah(Wonokusumo)",
            "nama_ketua"=>"Drs. H. YATIMUN,MM",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+52
        ]);

        User::create([
            "email" => "panti53@dinsos.com",
            "nama"=>"Panti Asuhan Miftahul Ulum(Simolawang)",
            "nama_ketua"=>"FAHRUR ROZI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+53
        ]);

        User::create([
            "email" => "panti54@dinsos.com",
            "nama"=>"Panti Asuhan Al Ichlash(Tanjungsari)",
            "nama_ketua"=>"NUR RAHMAWATI,S.Pd",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+54
        ]);

        User::create([
            "email" => "panti55@dinsos.com",
            "nama"=>"Panti Asuhan Rahman Rahim(Simomulyo Baru)",
            "nama_ketua"=>"Drs. ACHMAD ANANDJURI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+55
        ]);

        User::create([
            "email" => "panti56@dinsos.com",
            "nama"=>"Panti Asuhan Al Hidayah(Sukomanuggal)",
            "nama_ketua"=>"K A S T A R I",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+56
        ]);

        User::create([
            "email" => "panti57@dinsos.com",
            "nama"=>"Yayasan Nurul Fikri(Simomulyo Baru)",
            "nama_ketua"=>"Diyana Frida Yanti,S.Si",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+57
        ]);

        User::create([
            "email" => "panti58@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiyah Pakis(Pakis)",
            "nama_ketua"=>"Ir. H. MUHAMMAD ABIDULLOH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+58
        ]);

        User::create([
            "email" => "panti59@dinsos.com",
            "nama"=>"Yayasan Al Muniroh(Sawahan)",
            "nama_ketua"=>"Hj. SITI ROCHMAH THOYIB",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+59
        ]);

        User::create([
            "email" => "panti60@dinsos.com",
            "nama"=>"Panti Asuhan Yatim Piatu Karimah(Pakis)",
            "nama_ketua"=>"MOH. YUSUF,SH.",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+60
        ]);

        User::create([
            "email" => "panti61@dinsos.com",
            "nama"=>"Yayasan Bina Umat(Banyu Urip)",
            "nama_ketua"=>"NOER MUHAMMAD",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+61
        ]);

        User::create([
            "email" => "panti62@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiyah Putat Jaya(Putat Jaya)",
            "nama_ketua"=>"SOFYAN HADI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+62
        ]);

        User::create([
            "email" => "panti63@dinsos.com",
            "nama"=>"Yayasan Nurani Mandiri (Sawahan)",
            "nama_ketua"=>"SURYATININGSIH,S.Psi.",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+63
        ]);

        User::create([
            "email" => "panti64@dinsos.com",
            "nama"=>"Panti Asuhan Al Hidayah(Nginden Jangkungan)",
            "nama_ketua"=>"NURUL MUZAKKIYAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+64
        ]);

        User::create([
            "email" => "panti65@dinsos.com",
            "nama"=>"Panti Asuhan Arif Rahman Hakim(Klampis Ngasem)",
            "nama_ketua"=>"ABD. SAMI'",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+65
        ]);

        User::create([
            "email" => "panti66@dinsos.com",
            "nama"=>"Panti Asuhan Al Kahfi(Sukolilo)",
            "nama_ketua"=>"Hj. SOEWASIH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+66
        ]);

        User::create([
            "email" => "panti68@dinsos.com",
            "nama"=>"Panti Asuhan Yayasan Lentera Yatim Indonesia(Semolowaru)",
            "nama_ketua"=>"Dian Kristiniawati",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+68
        ]);

        User::create([
            "email" => "panti67@dinsos.com",
            "nama"=>"Panti Asuhan BJ Habibie(Keputih)",
            "nama_ketua"=>"WAHIDIL MAHFUDZ",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+67
        ]);

        User::create([
            "email" => "panti69@dinsos.com",
            "nama"=>"Yayasan Tunas Yatim(Pacar Kembang)",
            "nama_ketua"=>"MOCHAMMAD SUPI'I",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+69
        ]);

        User::create([
            "email" => "panti70@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiyah Tambaksari(Ploso)",
            "nama_ketua"=>"ABU HASAN",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+70
        ]);

        User::create([
            "email" => "panti71@dinsos.com",
            "nama"=>"Panti Asuhan LKSA Dr. Soetomo(Airlangga)",
            "nama_ketua"=>"KUSANDRINI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+71
        ]);

        User::create([
            "email" => "panti72@dinsos.com",
            "nama"=>"Panti Asuhan Noor Musholla(Ploso)",
            "nama_ketua"=>"HJ. HARYANI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+72
        ]);

        User::create([
            "email" => "panti73@dinsos.com",
            "nama"=>"Yayasan Firdaus Insan(Manukan Kulon)",
            "nama_ketua"=>"NURUL WULANSARI,S.Ag",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+73
        ]);

        User::create([
            "email" => "panti74@dinsos.com",
            "nama"=>"Panti Asuhan Muhammadiyah Tandes(Manukan Kulon)",
            "nama_ketua"=>"JUSUF ISMAIL ADAM",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+74
        ]);

        User::create([
            "email" => "panti75@dinsos.com",
            "nama"=>"Panti Asuhan At Thohiroh(Manukan Kulon)",
            "nama_ketua"=>"MUDJIASIH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+75
        ]);

        User::create([
            "email" => "panti76@dinsos.com",
            "nama"=>"Panti Asuhan Muslimat NU(Banjar Sugihan)",
            "nama_ketua"=>"SITI MUALLIFAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+76
        ]);

        User::create([
            "email" => "panti77@dinsos.com",
            "nama"=>"Panti Asuhan ISLAM HIDAYATULLOH(Manukan Wetan)",
            "nama_ketua"=>"LILIK ERFAWATI,M.Pd.I",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+77
        ]);

        User::create([
            "email" => "panti78@dinsos.com",
            "nama"=>"Panti Asuhan Al Fattah(Balongsari)",
            "nama_ketua"=>"MUTI’AH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+78
        ]);

        User::create([
            "email" => "panti79@dinsos.com",
            "nama"=>"Panti Asuhan Al Istiqomah(Kendangsari)",
            "nama_ketua"=>"Dra .HJ. HARTATIK",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+79
        ]);

        User::create([
            "email" => "panti80@dinsos.com",
            "nama"=>"Panti Asuhan Ulul Albab(Tenggilis Mejoyo)",
            "nama_ketua"=>"LALU MUSTALAM",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+80
        ]);

        User::create([
            "email" => "panti81@dinsos.com",
            "nama"=>"Yayasan Permata Insani(Tenggilis Mejoyo)",
            "nama_ketua"=>"USMAN MAARIF",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+81
        ]);

        User::create([
            "email" => "panti82@dinsos.com",
            "nama"=>"Yayasan Lentera Hati Al Hidayah(Kedungdoro)",
            "nama_ketua"=>"Drs. MOH. ROHIMIN",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+82
        ]);

        User::create([
            "email" => "panti83@dinsos.com",
            "nama"=>"Yayasan Himmatun Ayat Kedung Rukem(Kedungdoro)",
            "nama_ketua"=>"ZAINAL ABIDIN",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+83
        ]);

        User::create([
            "email" => "panti84@dinsos.com",
            "nama"=>"Yayasan Bina Cita(Keputran)",
            "nama_ketua"=>"H. ISKANDAR PUTRA",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+84
        ]);

        User::create([
            "email" => "panti85@dinsos.com",
            "nama"=>"Panti Asuhan Tumpuhan Harapan(Babatan)",
            "nama_ketua"=>"S U P A R D I",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+85
        ]);

        User::create([
            "email" => "panti86@dinsos.com",
            "nama"=>"Panti Asuhan Az Zahara(Wiyung)",
            "nama_ketua"=>"M. JA'FAR SHODIQ SH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+86
        ]);

        User::create([
            "email" => "panti87@dinsos.com",
            "nama"=>"Yayasan Darul Aitam(Wiyung)",
            "nama_ketua"=>"PARINGONO",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+87
        ]);

        User::create([
            "email" => "panti88@dinsos.com",
            "nama"=>"Panti Asuhan Al Achmar(Ngagel Rejo)",
            "nama_ketua"=>"REKHA PADMA SHAKTI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+88
        ]);

        User::create([
            "email" => "panti89@dinsos.com",
            "nama"=>"Yayasan Kartini(Wonokromo)",
            "nama_ketua"=>"ENDANG TRIKARDININGSIH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+89
        ]);

        User::create([
            "email" => "panti90@dinsos.com",
            "nama"=>"Panti Asuhan Darul Ilmi(Wonokromo)",
            "nama_ketua"=>"ISLAMIYAH,S.Pd",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+90
        ]);

        User::create([
            "email" => "panti91@dinsos.com",
            "nama"=>"Yayasan Al- Ikhlas(Wonokromo)",
            "nama_ketua"=>"M. FARID ZMY",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+91
        ]);

        User::create([
            "email" => "panti92@dinsos.com",
            "nama"=>"Panti Asuhan Nurul Islam(Jagir)",
            "nama_ketua"=>"WAHYUDIN ABDUL LATIF",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+92
        ]);

        User::create([
            "email" => "panti93@dinsos.com",
            "nama"=>"Panti Asuhan Al Rosyidu Shoburih(Wonokromo)",
            "nama_ketua"=>"ACHMAD GHOZALI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+93
        ]);

        User::create([
            "email" => "panti94@dinsos.com",
            "nama"=>"Yayasan Musliman(Siwalan Kerto)",
            "nama_ketua"=>"H. AGUS KUSMANI",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+94
        ]);

        User::create([
            "email" => "panti95@dinsos.com",
            "nama"=>"Panti Asuhan Nurul Hikmah(Sidosermo)",
            "nama_ketua"=>"M. RIZKI AMINULLAH",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+95
        ]);

        User::create([
            "email" => "panti96@dinsos.com",
            "nama"=>"Yayasan Mitra Arofah(Jemur Wonosari)",
            "nama_ketua"=>"Drs. SUWADJI,MM.",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+96
        ]);

        User::create([
            "email" => "panti97@dinsos.com",
            "nama"=>"Panti Asuhan Rodhiyatul Banat(Margorejo)",
            "nama_ketua"=>"MAS SUMARNO ARIEF",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+97
        ]);

        User::create([
            "email" => "panti98@dinsos.com",
            "nama"=>"Panti Asuhan Ar Rochim(Bendul Merisi)",
            "nama_ketua"=>"NUR IKHYAK",
            "password" => bcrypt('kucinglucu'),
            "role_id" => ROLE_PENYEDIA,
            "penyedia_id" => 154+153+98
        ]);

    }
}
