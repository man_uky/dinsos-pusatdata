<?php

use Illuminate\Database\Seeder;
use App\KuotaKecamatan;
class KuotaKecamatansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=18;$i++)
        {
            for($j=1;$j<=32;$j++){
                KuotaKecamatan::create([
                    'intervensi_id'=>$i,
                    'kecamatan_id'=>$j
                ]);
            }
        }
    }
}
