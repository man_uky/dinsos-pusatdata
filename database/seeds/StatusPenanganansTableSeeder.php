<?php

use Illuminate\Database\Seeder;
use App\StatusPenanganan;
class StatusPenanganansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusPenanganan::create([
        	"nama" => "BELUM DIVERIFIKASI"
        ]);
        StatusPenanganan::create([
        	"nama" => "BELUM DITANGANI"
        ]);
        StatusPenanganan::create([
        	"nama" => "AKTIF"
        ]);
        StatusPenanganan::create([
        	"nama" => "TIDAK AKTIF"
        ]);
    }
}
