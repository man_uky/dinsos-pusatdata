<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePergantianPmksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pergantian_pmks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik1');
            $table->string('nama1');
            $table->string('alamat1');
            $table->string('nipm1');
            $table->string('nik2')->nullable();
            $table->string('nama2')->nullable();
            $table->string('alamat2')->nullable();
            $table->string('nipm2')->nullable();
            $table->date('tanggal_ba');
            $table->date('tanggal_berhenti');
            $table->date('tanggal_menerima');
            $table->string('keterangan');
            $table->integer('kelurahan_id')->nullable();
            $table->integer('kecamatan_id')->nullable();
            $table->integer('intervensi_id');
            $table->integer('bidang_id');
            $table->integer('petugas_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pergantian_pmks');
    }
}
