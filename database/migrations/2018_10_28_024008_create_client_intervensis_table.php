<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientIntervensisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_intervensis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status_penanganan')->default('BELUM DITANGANI');
            $table->string('jenis_penanganan')->nullable();
            $table->string('jenis_rehabilitasi')->nullable();
            $table->string('status_rehabilitasi')->nullable(); //Belum Berhasil,Berhasil,Proses
            $table->string('deskripsi_rehabilitasi')->nullable();
            $table->string('status_pengentasan')->nullable();
            $table->string('psks_pembantu')->nullable();
            $table->string('perkembangan_terbaru')->nullable(); //Meninggal,Pindah
            $table->string('keterangan')->nullable();
            $table->integer('client_id');
            $table->integer('intervensi_id')->nullable();
            $table->integer('penyedia_id')->nullable();
            $table->integer('bidang_id')->nullable();
            $table->integer('is_active')->default(0);
            $table->integer('nipm')->nullable();
            $table->date('tanggal_mulai')->nullable();
            $table->date('tanggal_berhenti')->nullable();
            $table->string('no_berita_acara')->nullable();
            $table->unique( array('nipm','intervensi_id') );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_intervensis');
    }
}
