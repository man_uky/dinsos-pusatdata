<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormKhususesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_khususes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nilai')->nullable();
            $table->integer("pmks_id");
            $table->integer("client_id");
            $table->integer("pmks_form_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_khususes');
    }
}
