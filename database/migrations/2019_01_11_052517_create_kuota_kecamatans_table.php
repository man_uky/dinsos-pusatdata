<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKuotaKecamatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuota_kecamatans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kuota')->default(250);
            $table->integer('terisi')->default(0);
            $table->integer('kecamatan_id');
            $table->integer('intervensi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kuota_kecamatans');
    }
}
