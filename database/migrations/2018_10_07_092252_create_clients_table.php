<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik',25)->unique()->nullable();
            $table->string('nama')->nullable();
            $table->string('no_kk',25)->nullable();
            $table->string('alamat_kk')->nullable();
            $table->string('alamat')->nullable();
            $table->string('no_telp')->nullable();
            $table->integer('tahun')->nullable();
            $table->string('asal')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('jenis_kelamin')->default("LAKI-LAKI");
            $table->string('status_perkawinan')->default("BELUM NIKAH"); //BELUM NIKAH/NIKAH/CERAI HIDUP/CERAI MATI
            $table->string('pendidikan_tertinggi')->default("SD"); //SD/SMP/SMA/D3/S1
            $table->string('pekerjaan')->nullable();
            $table->integer('pendapatan')->nullable();
            #Perumahan
            $table->string('status_kepemilikan')->default("MILIK SENDIRI"); //Milik Sendiri/Kos/Kontrak/Bebas Sewa/Dinas/Lainya(input)

            $table->integer('luas_lantai')->nullable();
            $table->string('jenis_lantai')->default("Tanah"); //Marmer/Keramik/Vinil/Ubin/Kayu/Semen/Bambu/Tanah/Lainya
            $table->string('jenis_dinding')->default("Tembok"); //Tembok/Anyaman Bambu/Kayu/Lainya
            $table->string('jenis_atap')->default("Beton"); //Beton/Genteng Keramik/Genteng Metal/Genteng Tanah Liat/Asbes/Seng/Sirap/Bambu/Jerami/Lainya
            $table->integer('jumlah_kamar_tidur')->nullable();
            $table->string('sumber_air_minum')->default("Air Kemasan"); //Air Kemasan/Air Isi Ulang/Ledeng Meteran/Ledeng Eceran/Sumur/Mata Air(Sungai,Danau,Waduk)
            $table->string('memperoleh_air_minum')->default("Membeli Eceran"); //Membeli Eceran/Langganan/Tidak Membeli
            $table->string('sumber_penerangan')->default("PLN"); //PLN/Non PLN
            $table->string('daya_terpasang')->default("450"); //450/900/1300/2200/>2200/Tanpa Meteran
            $table->string('bahan_bakar_memasak')->default("Minyak Tanah"); //Listrik/Gas>3kg/Gas 3kg/Biogas/Minyak Tanah/Briket/Arang/Kayu Bakar/Tidak Memasak di rumah
            $table->string('tempat_bab')->default("Umum"); //Sendiri/Bersama/Umum/Tidak ada
            $table->string('pembuangan_bab')->default("SPAL"); //Tangki/SPAL/Lubang Tanah/Sawah,Kolam,Sungai,Danau,Laut/Pantai,Kebun,Tanah Lapang/Lainya

            $table->integer('biaya_sampah')->nullable();
            $table->integer('biaya_sewa')->nullable();
            $table->integer('biaya_air')->nullable();
            $table->integer('biaya_listrik')->nullable();
            $table->integer('biaya_gas')->nullable();
            $table->string('telp_rumah')->nullable();

            //aset
            $table->integer('tabung_gas')->default(0); //Ya dan Tidak
            $table->integer('lemari_es')->default(0); //Ya dan Tidak
            $table->integer('ac')->default(0); //Ya dan Tidak
            $table->integer('pemanas_air')->default(0); //Ya dan Tidak
            $table->integer('telepon')->default(0); //Ya dan Tidak
            $table->integer('tv')->default(0); //Ya dan Tidak
            $table->integer('emas')->default(0); //Ya dan Tidak
            $table->integer('komputer')->default(0); //Ya dan Tidak
            $table->integer('sepeda')->default(0); //Ya dan Tidak
            $table->integer('motor')->default(0); //Ya dan Tidak
            $table->integer('mobil')->default(0); //Ya dan Tidak
            $table->integer('perahu')->default(0); //Ya dan Tidak
            $table->integer('motor_tempel')->default(0); //Ya dan Tidak
            $table->integer('perahu_motor')->default(0); //Ya dan Tidak
            $table->integer('kapal')->default(0); //Ya dan Tidak
            $table->string('intervensi_pemerintah')->default("Tidak Ada"); //KKS/KPS/KIP/BPJS/RASKIN/MITRAWARGA/PKH/DLL
            #keikutsertaan
            $table->integer('kks')->default(0); //Ya dan Tidak
            $table->integer('kip')->default(0); //Ya dan Tidak
            $table->integer('bpjs_kesehatan_mandiri')->default(0); //Ya dan Tidak
            $table->integer('bpjs_kesehatan_pbi')->default(0); //Ya dan Tidak
            $table->integer('bpjs_ketenagakerjaan_mandiri')->default(0); //Ya dan Tidak
            $table->integer('asuransi_kesehatan_lain')->default(0); //Ya dan Tidak
            $table->integer('pkh')->default(0); //Ya dan Tidak
            $table->integer('raskin')->default(0); //Ya dan Tidak
            $table->integer('kur')->default(0); //Ya dan Tidak

            $table->date('tanggal_usulan')->nullable();
            $table->text('harapan')->nullable();
            $table->text('permasalahan')->nullable();

            $table->string('status_pmks')->default("AKTIF"); //AKTIF/MENINGGAL/PINDAH/MAMPU

            $table->integer('pmks_id')->nullable();
            $table->integer('bidang_id')->nullable();
            $table->integer('intervensi_id')->nullable();
            $table->integer('kecamatan_id')->nullable();
            $table->integer('kelurahan_id')->nullable();
            $table->integer('rt')->nullable();
            $table->integer('rw')->nullable();
            $table->integer('photo_id')->nullable();
            $table->integer('status_penanganan_id')->nullable();
            $table->integer('petugas_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
