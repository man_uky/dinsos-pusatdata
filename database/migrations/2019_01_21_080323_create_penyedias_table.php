<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenyediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penyedias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kuota')->default(100);
            $table->integer('terisi')->default(0);
            $table->integer('harga')->default(11000);
            $table->integer('tipe'); //1,2,3
            $table->integer('bidang_id');
            $table->integer('intervensi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penyedias');
    }
}
