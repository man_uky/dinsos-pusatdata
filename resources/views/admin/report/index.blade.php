@extends('layouts.main')
@section('title') Report @endsection
@section('navbar-title') Report @endsection
@section('navbar-link') /admin/report @endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>
                                Laporan
                                <small>Download Report</small>
                            </h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <select name="bulan">
                                @foreach(BULAN as $k => $b)
                                    <option value="{{$k+1}}" {{$month==$k+1 ? 'selected' : ''}}>{{$b}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="tahun">
                                @foreach(TAHUN as $k => $t)
                                    <option value="{{$t}}" {{$year==$t ? 'selected' : ''}}>{{$t}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                @if(Auth::user()->penyedia->tipe==1)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span >
                            <a style="text-align: center;" href="javascript:void(0);" onclick="getReport('/admin/download/kwitansi'); return false;">
                                <div class="body bg-red">
                                    KWITANSI
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="javascript:void(0);" onclick="getReport('/admin/download/pencairan'); return false;">
                                <div class="body bg-cyan">
                                    PERMOHONAN PENCAIRAN
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="javascript:void(0);" onclick="getReport('/admin/download/penerima'); return false;">
                                <div class="body bg-green">
                                    LAMPIRAN PENERIMA MANFAAT
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="javascript:void(0);" onclick="getReport('/admin/download/mutlak'); return false;">
                                <div class="body bg-orange">
                                    MUTLAK
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="javascript:void(0);" onclick="getReport('/admin/download/kesanggupan'); return false;">
                                <div class="body bg-blue-grey">
                                    KESANGGUPAN
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="javascript:void(0);" onclick="getReport('/admin/download/rekapitulasi'); return false;">
                                <div class="body bg-light-blue">
                                    REKAPITULASI
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                @endif
                @if(Auth::user()->penyedia->tipe==3)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="/admin/download/beritakirimpanti">
                                <div class="body bg-amber">
                                   Berita Kirim Panti
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="/admin/download/spjpanti">
                                <div class="body bg-pink">
                                    SPJ Panti
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="/admin/download/rekapspjpanti">
                                <div class="body bg-light-green">
                                    Rekapitulasi SPJ Panti
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                @endif
                @if(Auth::user()->penyedia->tipe==2)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span >
                            <a style="text-align: center;" href="/admin/download/kwitansiipsm">
                                <div class="body bg-red">
                                    Kwitansi IPSM
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="/admin/download/rekappaca">
                                <div class="body bg-cyan">
                                    Rekapitulasi PACA
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <span>
                            <a style="text-align: center;"  href="/admin/download/spjipsm">
                                <div class="body bg-green">
                                    SPJ IPSM
                                </div>
                            </a>
                        </span>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('js')
<script>
    function getReport(link){
        var bulan = $("select[name='bulan']").val()
        var tahun = $("select[name='tahun']").val()
        window.open(link+'?bulan='+bulan+'&tahun='+tahun, '_blank');
    }
</script>
@endsection
