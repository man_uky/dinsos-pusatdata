@extends('layouts.main')
@section('title') Data Client | {{$client->nama}} @endsection
@section('navbar-title') Data Client @endsection
@section('navbar-link') /admin/clients @endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="header">
                    <h2>
                        <div class="row">
                            <div class="col-md-5">
                                {{$client->nama}}
                                <small>{{$client->pmks->nama}}</small>
                            </div>
                            <div class="col-md-7">
                                <a href="{{strpos(URL::previous(),"?page")? URL::previous() : '/admin/clients'}}">
                                    <button type="button" class="btn bg-blue btn-circle-lg waves-effect waves-circle waves-float pull-right">
                                        <i class="material-icons">arrow_back</i>
                                    </button>
                                </a>
                            </div>
                        </div>
                        
                    </h2>
                </div>

                <div class="body row">
                    <div class="col-md-6">
                        <a class="thumbnail text-center">
                            @if($client->photo_id)
                            <img src="/photo/{{$client->photo_id}}" class="img-responsive">
                            @else
                            <img src="/images/Ciri-Balita-Tumbuh-Sehat.png" class="img-responsive">
                            @endif
                        </a>
                        <form>
                            <label>NIK</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->nik}}
                                </div>
                            </div>
                            <label>KK</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->no_kk}}
                                </div>
                            </div>
                            <label>Nama</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->nama}}
                                </div>
                            </div>
                            <label>Alamat Sesuai KK</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->alamat_kk}}
                                </div>
                            </div>
                            <label>Daerah Asal</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->asal}}
                                </div>
                            </div>
                            
                            <label>Kelurahan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->kelurahan->nama}}
                                </div>
                            </div>
                            <label>Kecamatan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->kecamatan->nama}}
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <label>Jenis PMKS</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->pmks ? $client->pmks->nama : ''}}
                                </div>
                            </div>
                            <label>Intervensi</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->intervensi ? $client->intervensi->nama : ''}}
                                </div>
                            </div>
                            <label>Bidang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->bidang ? $client->bidang->nama : ''}}
                                </div>
                            </div>
                            <label>Tahun Teridentifikasi</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->tahun}}
                                </div>
                            </div>
                            <label>Status Penanganan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    {{$client->status_penanganan ? $client->status_penanganan->nama : ''}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection