@extends('layouts.main')
@section('title') Data Client @endsection
@section('navbar-title') Data Client @endsection
@section('navbar-link') /admin/clients @endsection
@php
    $query = Request::query();
@endphp
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>
                                Data Client
                                <small>Kelola data client</small>
                            </h2>
                        </div>
                        @if(Auth::user()->role_id==ROLE_ADMIN || $query['status_penanganan']==1)
                        <div class="col-md-6">
                            <a href="/admin/clients/create?status_penanganan={{$query['status_penanganan']}}">
                                <button type="button" class="btn bg-green btn-circle-lg waves-effect waves-circle waves-float pull-right">
                                <i class="material-icons">add</i>
                            </button>
                            </a>
                        </div>
                        @endif
                    </div>

                </div>
                <div class="body">
                    <form action="/admin/clients" method="GET">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NIK</th>
                                    <th>Nama</th>
                                    <th>Intervensi</th>
                                    <th>Jenis PMKS</th>
                                    <th>Kelurahan</th>
                                    <th>Kecamatan</th>
                                    <th class="col-xs-2">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                                    <tr>
                                        <th><input type="hidden" name="status_penanganan" value="{{$query['status_penanganan']}}"></th>
                                        <th>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input class="form-control" type="text" name="nik" value="{{$nik}}">
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input class="form-control" type="text" name="nama" value="{{$nama}}">
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <select class="form-control show-tick" name="intervensi_id" data-live-search="true" data-size=5>
                                                    <option value="">-- Silahkan Pilih --</option>
                                                    @foreach($intervensis as $intervensi)
                                                        <option value="{{$intervensi->id}}">{{$intervensi->nama}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <select class="form-control show-tick" name="pmks_id" data-live-search="true" data-size=5>
                                                    <option value="">-- Silahkan Pilih --</option>
                                                    @foreach($pmks as $p)
                                                        <option value="{{$p->id}}">{{$p->nama}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <select class="form-control show-tick" name="kelurahan_id" data-live-search="true" data-size=5>
                                                    <option value="">-- Silahkan Pilih --</option>
                                                    @foreach($kelurahans as $kelurahan)
                                                        <option value="{{$kelurahan->id}}">{{$kelurahan->nama}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <select class="form-control show-tick" name="kecamatan_id" data-live-search="true" data-size=5>
                                                    <option value="">-- Silahkan Pilih --</option>
                                                    @foreach($kecamatans as $kecamatan)
                                                        <option value="{{$kecamatan->id}}">{{$kecamatan->nama}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </th>
                                        <th>
                                            <button type="submit" class="btn bg-green waves-effect" data-dismiss="modal">Cari</button>
                                        </th>
                                    </tr>


                                @foreach($clients as $key =>$client)
                                <tr>
                                    <th scope="row">{{$loop->index+$clients->firstItem()}}</th>
                                    <td>{{$client->nik}}</td>
                                    <td>{{$client->nama}}</td>
                                    <td>
                                        @if($client->client_intervensi->last() && $client->client_intervensi->last()->intervensi)
                                        {{$client->client_intervensi->last()->intervensi->nama}}
                                        @endif
                                    </td>
                                    <td>{{$client->pmks->nama}}</td>
                                    <td>{{$client->kelurahan->nama}}</td>
                                    <td>{{$client->kecamatan->nama}}</td>
                                    <td>
                                        @if($client->status_penanganan_id==STATUS_PENANGANAN_BELUM_DITANGANI || $client->status_penanganan_id==STATUS_PENANGANAN_AKTIF)
                                        <a href="/admin/clients/{{$client->id}}?status_penanganan={{$query['status_penanganan']}}">
                                            <button type="button" class="btn bg-blue waves-effect">
                                            <i class="material-icons">info</i>
                                        </button></a>
                                        <a href="/admin/clients/{{$client->id}}/intervensi?status_penanganan={{$query['status_penanganan']}}">
                                            <button type="button" class="btn bg-green waves-effect">
                                            <i class="material-icons">local_hospital</i>
                                        </button>
                                        </a>
                                        @elseif($user->role_id==ROLE_ADMIN && $client->status_penanganan_id==STATUS_PENANGANAN_BELUM_DIVERIFIKASI)
                                        <a href="/admin/clients/{{$client->id}}/activate">
                                            <button type="button" class="btn bg-green waves-effect">
                                            <i class="material-icons">check</i>
                                        </button>
                                        </a>
                                        @elseif($client->status_penanganan_id==STATUS_PENANGANAN_BELUM_DIVERIFIKASI)
                                        Data Belum Diverifikasi Admin
                                        @endif

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    </form>
                    <div class="row">
                        <div class="hidden-md hidden-lg">
                            <div class="col-sm-12 text-center">
                                Menampilkan <b>{{$clients->count()+($clients->currentPage()-1)*$clients->perPage()}} dari {{$clients->total()}}</b> data
                            </div>
                            <div class="col-sm-12 text-center pagination">
                                {{ $clients->appends($_GET)->links('vendor.pagination.custom') }}
                            </div>
                        </div>
                        <div class="hidden-sm hidden-xs">
                            <div class="col-sm-4">
                                Menampilkan <b>{{$clients->count()+($clients->currentPage()-1)*$clients->perPage()}} dari {{$clients->total()}}</b> data
                            </div>
                            <div class="col-sm-8 text-center pagination">
                                {{ $clients->appends($_GET)->links('vendor.pagination.custom') }}
                            </div>
                        </div>
				    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script>
    @if (session('create_success'))
        swal("Membuat data berhasil  ", "Data berhasil disimpan", "success");
    @endif
    @if (session('activate_success'))
        swal("Verifikasi Berhasil", "Data Telah Diverifikasi", "success");
    @endif
    $("select[name='intervensi_id']").val({{$intervensi_id}})
    $("select[name='kelurahan_id']").val({{$kelurahan_id}})
    $("select[name='kecamatan_id']").val({{$kecamatan_id}})
    $('select').selectpicker('refresh')
</script>
@endsection
