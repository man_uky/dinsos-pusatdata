@extends('layouts.main')
@section('title') Data Client @endsection
@section('navbar-title') Data Client @endsection
@section('navbar-link') /admin/clients @endsection
@section('css')
  <style type="text/css">
    .avatar-upload {
      position: relative;
      max-width: 100%;
    }
    .avatar-upload .avatar-edit {
      position: absolute;
      z-index: 1;
    }
    .avatar-upload .avatar-edit input {
      display: none;
    }
    .avatar-upload .avatar-edit input + label {
      display: inline-block;
      width: 34px;
      height: 34px;
      margin-bottom: 0;
      border-radius: 0%;
      background: #FFFFFF;
      border: 1px solid transparent;
      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
      cursor: pointer;
      font-weight: normal;
      transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
      background: #f1f1f1;
      border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
      content: "\f044";
      font-family: 'Font Awesome 5 Free';
      color: #757575;
      position: absolute;
      top: 10px;
      left: 0;
      right: 0;
      text-align: center;
      margin: auto;
    }
    .avatar-upload .avatar-preview {
      width: 100%;
      height: 400px;
      position: relative;
      border-radius: 0%;
      border: 6px solid #F8F8F8;
      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
      width: 100%;
      height: 100%;
      border-radius: 0%;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
    }
  </style>
@endsection
@php
    $query = Request::query();
@endphp
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>
                                Tambah Data Klien
                                <small>Usulan data klien baru</small>
                            </h2>
                        </div>
                        <div class="col-md-6">
                            <a href="/admin/clients/create">
                                <a href="{{strpos(URL::previous(),"?page")? URL::previous() : '/admin/clients'}}">
                                    <button type="button" class="btn bg-blue btn-circle-lg waves-effect waves-circle waves-float pull-right">
                                        <i class="material-icons">arrow_back</i>
                                    </button>
                                </a>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="body">
                    <form id="wizard_vertical" enctype="multipart/form-data" method="POST" action="/admin/clients?status_penanganan={{$query['status_penanganan']}}">
                         @csrf
                        <h2>Biodata</h2>
                        <section>
                            <label>NIK</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control  nik" type="text" name="nik" maxlength="16" minlength="6" required>
                                </div>
                                <div class="help-info">Panjang NIK 16 digit</div>
                            </div>
                            <div class="form-group form-float">
                              <label class="form-label">Foto</label>
                              <div class="avatar-upload">
                                  <div class="avatar-edit">
                                      <input name="photo" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                      <label for="imageUpload"></label>
                                  </div>
                                  <div class="avatar-preview">
                                      <div id="imagePreview" style="">
                                      </div>
                                  </div>
                              </div>
                            </div>
                            <label>Nama</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="nama" required>
                                </div>
                            </div>
                            <label>KK</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control nik" type="text" name="no_kk" required>
                                </div>
                                <div class="help-info">Panjang KK 16 digit</div>
                            </div>

                            <label>Alamat Sesuai KK</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="alamat_kk" required>
                                </div>
                            </div>
                            <label>Daerah Asal</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="asal"  required>
                                </div>
                                <div class="help-info">Ex: Surabaya</div>
                            </div>


                            <label>Jenis Kelamin</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="jenis_kelamin" data-live-search="true" data-size=5>
                                    <option value="LAKI-LAKI">LAKI-LAKI</option>
                                    <option value="PEREMPUAN">PEREMPUAN</option>
                                </select>
                            </div>
                            <label>Status Perkawinan</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="status_perkawinan" data-live-search="true" data-size=5>
                                        <option value="BELUM-NIKAH">BELUM-NIKAH</option>
                                        <option value="NIKAH">NIKAH</option>
                                        <option value="CERAI-HIDUP">CERAI-HIDUP</option>
                                        <option value="CERAI-MATI">CERAI-MATI</option>

                                </select>
                            </div>
                            <label>Pendidikan Tertinggi</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="pendidikan_tertinggi" data-live-search="true" data-size=5>
                                        <option value="SD">SD</option>
                                        <option value="SMP">SMP</option>
                                        <option value="SMA">SMA</option>
                                        <option value="D3">D3</option>
                                        <option value="S1">S1</option>
                                </select>
                            </div>
                            <label>Kecamatan</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="kecamatan_id" data-live-search="true" data-size=5>
                                    <option value="">-- Silahkan Pilih --</option>
                                    @foreach($kecamatans as $kecamatan)
                                        <option value="{{$kecamatan->id}}">{{$kecamatan->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label>Kelurahan</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="kelurahan_id" data-live-search="true" data-size=5>
                                    <option value="">-- Silahkan Pilih --</option>
                                    @foreach($kelurahans as $kelurahan)
                                        <option value="{{$kelurahan->id}}">{{$kelurahan->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </section>

                        <h2>Perekonomian</h2>
                        <section>
                            <label>Pekerjaan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="pekerjaan">
                                </div>
                            </div>
                            <label>Pendapatan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control money-rupiah" type="text" name="pendapatan">
                                </div>
                            </div>
                            <label>Status Tempat Tinggal</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="status_kepemilikan" data-live-search="true" data-size=5>
                                        <option value="MILIK-SENDIRI">MILIK SENDIRI</option>
                                        <option value="KOS">KOS</option>
                                        <option value="KONTRAK">KONTRAK</option>
                                        <option value="DINAS">DINAS</option>
                                        <option value="LAIN-LAIN">LAIN-LAIN</option>
                                </select>
                            </div>
                            <label>Luas Lantai</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="number" name="luas_lantai">
                                </div>
                            </div>
                            <label>Jenis Lantai</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="jenis_lantai" data-live-search="true" data-size=5>
                                        <option value="Tanah">Tanah</option>
                                        <option value="Marmer">Marmer</option>
                                        <option value="Keramik">Keramik</option>
                                        <option value="Vinil">Vinil</option>
                                        <option value="Ubin">Ubin</option>
                                        <option value="Kayu">Kayu</option>
                                        <option value="Vinil">Vinil</option>
                                        <option value="Semen">Semen</option>
                                        <option value="Bambu">Bambu</option>
                                        <option value="Lainya">Lainya</option>
                                </select>
                            </div>
                            <label>Jenis Dinding</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="jenis_dinding" data-live-search="true" data-size=5>
                                        <option value="Tembok">Tembok</option>
                                        <option value="Anyaman Bambu">Anyaman Bambu</option>
                                        <option value="Kayu">Kayu</option>
                                        <option value="Lainya">Lainya</option>
                                </select>
                            </div>
                            <label>Jenis Atap</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="jenis_atap" data-live-search="true" data-size=5>
                                        <option value="Beton">Beton</option>
                                        <option value="Genteng Keramik">Genteng Keramik</option>
                                        <option value="Genteng Metal">Genteng Metal</option>
                                        <option value="Genteng Tanah Liat">Genteng Tanah Liat</option>
                                        <option value="Asbes">Asbes</option>
                                        <option value="Seng">Seng</option>
                                        <option value="Sirap">Sirap</option>
                                        <option value="Bambu">Bambu</option>
                                        <option value="Jerami">Jerami</option>
                                        <option value="Lainya">Lainya</option>
                                </select>
                            </div>
                            <label>Jumlah Kamar Tidur</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="number" name="jumlah_kamar_tidur">
                                </div>
                            </div>
                            <label>Sumber Air Minum</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="sumber_air_minum" data-live-search="true" data-size=5>
                                        <option value="Air Kemasan">Air Kemasan</option>
                                        <option value="Air Isi Ulang">Air Isi Ulang</option>
                                        <option value="Ledeng Meteran">Ledeng Meteran</option>
                                        <option value="Ledeng Eceran">Ledeng Eceran</option>
                                        <option value="Sumur">Sumur</option>
                                        <option value="Mata Air(Sungai,Danau,Waduk)">Mata Air(Sungai,Danau,Waduk)</option>
                                </select>
                            </div>
                            <label>Cara Memperoleh Air Minum</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="memperoleh_air_minum" data-live-search="true" data-size=5>
                                        <option value="Membeli Eceran">Membeli Eceran</option>
                                        <option value="Langganan">Langganan</option>
                                        <option value="Tidak Membeli">Tidak Membeli</option>
                                </select>
                            </div>
                            <label>Sumber Penerangan</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="sumber_penerangan" data-live-search="true" data-size=5>
                                        <option value="PLN">PLN</option>
                                        <option value="Non PLN">Non PLN</option>
                                </select>
                            </div>
                            <label>Daya Terpasang</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="daya_terpasang" data-live-search="true" data-size=5>
                                        <option value="450">450</option>
                                        <option value="900">900</option>
                                        <option value="1300">1300</option>
                                        <option value="2200">2200</option>
                                        <option value=">2200">>2200</option>
                                        <option value="Tanpa Meteran">Tanpa Meteran</option>
                                </select>
                            </div>
                            <label>Bahan Bakar Untuk Memasak</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="bahan_bakar_memasak" data-live-search="true" data-size=5>
                                        <option value="Listrik">Listrik</option>
                                        <option value="Gas>3kg">Gas>3kg</option>
                                        <option value="Gas 3kg">Gas 3kg</option>
                                        <option value="Biogas">Biogas</option>
                                        <option value="Minyak Tanah">Minyak Tanah</option>
                                        <option value="Briket">Briket</option>
                                        <option value="Arang">Arang</option>
                                        <option value="Kayu Bakar">Kayu Bakar</option>
                                        <option value="Tidak Memasak di rumah">Tidak Memasak di rumah</option>
                                </select>
                            </div>
                            <label>Tempat BAB</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="tempat_bab" data-live-search="true" data-size=5>
                                        <option value="Sendiri">Sendiri</option>
                                        <option value="Bersama">Bersama</option>
                                        <option value="Umum">Umum</option>
                                        <option value="Tidak ada">Tidak ada</option>
                                </select>
                            </div>
                            <label>Pembuangan BAB</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="pembuangan_bab" data-live-search="true" data-size=5>
                                        <option value="Tangki">Tangki</option>
                                        <option value="SPAL">SPAL</option>
                                        <option value="Lubang Tanah">Lubang Tanah</option>
                                        <option value="Sawah,Kolam,Sungai,Danau,Laut">Sawah,Kolam,Sungai,Danau,Laut</option>
                                        <option value="Pantai,Kebun,Tanah Lapang">Pantai,Kebun,Tanah Lapang</option>
                                        <option value="Lainya">Lainya</option>
                                </select>
                            </div>
                            <label>Biaya Sampah</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control money-rupiah" type="text" name="biaya_sampah">
                                </div>
                            </div>
                            <label>Biaya sewa bulanan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control money-rupiah" type="text" name="biaya_sewa">
                                </div>
                            </div>
                            <label>Biaya Listrik</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control money-rupiah" type="text" name="biaya_listrik" >
                                </div>
                            </div>
                            <label>Biaya Air</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control money-rupiah" type="text" name="biaya_air" >
                                </div>
                            </div>
                            <label>Biaya Gas/Makan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control money-rupiah" type="text" name="biaya_gas" >
                                </div>
                            </div>
                            <label>No Telepon Rumah/HP</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control phone" type="text" name="telp_rumah" >
                                </div>
                            </div>
                        </section>
                        <h2>Aset dan Keikutsertaan</h2>
                        <section>
                            <label>Kepemilikan Aset</label>
                            <div class="demo-checkbox">
                                <input id="id_tabung_gas" name="tabung_gas" value="1" type="checkbox" class="filled-in" />
                                <label for="id_tabung_gas">Tabung Gas</label>
                                <input id="id_lemari_es" name="lemari_es" value="1" type="checkbox" class="filled-in" />
                                <label for="id_lemari_es">Lemari Es</label>
                                <input id="id_ac" name="ac" value="1" type="checkbox" class="filled-in" />
                                <label for="id_ac">AC</label>
                                <input id="id_pemanas_air" name="pemanas_air" value="1" type="checkbox" class="filled-in" />
                                <label for="id_pemanas_air">Pemanas Air</label>
                                <input id="id_telepon" name="telepon" value="1" type="checkbox" class="filled-in" />
                                <label for="id_telepon">Telepon</label>
                                <input id="id_tv" name="tv" value="1" type="checkbox" class="filled-in" />
                                <label for="id_tv">TV</label>
                                <input id="id_emas" name="emas" value="1" type="checkbox" class="filled-in" />
                                <label for="id_emas">Emas</label>
                                <input id="id_komputer" name="komputer" value="1" type="checkbox" class="filled-in" />
                                <label for="id_komputer">Komputer/Laptop</label>
                                <input id="id_sepeda" name="sepeda" value="1" type="checkbox" class="filled-in"/>
                                <label for="id_sepeda">Sepeda</label>
                                <input id="id_motor" name="motor" value="1" type="checkbox" class="filled-in" />
                                <label for="id_motor">Sepeda Motor</label>
                                <input id="id_mobil" name="mobil" value="1" type="checkbox" class="filled-in" />
                                <label for="id_mobil">Mobil</label>
                                <input id="id_perahu" name="perahu" value="1" type="checkbox" class="filled-in" />
                                <label for="id_perahu">Perahu</label>
                                <input id="id_motor_tempel" name="motor_tempel" value="1" type="checkbox" class="filled-in" />
                                <label for="id_motor_tempel">Motor Tempel</label>
                                <input id="id_perahu_motor" name="perahu_motor" value="1" type="checkbox" class="filled-in" />
                                <label for="id_perahu_motor">Perahu Motor</label>
                                <input id="id_kapal" name="kapal" value="1" type="checkbox" class="filled-in" />
                                <label for="id_kapal">Kapal</label>
                            </div>

                            <br>
                            <label>Keikutsertaan Program Pemerintah</label>
                            <div class="demo-checkbox">
                              <input id="id_kks" name="kks" value="1" type="checkbox" class="filled-in" />
                              <label for="id_kks">KKS (Kartu Keluarga Sejahtera)</label>
                              <input id="id_kip" name="kip" value="1" type="checkbox" class="filled-in" />
                              <label for="id_kip">KIP (Kartu Indonesia Pintar)</label>
                              <input id="id_bpjs_kesehatan_mandiri" name="bpjs_kesehatan_mandiri" value="1" type="checkbox" class="filled-in" />
                              <label for="id_bpjs_kesehatan_mandiri">BPJS Kesehatan (Mandiri)</label>
                              <input id="id_bpjs_kesehatan_mandiri" name="bpjs_kesehatan_pbi" value="1" type="checkbox" class="filled-in" />
                              <label for="id_bpjs_kesehatan_pbi">BPJS (Penerima Bantuan Iuran)</label>
                              <input id="id_bpjs_ketenagakerjaan_mandiri" name="bpjs_ketenagakerjaan_mandiri" value="1" type="checkbox" class="filled-in" />
                              <label for="id_bpjs_ketenagakerjaan_mandiri">BPJS Ketenagakerjaan (Mandiri)</label>
                              <input id="id_asuransi_kesehatan_lain" name="asuransi_kesehatan_lain" value="1" type="checkbox" class="filled-in" />
                              <label for="id_asuransi_kesehatan_lain">Asuransi Kesehatan Lain</label>
                              <input id="id_pkh" name="pkh" value="1" type="checkbox" class="filled-in" />
                              <label for="id_pkh">PKH</label>
                              <input id="id_raskin" name="raskin" value="1" type="checkbox" class="filled-in" />
                              <label for="id_raskin">Raskin</label>
                              <input id="id_kur" name="kur" value="1" type="checkbox" class="filled-in" />
                              <label for="id_kur">KUR</label>
                            </div>

                        </section>
                        <h2>Permasalahan</h2>
                        <section>
                            <label>Tanggal Usulan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="datepicker form-control" type="text" name="tanggal_usulan" required>
                                </div>
                            </div>
                            <label>Permasalahan Klien</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea class="form-control no-resize auto-growth" name="permasalahan" rows="1" ></textarea>
                                </div>
                            </div>
                            <label>Harapan Klien</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea class="form-control no-resize auto-growth" name="harapan" rows="1" ></textarea>
                                </div>
                            </div>
                            <label>Tahun Teridentifikasi</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="number" name="tahun" required>
                                </div>
                            </div>
                            <label>Bidang</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="bidang_id" data-live-search="true" data-size=5 required>
                                    <option value="">-- Silahkan Pilih --</option>
                                    @foreach($bidangs as $bidang)
                                        <option value="{{$bidang->id}}">{{$bidang->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label>Jenis PMKS</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="pmks_id" data-live-search="true" data-size=5>
                                    <option value="">-- Silahkan Pilih --</option>
                                    @foreach($jenis_pmks as $key => $pmks)
                                        <option value="{{$pmks->id}}" {{$key==0? 'selected': ''}}>{{$pmks->nama}}</option>
                                    @endforeach
                                </select>
                            </div>

                            @foreach($pmks_all as $key => $s)
                                <div id="pmks_{{$s->id}}" style="{{$key!=0? 'display: none' : ''}}">
                                @if($s->pmks_form)
                                    @foreach($s->pmks_form as $k => $pf)
                                        <label>{{$pf->title}}</label>
                                        @if($pf->type=="text")
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input class="form-control" type="text" name="tambahan_{{$pf->id}}" {{$key!=0? 'disabled': ''}}>
                                            </div>
                                        </div>
                                        @elseif($pf->type=="select")
                                        <div class="form-group">
                                            <select class="form-control show-tick" name="tambahan_{{$pf->id}}" {{$key!=0? 'disabled': ''}}>
                                                <option value="1">Ya</option>
                                                <option value="0">Tidak</option>
                                            </select>
                                        </div>
                                        @endif
                                    @endforeach
                                @endif
                                </div>
                            @endforeach
                        </section>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script src="/bower/materialdesign/plugins/jquery-validation/jquery.validate.js"></script>
<script src="/bower/materialdesign/plugins/jquery-steps/jquery.steps.js"></script>
<script src="/bower/materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="/bower/materialdesign/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script>
    setTimeout(function(){
        var last_val = 1;
        $("select[name='pmks_id']").on('changed.bs.select', function(e, clickedIndex, newValue, oldValue) {
            $("#pmks_"+last_val).hide();
            $("#pmks_"+last_val+" input").attr("disabled",true);
            $("#pmks_"+last_val+" select").attr("disabled",true);
            $("#pmks_"+last_val+" button").addClass("disabled");
            last_val = this.value;
            $("#pmks_"+last_val).show();
            $("#pmks_"+last_val+" input").attr("disabled",false);
            $("#pmks_"+last_val+" select").attr("disabled",false);
            $("#pmks_"+last_val+" button").removeClass("disabled");
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
    }, 3000);

    var form = $('#wizard_vertical')
    form.steps({
        headerTag: 'h2',
        bodyTag: 'section',
        transitionEffect: 'slideLeft',
        stepsOrientation: 'vertical',
        onInit: function (event, currentIndex) {
            $.AdminBSB.input.activate();
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            form.submit();
        }
    });
    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'confirm': {
                equalTo: '#password'
            }
        }
    });
    function setButtonWavesEffect(event) {
        $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
        $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
    }
    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        clearButton: true,
        weekStart: 1,
        time: false
    });
    $('.money-rupiah').inputmask('Rp 9.999.999', { numericInput: true,removeMaskOnSubmit:true });
    $('.phone').inputmask('+62 999999999999', { removeMaskOnSubmit:true });
    $('.nik').inputmask('9999999999999999', { removeMaskOnSubmit:true });
</script>
@endsection
