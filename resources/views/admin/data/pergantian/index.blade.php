@extends('layouts.main')
@section('title') Data Pergantian @endsection
@section('navbar-title') Data Pergantian @endsection
@section('navbar-link') /admin/pergantians @endsection
@php
    $query = Request::query();
@endphp
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>
                                Data Pergantian PMKS
                            </h2>
                        </div>
                        <div class="col-md-6">
                            <a href="/admin/pergantians/create">
                                <button type="button" class="btn bg-green btn-circle-lg waves-effect waves-circle waves-float pull-right">
                                <i class="material-icons">add</i>
                            </button>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="body">
                    <form action="/admin/pergantians" method="GET">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>NIK PMKS diganti</th>
                                    <th>Nama PMKS diganti</th>
                                    <th>NIPM PMKS diganti</th>
                                    <th>NIK PMKS Pengganti</th>
                                    <th>Nama PMKS Pengganti</th>
                                    <th>NIPM PMKS Pengganti</th>
                                    <th>Intervensi</th>
                                    <th>Kelurahan</th>
                                    <th>Kecamatan</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input class="form-control" type="text" name="nik1" value="{{$nik1}}">
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input class="form-control" type="text" name="nama1" value="{{$nama1}}">
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input class="form-control" type="text" name="nipm1" value="{{$nipm1}}">
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input class="form-control" type="text" name="nik2" value="{{$nik2}}">
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input class="form-control" type="text" name="nama2" value="{{$nama2}}">
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input class="form-control" type="text" name="nipm2" value="{{$nipm2}}">
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <select class="form-control show-tick" name="intervensi_id" data-live-search="true" data-size=5>
                                                    <option value="">-- Silahkan Pilih --</option>
                                                    @foreach($intervensis as $intervensi)
                                                        <option value="{{$intervensi->id}}">{{$intervensi->nama}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <select class="form-control show-tick" name="kelurahan_id" data-live-search="true" data-size=5>
                                                    <option value="">-- Silahkan Pilih --</option>
                                                    @foreach($kelurahans as $kelurahan)
                                                        <option value="{{$kelurahan->id}}">{{$kelurahan->nama}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="form-group">
                                                <select class="form-control show-tick" name="kecamatan_id" data-live-search="true" data-size=5>
                                                    <option value="">-- Silahkan Pilih --</option>
                                                    @foreach($kecamatans as $kecamatan)
                                                        <option value="{{$kecamatan->id}}">{{$kecamatan->nama}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </th>
                                        <th>
                                            <button type="submit" class="btn bg-green waves-effect" data-dismiss="modal">Cari</button>
                                        </th>
                                    </tr>


                                @foreach($pergantians as $key =>$pergantian)
                                <tr>
                                    <th scope="row">{{$loop->index+$pergantians->firstItem()}}</th>
                                    <td>{{$pergantian->nik1}}</td>
                                    <td>{{$pergantian->nama1}}</td>
                                    <td>{{$pergantian->nipm1}}</td>
                                    <td>{{$pergantian->nik2}}</td>
                                    <td>{{$pergantian->nama2}}</td>
                                    <td>{{$pergantian->nipm2}}</td>
                                    <td>
                                        {{$pergantian->intervensi->nama}}
                                    </td>
                                    <td>{{$pergantian->kelurahan->nama}}</td>
                                    <td>{{$pergantian->kecamatan->nama}}</td>
                                    <td>
                                        <a href="/admin/pergantians/{{$pergantian->id}}">
                                            <button type="button" class="btn bg-blue waves-effect">
                                                <i class="material-icons">info</i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    </form>
                    <div class="row">
                        <div class="hidden-md hidden-lg">
                            <div class="col-sm-12 text-center">
                                Menampilkan <b>{{$pergantians->count()+($pergantians->currentPage()-1)*$pergantians->perPage()}} dari {{$pergantians->total()}}</b> data
                            </div>
                            <div class="col-sm-12 text-center pagination">
                                {{ $pergantians->appends($_GET)->links('vendor.pagination.custom') }}
                            </div>
                        </div>
                        <div class="hidden-sm hidden-xs">
                            <div class="col-sm-4">
                                Menampilkan <b>{{$pergantians->count()+($pergantians->currentPage()-1)*$pergantians->perPage()}} dari {{$pergantians->total()}}</b> data
                            </div>
                            <div class="col-sm-8 text-center pagination">
                                {{ $pergantians->appends($_GET)->links('vendor.pagination.custom') }}
                            </div>
                        </div>
				    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script>
    @if (session('create_success'))
        swal("Membuat data berhasil  ", "Data berhasil disimpan", "success");
    @endif
    @if (session('activate_success'))
        swal("Verifikasi Berhasil", "Data Telah Diverifikasi", "success");
    @endif
    $("select[name='intervensi_id']").val({{$intervensi_id}})
    $("select[name='kelurahan_id']").val({{$kelurahan_id}})
    $("select[name='kecamatan_id']").val({{$kecamatan_id}})
    $('select').selectpicker('refresh')
</script>
@endsection
