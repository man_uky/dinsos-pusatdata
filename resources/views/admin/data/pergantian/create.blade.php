@extends('layouts.main')
@section('title') Pergantian @endsection
@section('navbar-title') Pergantian @endsection
@section('navbar-link') /admin/pergantians @endsection
@php
    $query = Request::query();
@endphp
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>
                                Pergantian
                                <small>Kelola Pergantian</small>
                            </h2>
                        </div>
                    </div>

                <div class="body">
                    <form id="search_form" action="/admin/pergantians/create" method="GET">
                        <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="intervensi_id" data-live-search="true" data-size=5>
                                            <option selected disabled>Pilih Intervensi</option>
                                            @foreach ($intervensis as $iv)
                                                <option value="{{$iv->id}}">{{$iv->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="kelurahan_id" data-live-search="true" data-size=5>
                                            <option selected value="">Pilih Kelurahan</option>
                                            @foreach ($kelurahans as $iv)
                                                <option value="{{$iv->id}}">{{$iv->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="kecamatan_id" data-live-search="true" data-size=5>
                                            <option selected value="">Pilih Kecamatan</option>
                                            @foreach ($kecamatans as $iv)
                                                <option value="{{$iv->id}}">{{$iv->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-success">Cari</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form action="/admin/pergantians" method="post">
                    @csrf
                    <input type="hidden" name="intervensi_id" value="{{$intervensi_id}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control show-tick" name="pmks_1" data-live-search="true" data-size=5>
                                    <option selected disabled>Pilih PMKS yang digantikan</option>
                                    @if(isset($clients_1))
                                    @foreach ($clients_1 as $iv)
                                        <option value="{{$iv->id}}">{{$iv->nik}} - {{$iv->nama}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <br>
                            <label>NIK</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control  nik" type="text" name="nik1" maxlength="16" minlength="6" readonly>
                                </div>
                                <div class="help-info">Panjang NIK 16 digit</div>
                            </div>
                            <label>Nama</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="nama1" readonly>
                                </div>
                            </div>
                            <label>NIPM</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="nipm1" readonly>
                                </div>
                            </div>
                            <label>Alamat</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="alamat1" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control show-tick" name="pmks_2" data-live-search="true" data-size=5>
                                    <option selected disabled>Pilih PMKS yang menggantikan</option>
                                    @if(isset($clients_2))
                                    @foreach ($clients_2 as $iv)
                                        <option value="{{$iv->id}}">{{$iv->nik}} - {{$iv->nama}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <br>
                            <label>NIK</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control  nik" type="text" name="nik2" maxlength="16" minlength="6" readonly>
                                </div>
                                <div class="help-info">Panjang NIK 16 digit</div>
                            </div>
                            <label>Nama</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="nama2" readonly>
                                </div>
                            </div>
                            <label>NIPM</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="nipm2">
                                </div>
                            </div>
                            <label>Alamat</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control" type="text" name="alamat2" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="border: 1px solid green;"/>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Tanggal Berhenti</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="datepicker form-control" type="text" name="tanggal_berhenti" maxlength="16" minlength="6" readonly>
                                </div>
                            </div>
                            <label>Keterangan</label>
                            <div class="form-group">
                                <select class="form-control show-tick" name="keterangan" data-live-search="true" data-size=5>
                                    <option value="Meninggal">Meninggal</option>
                                    <option value="Pindah Kota">Pindah Kota</option>
                                    <option value="Menolak">Menolak</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-block btn-lg wave-effect btn-success">Simpan</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Tanggal Berita Acara</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="datepicker form-control" type="text" name="tanggal_ba" maxlength="16" minlength="6" readonly>
                                </div>
                            </div>
                            <label>Tanggal Menerima</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="datepicker form-control" type="text" name="tanggal_menerima" maxlength="16" minlength="6" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script src="/bower/materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script>
    @if (session('create_success'))
        swal("Membuat data berhasil  ", "Data berhasil disimpan", "success");
    @endif
    @if (session('activate_success'))
        swal("Verifikasi Berhasil", "Data Telah Diverifikasi", "success");
    @endif
    $("select[name='pmks_1']").on('change', function() {
        $.get( "/api/get_client/"+this.value, function( data ) {
            $("input[name='nik1']").val(data.nik)
            $("input[name='nama1']").val(data.nama)
            $("input[name='alamat1']").val(data.alamat_kk)
        });
        $.get( "/api/get_client_nipm/{{$intervensi_id}}/"+this.value, function( data ) {
            $("input[name='nipm1']").val(data.nipm)
        });
    });
    $("select[name='pmks_2']").on('change', function() {
        $.get( "/api/get_client/"+this.value, function( data ) {
            $("input[name='nik2']").val(data.nik)
            $("input[name='nama2']").val(data.nama)
            $("input[name='alamat2']").val(data.alamat_kk)
        });
        $.get( "/api/get_last_nipm/{{$intervensi_id}}", function( res ) {
            if(!res)nipm=1;
            else nipm=res.nipm+1
            $("input[name='nipm2']").val(nipm)
        });
    });
    $("select[name='intervensi_id']").val({{$intervensi_id}})
    $("select[name='kelurahan_id']").val({{$kelurahan_id}})
    $("select[name='kecamatan_id']").val({{$kecamatan_id}})
    $('select').selectpicker('refresh')
    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        clearButton: true,
        weekStart: 1,
        time: false
    });
</script>
@endsection
