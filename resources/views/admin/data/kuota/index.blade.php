@extends('layouts.main')
@section('title') Kuota @endsection
@section('navbar-title') Kuota @endsection
@section('navbar-link') /admin/kuotas @endsection
@php
    $query = Request::query();
@endphp
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>
                                Kuota Penyedia
                                <small>Kelola Kuota</small>
                            </h2>
                        </div>
                    </div>

                <div class="body">
                    <form id="search_form" action="/admin/kuotas" method="GET">
                        <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="kuota_type" data-live-search="true" data-size=5>
                                            <option selected disabled>Pilih Peneydia</option>
                                            <option value="1">Karang Werdha</option>
                                            <option value="2">IPSM</option>
                                            <option value="3">PANTI</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-success">Cari</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Kuota Sebelumnya</th>
                                    <th>Terisi</th>
                                    <th>Status</th>
                                    <th>Kuota baru</th>
                                    <th class="col-xs-2">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                                @forelse($kuotas as $key =>$kuota)
                                <form action="/admin/kuotas/{{$kuota->id}}" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <input type="hidden" name="kuota_type" value="{{$kuota_type}}">
                                    <tr>
                                        <th scope="row">{{$loop->index+1}}</th>
                                        <td>{{$kuota->user->nama}}</td>
                                        <td>{{$kuota->kuota}}</td>
                                        <td>{{$kuota->terisi}}</td>
                                        <td>
                                        @if($kuota->terisi>$kuota->kuota*3/4)
                                        <span class="label label-warning" style="font-size: small;">Terisi {{$kuota->kuota-$kuota->terisi}}
                                        @elseif($kuota->terisi<$kuota->kuota)
                                        <span class="label label-success" style="font-size: small;">Terisi {{$kuota->kuota-$kuota->terisi}}
                                        @elseif($kuota->kuota==$kuota->terisi)
                                        <span class="label label-danger" style="font-size: small;">Penuh
                                        @else
                                        <span class="label label-danger" style="font-size: small;">Lebih {{$kuota->terisi-$kuota->kuota}}
                                        @endif
                                        </span>
                                        </td>
                                        <td><input type="number" name="kuota_baru"></td>
                                        <td><button type="submit" class="btn btn-primary">Simpan</button></td>
                                    </tr>
                                </form>

                                @empty
                                @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script>
    @if (session('create_success'))
        swal("Membuat data berhasil  ", "Data berhasil disimpan", "success");
    @endif
    @if (session('activate_success'))
        swal("Verifikasi Berhasil", "Data Telah Diverifikasi", "success");
    @endif
    $("select[name='kuota_type']").val({{$kuota_type}})
    $('select').selectpicker('refresh')
</script>
@endsection
