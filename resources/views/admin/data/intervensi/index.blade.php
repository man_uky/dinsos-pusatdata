@extends('layouts.main')
@section('title') Intervensi @endsection
@section('navbar-title') Intervensi @endsection
@section('navbar-link') /admin/clients @endsection
@php
$user = Auth::user();
$query = Request::query();
@endphp
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>
                                Data Client
                                <small>Tambah Intervensi</small>
                            </h2>
                        </div>
                        <div class="col-md-6">
                            <a href="/admin/clients/create">
                                <a href="{{strpos(URL::previous(),"?page")? URL::previous() : '/admin/clients?status_penanganan='.$query['status_penanganan']}}">
                                    <button type="button" class="btn bg-blue btn-circle-lg waves-effect waves-circle waves-float pull-right">
                                        <i class="material-icons">arrow_back</i>
                                    </button>
                                </a>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="body">
                    <div class="row">
                    	<div class="col-md-6">
	                        <a class="thumbnail text-center">
	                            @if($client->photo_id)
                                <img src="/photo/{{$client->photo_id}}" class="img-responsive">
                                @else
                                <img src="/images/Ciri-Balita-Tumbuh-Sehat.png" class="img-responsive">
                                @endif
	                        </a>
                    	</div>
                    	<div class="col-md-6">
                    		<label>NIK</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->nik}}
	                                </div>
	                            </div>
	                            <label>KK</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->no_kk}}
	                                </div>
	                            </div>
	                            <label>Nama</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->nama}}
	                                </div>
	                            </div>

	                            <label>Alamat Sesuai KK</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->alamat_kk}}
	                                </div>
	                            </div>
                    	</div>
                    </div>
                	<div class="row">
	                	<div class="col-md-6">
	                        <form>
	                        	<label>Daerah Asal</label>
	                        	<div class="form-group">
	                        	    <div class="form-line">
	                        	        {{$client->asal}}
	                        	    </div>
	                        	</div>
	                            <label>Kelurahan</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->kelurahan->nama}}
	                                </div>
	                            </div>
	                            <label>Kecamatan</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->kecamatan->nama}}
	                                </div>
	                            </div>
	                            <label>Jenis PMKS</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->pmks ? $client->pmks->nama : ''}}
	                                </div>
	                            </div>

	                        </form>
	                    </div>
	                    <div class="col-md-6">
	                        <form>
	                        	<label>Intervensi</label>
	                        	<div class="form-group">
	                        	    <div class="form-line">
	                        	        {{$client->intervensi ? $client->intervensi->nama : ''}}
	                        	    </div>
	                        	</div>
	                            <label>Bidang</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->bidang ? $client->bidang->nama : ''}}
	                                </div>
	                            </div>
	                            <label>Tahun Teridentifikasi</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->tahun}}
	                                </div>
	                            </div>
	                            <label>Status Penanganan</label>
	                            <div class="form-group">
	                                <div class="form-line">
	                                    {{$client->status_penanganan ? $client->status_penanganan->nama : ''}}
	                                </div>
	                            </div>
	                        </form>
	                    </div>
                	</div>
                	@if($user->role_id==ROLE_ADMIN)
                	<div class="row">
                		<div class="col-md-3">
                			<button id="add_intervensi" type="button" class="btn bg-green waves-effect">
                                <i class="material-icons">note_add</i>
                            </button>
                		</div>
                	</div>
                	@endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Intervensi</th>
                                    <th>Bidang</th>
                                    <th>Status Penanganan</th>
                                    <th>Jenis Penanganan</th>
                                    <th class="col-xs-3">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($client_intervensis as $key => $client_intervensi)
                                <tr>
                                    <th scope="row">{{$loop->index+$client_intervensis->firstItem()}}</th>
                                    <td>
                                    	@if($client_intervensi->intervensi)
                                    	{{$client_intervensi->intervensi->nama}}
                                    	@else
                                    	Belum di tentukan
                                    	@endif
                                    </td>
                                    <td>{{$client_intervensi->bidang->nama}}</td>
                                    <td>{{$client_intervensi->status_penanganan}}</td>
                                    <td>{{$client_intervensi->jenis_penanganan}}</td>
                                    <td>
                                    	@if($user->role_id==ROLE_BIDANG)
                                        <button type="button" class="btn bg-blue waves-effect" onclick="edit_intervensi({{$client_intervensi->id}})">
                                            <i class="material-icons">edit</i>
                                        </button>
                                        @endif
                                        @if($user->role_id==ROLE_ADMIN || $client_intervensi->intervensi_id!=null)
                                        <button type="button" class="btn bg-blue waves-effect" onclick="show_intervensi({{$client_intervensi->toJson()}})">
                                            <i class="material-icons">info</i>
                                        </button>
                                        <button type="button" class="btn bg-pink waves-effect" data-color="brown" onclick="hapus_intervensi({{$client_intervensi->id}})">
                                            <i class="material-icons">delete</i>
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <div class="hidden-md hidden-lg">
                            <div class="col-sm-12 text-center">
                                Menampilkan <b>{{$client_intervensis->count()+($client_intervensis->currentPage()-1)*$client_intervensis->perPage()}} dari {{$client_intervensis->total()}}</b> data
                            </div>
                            <div class="col-sm-12 text-center pagination">
                                {{ $client_intervensis->links('vendor.pagination.custom') }}
                            </div>
                        </div>
                        <div class="hidden-sm hidden-xs">
                            <div class="col-sm-4">
                                Menampilkan <b>{{$client_intervensis->count()+($client_intervensis->currentPage()-1)*$client_intervensis->perPage()}} dari {{$client_intervensis->total()}}</b> data
                            </div>
                            <div class="col-sm-8 text-center pagination">
                                {{ $client_intervensis->links('vendor.pagination.custom') }}
                            </div>
                        </div>
				    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- Modal --}}
    <div class="modal fade" id="addIntervensi" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Tambah Intervensi Baru</h4>
                </div>
                <form method="POST" action="/admin/clients/{{$client->id}}/intervensi">
                <div class="modal-body">
                	@csrf
                	<label>Bidang</label>
                    <div class="form-group">
                        <select class="form-control show-tick" name="bidang_id" data-live-search="true" data-size=5>
                            <option value="">-- Silahkan Pilih --</option>
                            @foreach($bidangs as $bidang)
                                <option value="{{$bidang->id}}">{{$bidang->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">Simpan</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Batal</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editIntervensi" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Keterangan Intervensi</h4>
                </div>
                <form method="POST" action="/admin/clients/{{$client->id}}/intervensi">
                <div class="modal-body">
                	@method('PUT')
                	@csrf
                	<input id="client_intervensi_id" type="hidden" name="id">
                    <input type="hidden" name="status_penanganan" value="DITANGANI">
                    {{-- <div class="row">
                        <div class="col-md-3">
                            <span id="kuota_kelurahan" class="label label-warning" style="font-size: small;">Kelurahan 1/250</span>
                        </div>
                        <div class="col-md-3">
                            <span id="kuota_kecamatan" class="label label-warning" style="font-size: small;">Kecamatan 1/250</span>
                        </div>
                    </div> --}}
                    <br>
                    <label>Jenis Intervensi</label>
                    <div class="form-group">
                        <select class="form-control show-tick" name="intervensi_id" data-live-search="true" data-size=5>
                            @foreach($intervensis as $intervensi)
                        		<option value="{{$intervensi->id}}">{{$intervensi->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label>NIPM</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input class="form-control" type="text" name="nipm" readonly>
                        </div>
                    </div>
                    <label>Penyedia</label>
                    <div class="form-group">
                        <select class="form-control show-tick" name="penyedia_id" data-live-search="true" data-size=5>
                            @foreach($penyedias as $penyedia)
                        		<option value="{{$penyedia->id}}">{{$penyedia->user->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label>Tanggal Mulai Intervensi</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input class="datepicker form-control" type="text" name="tanggal_mulai" required>
                        </div>
                    </div>
                	<label>Jenis Penanganan</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input class="form-control" type="text" name="jenis_penanganan" required>
                        </div>
                    </div>
                    <label>Jenis Rehabilitasi</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input class="form-control" type="text" name="jenis_rehabilitasi" required>
                        </div>
                    </div>
                	<label>Status Rehabilitasi</label>
                    <div class="form-group">
                        <select class="form-control show-tick" name="status_rehabilitasi" data-live-search="true" data-size=5>
                        	<option value="PROSES">PROSES</option>
                            <option value="BERHASIL">BERHASIL</option>
                            <option value="GAGAL">GAGAL</option>
                        </select>
                    </div>
                    <label>Deskripsi Keberhasilan Rehabilitasi</label>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea class="form-control no-resize auto-growth" name="deskripsi_rehabilitasi" rows="1" ></textarea>
                        </div>
                    </div>
                    <label>Status Pengentasan</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input class="form-control" type="text" name="status_pengentasan">
                        </div>
                    </div>
                    <label>PSKS Pembantu</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input class="form-control" type="text" name="psks_pembantu">
                        </div>
                    </div>
                    <label>Perkembangan Terbaru</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input class="form-control" type="text" name="perkembangan_terbaru">
                        </div>
                    </div>
                    <label>No Berita Acara</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input class="form-control" type="text" name="no_berita_acara">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">Simpan</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Batal</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteIntervensi" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="GET" action="/admin/{{$client->id}}/delete_intervensi">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Selesaikan Intervensi</h4>
                </div>

                    <div class="modal-body">
                        <label>Tanggal Selesai Intervensi</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input class="datepicker form-control" type="text" name="tanggal_berhenti" required>
                            </div>
                        </div>
                    </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">Simpan</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Batal</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="showIntervensi" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Keterangan Intervensi</h4>
                </div>
                <div class="modal-body">
                    <input id="client_intervensi_id" type="hidden" name="id">
                    <input type="hidden" name="status_penanganan" value="DITANGANI">
                    <label>Jenis Intervensi</label>
                    <div class="form-group">
                        <div class="form-line">
                            <span id="intervensi"></span>
                        </div>
                    </div>
                    <label>Jenis Penanganan</label>
                    <div class="form-group">
                        <div class="form-line">
                            <span id="jenis_penanganan"></span>
                        </div>
                    </div>
                    <label>Jenis Rehabilitasi</label>
                    <div class="form-group">
                        <div class="form-line">
                            <span id="jenis_rehabilitasi"></span>
                        </div>
                    </div>
                    <label>Status Rehabilitasi</label>
                    <div class="form-group">
                        <div class="form-line">
                            <span id="status_rehabilitasi"></span>
                        </div>
                    </div>
                    <label>Deskripsi Keberhasilan Rehabilitasi</label>
                    <div class="form-group">
                        <div class="form-line">
                            <span id="deskripsi_rehabilitasi"></span>
                        </div>
                    </div>
                    <label>Status Pengentasan</label>
                    <div class="form-group">
                        <div class="form-line">
                            <span id="status_pengentasan"></span>
                        </div>
                    </div>
                    <label>PSKS Pembantu</label>
                    <div class="form-group">
                        <div class="form-line">
                            <span id="psks_pembantu"></span>
                        </div>
                    </div>
                    <label>Perkembangan Terbaru</label>
                    <div class="form-group">
                        <div class="form-line">
                            <span id="perkembangan_terbaru"></span>
                        </div>
                    </div>
                    <label>No Berita Acara</label>
                    <div class="form-group">
                        <div class="form-line">
                            <span id="no_berita_acara"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/bower/materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	<script>
		$("#add_intervensi").click(function(){
			$("#addIntervensi").modal('show');
		})
        function get_kuota(id)
        {
            if(id==null)id={{$intervensis->first()->id}}
            $.get( "/api/get_kuota/{{$client->id}}/"+id, function( res ) {
                console.log(res)
                kuota_kelurahan = $("#kuota_kelurahan")
                kuota_kecamatan = $("#kuota_kecamatan")
                kuota_kelurahan.text("Kelurahan "+res.kuota_kelurahan.terisi+"/"+res.kuota_kelurahan.kuota)
                kuota_kecamatan.text("Kecamatan "+res.kuota_kecamatan.terisi+"/"+res.kuota_kecamatan.kuota)
                if(res.kuota_kecamatan.terisi>res.kuota_kecamatan.kuota*3/4)
                {
                    kuota_kecamatan.attr("class","label label-warning")
                    kuota_kelurahan.attr("class","label label-warning")
                }
                else if(res.kuota_kecamatan.terisi<res.kuota_kecamatan.kuota)
                {
                    kuota_kecamatan.attr("class","label label-success")
                    kuota_kelurahan.attr("class","label label-success")
                }
                else if(res.kuota_kecamatan.terisi==res.kuota_kecamatan.kuota){
                    kuota_kecamatan.attr("class","label label-danger")
                    kuota_kelurahan.attr("class","label label-danger")
                }

            });
        }
		function edit_intervensi(id)
		{
			$("#client_intervensi_id").val(id);
			$("#editIntervensi").modal('show');
            $.get( "/api/get_data_intervensi/"+id, function( data ) {
            //   get_kuota(data.intervensi_id);
              $("select[name='intervensi_id']").val(data.intervensi_id)
              $("input[name='jenis_penanganan']").val(data.jenis_penanganan)
              $("input[name='jenis_rehabilitasi']").val(data.jenis_rehabilitasi)
              $("select[name='status_rehabilitasi']").val(data.status_rehabilitasi)
              $("input[name='deskripsi_rehabilitasi']").val(data.deskripsi_rehabilitasi)
              $("input[name='status_pengentasan']").val(data.status_pengentasan)
              $("input[name='psks_pembantu']").val(data.psks_pembantu)
              $("input[name='tanggal_mulai']").val(data.tanggal_mulai)
              $("input[name='perkembangan_terbaru']").val(data.perkembangan_terbaru)
              $('select').selectpicker('refresh')
              if(!data.nipm){
                intervensi_id = data.intervensi_id;
                if(!data.intervensi_id)intervensi_id=$("select[name='intervensi_id']").val();
                $.get( "/api/get_last_nipm/"+intervensi_id, function( res ) {
                  if(!res)nipm=1;
                  else nipm=res.nipm+1
                  $("input[name='nipm']").val(nipm)
                });
              }
              else $("input[name='nipm']").val(data.nipm)
            });

		}
        $("select[name='intervensi_id']").on('change', function() {
            // get_kuota(this.value)
            $.get( "/api/get_last_nipm/"+this.value, function( res ) {
                if(!res)nipm=1;
                else nipm=res.nipm+1
                $("input[name='nipm']").val(nipm)
            });
        });
        function show_intervensi(data)
        {
            $("#intervensi").text(data.intervensi.nama);
            $("#jenis_penanganan").text(data.jenis_penanganan);
            $("#jenis_rehabilitasi").text(data.jenis_rehabilitasi);
            $("#status_rehabilitasi").text(data.status_rehabilitasi);
            $("#status_pengentasan").text(data.status_pengentasan);
            $("#deskripsi_rehabilitasi").text(data.deskripsi_rehabilitasi);
            $("#psks_pembantu").text(data.psks_pembantu);
            $("#perkembangan_terbaru").text(data.perkembangan_terbaru);
            $("#showIntervensi").modal('show');
        }
		function hapus_intervensi(id)
		{
            $("#deleteIntervensi").modal('show');
		}
        $('.datepicker').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            clearButton: true,
            weekStart: 1,
            time: false
        });
		@if(session('create_success'))
	        swal("Membuat data berhasil  ", "Data berhasil disimpan", "success");
	    @endif
	    @if(session('delete_success'))
	        swal("Menghapus data berhasil  ", "Data berhasil dihapus", "success");
	    @endif
	</script>
@endsection
