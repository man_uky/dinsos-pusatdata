@extends('layouts.main')
@section('title') Home @endsection
@section('navbar-title') Home @endsection
@section('navbar-link') /admin @endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Home</h2>
            </div>
        </div>
    </section>
@endsection