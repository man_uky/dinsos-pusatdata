@php
   $route = Request::path();
   $query = Request::query();
   $route = explode('/', $route); //split
   $user = Auth::user();
@endphp

<!-- #Top Bar Side bar -->
<section>
   <!-- Left Sidebar -->
   <aside id="leftsidebar" class="sidebar">
      <!-- Menu -->
      <div class="menu">
         <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ isset($route[0]) && $route[0]=='admin' && !isset($route[1]) ? 'active' : ''}}"> <!-- admin: yg di routes. jika if benar maka active, jika salah tdk muncul apa2-->
               <a href="/admin">
               <i class="material-icons">home</i>
               <span>Home</span>
               </a>
            </li>
            @if (in_array(Auth::user()->role_id, [ROLE_ADMIN,ROLE_BIDANG]))
            <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && $route[1]=='clients' ? 'active' : ''}}">
               <a href="#" class="menu-toggle">
               <i class="material-icons">storage</i>
               <span>Data PMKS</span>
               </a>
               <ul class="ml-menu">
                  <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && isset($query['status_penanganan']) && $query['status_penanganan']==1 ? 'active' : ''}}">
                     <a href="/admin/clients?status_penanganan=1">Data Baru</a>
                  </li>
                  <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && isset($query['status_penanganan']) && $query['status_penanganan']==2 ? 'active' : ''}}">
                     <a href="/admin/clients?status_penanganan=2">Belum Ditangani</a>
                  </li>
                  <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && isset($query['status_penanganan']) && $query['status_penanganan']==3 ? 'active' : ''}}">
                     <a href="/admin/clients?status_penanganan=3">PMKS Aktif</a>
                  </li>
               </ul>
            </li>
            @endif
            @if($user->role_id==ROLE_ADMIN)
            <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && $route[1]== 'bidangs' ? 'active' : ''}}">
               <a href="#" class="menu-toggle">
               <i class="material-icons">storage</i>
               <span>Data Bidang</span>
               </a>
               <ul class="ml-menu">
                  <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && $route[1]=='bidangs' ? 'active' : ''}}">
                     <a href="/admin/bidangs">Data Bidang</a>
                  </li>
               </ul>
            </li>
            @endif
            @if($user->role_id==ROLE_BIDANG)
            <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && $route[1]=='kuotas' ? 'active' : ''}}">
                <a href="/admin/kuotas">
                    <i class="material-icons">queue</i>
                    <span>Kuota</span>
                </a>
            </li>
            @endif

            {{-- <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && $route[1]== 'pergantians' ? 'active' : ''}}">
                <a href="#" class="menu-toggle">
                <i class="material-icons">storage</i>
                <span>Pergantian PMKS</span>
                </a>
                <ul class="ml-menu">
                    <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && $route[1]=='pergantians' && isset($route[2]) &&  $route[2]=='create' ? 'active' : ''}}">
                        <a href="/admin/pergantians/create">Ganti PMKS Baru</a>
                    </li>
                    <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && $route[1]=='pergantians' && !isset($route[2]) ? 'active' : ''}}">
                        <a href="/admin/pergantians">Daftar Pergantian PMKS</a>
                    </li>
                </ul>
            </li> --}}
            @if($user->role_id==ROLE_PENYEDIA)
            <li class="{{ isset($route[0]) && $route[0]=='admin' && isset($route[1]) && $route[1]=='download' ? 'active' : ''}}">
               <a href="/admin/download">
               <i class="material-icons">assignment</i>
               <span>Cetak Dokumen</span>
               </a>
            </li>
            @endif

         </ul>
      </div>
      <!-- #Menu -->


      <!-- Footer -->
      <div class="legal">
         <div class="copyright">
            &copy; 2017 - 2018 <a href="#">Pusat Data Dinas Sosial</a>.
         </div>
         <div class="version">
            <b>Version: </b> 1.0.0
         </div>
      </div>
      <!-- #Footer -->

   </aside>
   <!-- #END# Left Sidebar -->
</section>
