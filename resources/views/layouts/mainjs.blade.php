<!-- Jquery Core Js -->
    <script src="/bower/materialdesign/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/bower/materialdesign/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="/bower/materialdesign/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/bower/materialdesign/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/bower/materialdesign/plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="/bower/materialdesign/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="/bower/materialdesign/plugins/raphael/raphael.min.js"></script>
    <script src="/bower/materialdesign/plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="/bower/materialdesign/plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="/bower/materialdesign/plugins/flot-charts/jquery.flot.js"></script>
    <script src="/bower/materialdesign/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="/bower/materialdesign/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="/bower/materialdesign/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="/bower/materialdesign/plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="/bower/materialdesign/plugins/jquery-sparkline/jquery.sparkline.js"></script>
    <script src="/bower/materialdesign/plugins/momentjs/moment.js"></script>
    <!-- Custom Js -->
    <script src="/bower/materialdesign/js/admin.js"></script>
    <!-- <script src="/bower/materialdesign/js/pages/index.js"></script> -->

    <!-- Demo Js -->
    <!-- <script src="/bower/materialdesign/js/demo.js"></script> -->
    <script src="/bower/materialdesign/plugins/sweetalert/sweetalert.min.js"></script>
    @yield('js')