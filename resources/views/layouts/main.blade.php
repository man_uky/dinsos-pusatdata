<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('title')</title>
    @include('layouts.maincss')

</head>

<body class="theme-green">
    @include('layouts.pageloader')

    @include('layouts.navbar')

    <!-- #Top Bar Side bar -->
    @include('layouts.sidebar')

    <!-- content -->
    <div class="header">
        
    </div>
    
    <div class="body">
        @yield('content')
    </div>


    @include('layouts.mainjs')
</body>

</html>