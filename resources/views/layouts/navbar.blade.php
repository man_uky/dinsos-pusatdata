<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<nav class="navbar">
   <div class="container-fluid">
      <div class="navbar-header">
         <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
         <a href="javascript:void(0);" class="bars"></a>
         <a class="navbar-brand" href="@yield('navbar-link')">@yield('navbar-title')</a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse">
         <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
               <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                  <i class="material-icons">account_circle</i>
               </a>
               <ul class="dropdown-menu">
                  <li class="header">PROFIL</li>
                  <li class="body">
                     <!--  <ul class="menu"> -->
                     <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;">
                        <ul class="menu" style="overflow: hidden; width: auto; height: auto;">
                           <li>
                              <a href="#">
                                 <div class="icon-circle">
                                    <img src="/bower/materialdesign/images/user.png" width="40" height="40" alt="User" />
                                 </div>
                                 <div class="menu-info">
                                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->nama}}</div>
                                    
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="/logout">
                                 <div class="icon-circle bg-light-blue">
                                    <i class="material-icons">input</i>
                                 </div>
                                 <div class="menu-info">
                                    <h4>SIGN OUT</h4>
                                 </div>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </li>
               </ul>
            </li>
         </ul>
      </div>
   </div>
</nav>