<?php

use Illuminate\Http\Request;
use App\Client;
use App\ClientIntervensi;
use App\KuotaKelurahan;
use App\KuotaKecamatan;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get_last_nipm/{intervensi_id}', function (Request $request,$intervensi_id) {
	$client_intervensi= ClientIntervensi::where('intervensi_id',$intervensi_id)->orderBy('nipm','DESC')->first();
	return $client_intervensi;
});

Route::get('/get_data_intervensi/{id}', function (Request $request,$id) {
    $client_intervensi= ClientIntervensi::find($id);
    $client_intervensi['tanggal_mulai'] = date_view($client_intervensi->tanggal_mulai);
	return $client_intervensi;
});
Route::get('/get_client/{id}', function (Request $request,$id) {
    $client= Client::find($id);
	return $client;
});
Route::get('/get_client_nipm/{intervensi_id}/{id}', function (Request $request,$intervensi_id,$id) {
    $client_intervensi = ClientIntervensi::where('client_id',$id)->where('intervensi_id',$intervensi_id)->first();
	return $client_intervensi;
});

Route::get('/get_kuota/{id}/{intervensi_id}', function (Request $request,$id,$intervensi_id) {
    $client= Client::find($id);
    $data['kuota_kelurahan']= KuotaKelurahan::where('intervensi_id',$intervensi_id)
    ->where('kelurahan_id',$client->kelurahan_id)->first();
    $data['kuota_kecamatan']= KuotaKecamatan::where('intervensi_id',$intervensi_id)
    ->where('kecamatan_id',$client->kecamatan_id)->first();
	return $data;
});
