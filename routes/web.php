<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    if(\Auth::check()) return redirect('/admin');
    else return redirect('/login');
});

Route::get('/login', function () {
    if(\Auth::check()) return redirect('/admin');
    else return view('login.index');
});

Route::group(['middleware' => 'guest'], function () {
    Route::post('login','AuthController@login');
});
Route::get('/photo/{id}','IndexController@photo');
Route::group(['middleware' => 'auth'], function () {
	Route::get('/admin', 'IndexController@admin');
	Route::resource('admin/clients','ClientController');
    Route::resource('admin/bidangs','BidangController');
    Route::resource('admin/kuotas','KuotaController');
    Route::resource('admin/pergantians','PergantianController');
	Route::get('/admin/clients/{id}/intervensi','ClientIntervensiController@index');
	Route::get('/admin/clients/{id}/activate','ClientController@activate');
	Route::post('/admin/clients/{id}/intervensi','ClientIntervensiController@store');
	Route::put('/admin/clients/{id}/intervensi','ClientIntervensiController@update');
	Route::get('/admin/{id}/delete_intervensi','ClientIntervensiController@delete');
	Route::get('/admin/download/clients', 'ReportController@download_client');
	Route::get('/admin/download/kwitansi', 'ReportController@download_kwitansi');
	Route::get('/admin/download/pencairan', 'ReportController@download_pencairan');
	Route::get('/admin/download/penerima', 'ReportController@download_penerima');
	Route::get('/admin/download/mutlak', 'ReportController@download_mutlak');
	Route::get('/admin/download/kesanggupan', 'ReportController@download_kesanggupan');
	Route::get('/admin/download/rekapitulasi', 'ReportController@download_rekapitulasi');
	Route::get('/admin/download/beritakirimpanti', 'ReportController@download_beritakirimpanti');
	Route::get('/admin/download/spjpanti', 'ReportController@download_spjpanti');
	Route::get('/admin/download/rekapspjpanti', 'ReportController@download_rekapspjpanti');
	Route::get('/admin/download/kwitansiipsm', 'ReportController@download_kwitansiipsm');
	Route::get('/admin/download/rekappaca', 'ReportController@download_rekappaca');
	Route::get('/admin/download/spjipsm', 'ReportController@download_spjipsm');
	Route::get('/admin/download', 'ReportController@index');
    Route::get('logout','AuthController@logout');
});
